import PIL

# このファイルをインポートすることで、呪文等の名前を取得できるようにする。
# PEQのDBをSQLite等に格納してアクセスすると話が早いが、オンメモリとはいえ
# いちいち他のファイルにアクセスするのはどうかと思うので、とりあえず辞書にする。

SPELL_LIST_FILE = "database/spell.txt"
ZONE_LIST_FILE = "database/zone.txt"
MAP_FILE_PATH = ""

# Class ID -> (Class Name, Tank Priority, Buff Priority)
PLAYER_CLASS = {
    1: ('WAR',  16,  1),
    2: ('CLE',   7, 15),
    3: ('PAL',  14,  9),
    4: ('RNG',  13, 10),
    5: ('SHD',  15,  8),
    6: ('DRU',   1, 13),
    7: ('MNK',  10,  3),
    8: ('BRD',  12,  4),
    9: ('ROG',   8,  2),
    10: ('SHM',  6, 14),
    11: ('NEC',  5,  6),
    12: ('WIZ',  3,  5),
    13: ('MAG',  2,  7),
    14: ('ENC',  4, 16),
    15: ('BST', 11, 12),
    16: ('BSK',  9,  1),
    -2054057723: ('Unknown', 1, 1)      # なぜかこれが送られてきたことがあった
}

SPELLS = {}
ZONES = {}


with open(SPELL_LIST_FILE, "rt", encoding="CP932") as df:
    for _line in df:
        _line = _line.rstrip('\n')
        _id, _spell_name = _line.split("\t")
        SPELLS[int(_id)] = _spell_name

with open(ZONE_LIST_FILE, "rt", encoding="CP932") as df:
    for _line in df:
        _line = _line.rstrip('\n')
        _id, _short_name, _long_name = _line.split("\t")
        ZONES[int(_id)] = (_short_name, _long_name)


def get_spell_name(spell_id):
    return SPELLS.get(spell_id, '%d: Unknown Spell' % spell_id)


def get_zone_shortname(zone_id):
    if zone_id in ZONES.keys():
        return ZONES.get(zone_id)[0]
    else:
        return '%d: Unknown Zone' % zone_id


def get_zone_longname(zone_id):
    if zone_id in ZONES.keys():
        return ZONES.get(zone_id)[1]
    else:
        return '%d: Unknown Zone' % zone_id


def get_player_class(class_id):
    return PLAYER_CLASS.get(class_id, ('%d: Undefined Class' % class_id, 0, 0))


def db_get_mapimage(zone_shortname, magnification=0.2):
    """
    マップの画像と、画像の左上基準の座標とのオフセット値を返す。
    :param zone_shortname:
    :return:
    """
    pass
