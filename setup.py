import sys, os
from cx_Freeze import setup,Executable

import sys
import os
import pathlib

# set {TCL,TK}_LIBRARY environment variable
import tkinter
root = tkinter.Tk()
tcl_library_path = pathlib.Path(root.tk.exprstring('$tcl_library'))
tk_library_path = pathlib.Path(root.tk.exprstring('$tk_library'))
os.environ['TCL_LIBRARY'] = str(tcl_library_path)
os.environ['TK_LIBRARY'] = str(tk_library_path)

# find tk86t.dll and tcl86t.dll
tcltk_search_dll = {'tk86t.dll', 'tcl86t.dll'}
tcltk_found_dll = {}

for p in sys.path:
    p = pathlib.Path(p)
    for f in p.glob('*'):
        if f.is_file():
            f_low = f.name.lower()
            if f_low in tcltk_search_dll:
                tcltk_found_dll[f_low] = str(f)
                tcltk_search_dll.remove(f_low)

                if not tcltk_search_dll:
                    break
    else:
        continue
    break

if tcltk_search_dll:
    raise 'cannot find tcl/tk DLL. ' + str(tcltk_search_dll)
print('tcl/tk DLL:', tcltk_found_dll)

tcltk_include_files = [(v, 'lib/'+k) for k, v in tcltk_found_dll.items()]

# Dependencies are automatically detected, but it might need fine tuning.
'''
build_exe_options = {"packages": ['winsound',
                                  'math',
                                  'graphviz',
                                  'json',
                                  're'],
                     "include_files": ['sounds/',
                                       'database/',
                                       'path'] + tcltk_include_files}
'''
build_exe_options = {"excludes": ['email',
                                  'lib2to3',
                                  'html',
                                  'test',
                                  'unittest'],
                     "include_files": ['sounds/',
                                       'database/',
                                       'path',
                                       'settings.txt',
                                       'changelog'] + tcltk_include_files}

if sys.platform == 'win32' :
    base = 'Win32GUI'

setup(
    name='ldonrunner',
    version='0.2.3',
    description='ldon auto runner alpha',
    author='chibamax',
    options={'build_exe': build_exe_options},
    executables=[Executable('main.py')]
)

# python setup.py build
