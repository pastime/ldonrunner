# このファイルでは、空行及び#で開始する行は無視されます。
# マップファイルのパス。環境によって書き換えが必要。
MAP_PATH = C:/RoF/Project Lazarus/maps/GoodsMaps12-26-2020/Good's Maps

# E3/E4の設定。それぞれのマクロで使用する戦闘開始コマンドを登録する。
# また、ツール開始後にE3/E4のどちらを使っているか不明な場合、一定間隔ごとにldonrunnerプラグインに対して問い合わせを行う。
# 問い合わせの回答が得られるまでに仮として指定するE3/E4と、問い合わせの間隔（秒数）もここで指定できる。
E3_COMBAT_COMMAND = //bc startbattle
E4_COMBAT_COMMAND = //bc autobattle
DEFAULT_BACKEND = E3
BACKEND_POLLING_INTERVAL = 30

# Look UpとLook Downのキーを登録する。キーバインドの名称は、Macroquest2ドキュメントのAppendix Dを参照のこと。
# また、経路のリカバリーもーとで、グループの平均高度を参考に見上げたり見下ろしたりするかどうかのスイッチもここで設定する(1ならオン、0ならオフ）。
KEYMAP_LOOKUP = Page_Up
KEYMAP_LOOKDOWN = Page_Down
LOOKUP_DOWN_ON_RECOVERY = 1

# ウェイポイント間の走行時に、この秒数を経過すると到達できなかったとみなす。
ARRIVAL_WAIT_SEC = 5.0

# ウェイポイントに到達したとみなす座標誤差の閾値。20未満だと不安定になる。
ARRIVAL_DISTANCE_THRESHOLD = 20.0

# イージーリカバリーに失敗した際に、目標地点に向かって斜め後ろに後退するダイアゴナル移動モードに移行する。
# その際、円周方向にどれくらいの角度をランダムで動かすかを度数で設定。
# また、円周方向への移動と同時に、どれくらいの距離後ろ側に後退させるかを距離で設定。
# 斜め後ろ移動後に目的地へ向かい、それに失敗すると、左右を切り替えながら徐々に後退量を大きくしてリトライする。
DIAGONAL_WALK_MIN_DEGREE = 45
DIAGONAL_WALK_MAX_DEGREE = 90
DIAGONAL_WALK_SETBACK_START = 25
DIAGONAL_WALK_SETBACK_INCREMENT = 5
DIAGONAL_WALK_SETBACK_MAX = 60

# 戦闘中に自動的に戦闘開始コマンドをやり直す間隔を秒数で設定する。戦闘途中で戦闘を中止してしまうキャラがいる場合の対策。
# 指示を出してから戦闘に参加するまでにタイムラグがあるため、あまり頻繁にやると逆に戦闘に参加してくれなくなる。2秒では早すぎる。
# この値を長くすると、実質的に無効化できる。
STARTBATTLE_REFLESH_SEC = 4.0

# クライアント側で戦闘状態が変化するとE3/E4プラグインからldonrunnerに報告するが、何らかの理由でこの報告が実行されないと、
# クライアントとldonrunnerで戦闘状態の終了状態がずれてしまう。最後にいずれかのキャラクターのヒットポイントが変化してから
# 一定時間経過すると、//bc lrwazzupを送出して戦闘情報を強制的に更新をさせる。その時間設定(秒数）。
# 敵が非常に弱い場合はノーダメージで戦闘が終了することがあるので、そういう場合でもスパムにならない程度の長さが必要。
CHECK_BATTLE_STATUS_AFTER_THIS_SECONDS = 20

# ゾーンプランの消費が進んだ状態（headが0でない）で最も近いウェイポイントの探索を行うときに、候補として上位何個までポイントを見つけるか。
# 消費済みかつこの候補の点のうちで、もっともインデックスの大きいウェイポイントが選択される。1にすると、常に最も近い点を採用する。
MAX_SEEK_HEAD_CANDIDATES = 3

# イージーリカバリーモードの時に、何個前のウェイポイントまで戻るか。
MAX_ROLLBACK_WAYPOINTS_FOR_EZ_RECOVERY = 5

# 最低バフ間隔。分数で指定する。ゾーンプラン上でbuffをする指示があっても、前回のバフからこの時間が経過していなければ、バフしない。
MINIMUM_BUFF_INTERVAL=30

# ldonrunnerで表示するバフの文字色。
# 色名は以下を参照のこと。http://www.science.smith.edu/dftwiki/images/3/3d/TkInterColorCharts.png
BUFF_COLOR=Bottle of Shared Adventure I/pale goldenrod
BUFF_COLOR=Bottle of Shared Adventure II/pale goldenrod
BUFF_COLOR=Bottle of Shared Adventure III/pale goldenrod
BUFF_COLOR=Bottle of Shared Adventure IV/pale goldenrod
BUFF_COLOR=Bottle of Shared Adventure V/khaki
BUFF_COLOR=Bottle of Advanced Shared Adventure I/light goldenrod
BUFF_COLOR=Bottle of Advanced Shared Adventure II/light goldenrod
BUFF_COLOR=Bottle of Advanced Shared Adventure III/light goldenrod
BUFF_COLOR=Bottle of Advanced Shared Adventure IV/light goldenrod
BUFF_COLOR=Bottle of Advanced Shared Adventure V/Gold
BUFF_COLOR=Bottle of Advanced Shared Adventure VI/Gold
BUFF_COLOR=Lesson of the Devoted/Gold

# 特定のゾーンにいるときは、移動時の目標地点にランダム値を加味する。大きめにとると、キャラクター同士がスタックしにくくなる。
DEST_LOC_SALT=hohonora/5.0
DEST_LOC_SALT=poknowledge/3.0

# ldonrunnerに認識させないキャラクター名。売り子などを登録する。
IGNORE_CHARACTER = Irako
IGNORE_CHARACTER = Enraku

# 定期的に/hidec npcするモードをオンにしたときに、hidecする間隔（秒数）。
PERIODIC_HIDE_CORPSE_INTERVAL = 30
