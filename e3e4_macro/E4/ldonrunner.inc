|========
| Ldon Runner assistant.
| yell /bc something when combatstate change is detected.


sub LdonRunnerAssistant_Setup

	| ldonrunner status on client side
	| TRUE - Enabled
	| FALSE - Disabled
	/call iniToVarV "${CharacterIni},Zuru,ldonrunner (On/Off)" lrunEnable bool outer
	/if (!${Defined[lrunEnable]}) /declare lrunEnable bool outer FALSE

	| last state
	| 0 - not in combat
	| 1 - started combat.
	/declare lrunLastState int outer 0
	
	| ldonrunner EQBCS client name
	/declare lrunDummyName string outer ldonrunner

    | tell ldonrunner macro version.
	/call LdonRunnerAssistant_CheckLdonRunner
	/if (${Macro.Return}) {
		/echo Found ldonrunner is up.
	    /bct ${lrunDummyName} My backend is E4.
	} else {
		/echo LdonRunner: ldonrunner is not up.
	}

	/if (${lrunEnable}) {
		/echo LdonRunnder: setup done.
	}
/RETURN

#EVENT findLdonRunner "# - Names: #1#."
SUB EVENT_findLdonRunner(line, memberList)
	/declare lr_memberlist string outer ${memberList}
/RETURN

SUB LdonRunnerAssistant_CheckLdonRunner
	/if (${Defined[lr_memberlist]}) /deletevar lr_memberlist
	/squelch /bccmd names
	|/delay 1
	/doevents findLdonRunner
	/if (${Defined[lr_memberlist]}) {
		/if (${lr_memberlist.Find[${lrunDummyName}]}) {
			/return TRUE
		}
	}
/RETURN FALSE

| detect ldonrunnder started or stopped
#EVENT detectLRControl "<#1#> lrwakeup"
#EVENT detectLRControl "<#1#> lrstop"
SUB EVENT_detectLRStarts(line, ChatSender)
	/if (${line.Find[lrwakeup]}) {
		/bc e4 ldonrunner ready.
		/varset lrunEnable TRUE
	} else /if (${line.Find[lrstop]}) {
		/bc e4 ldonrunner stopped.
		/varset lrunEnable FALSE
	}
/RETURN

SUB LdonRunnerAssistant_Background_Events
	/doevents detectLRControl

	/if (${lrunEnable}) {
		/if (${InCombat} && ${lrunLastState} == 0) {
			/bct ${lrunDummyName} I supposed to start a combat.
			/varset lrunLastState 1
		}
		
		/if (!${InCombat} && ${lrunLastState} == 1) {
			/bct ${lrunDummyName} I supposed to end a combat.
			/varset lrunLastState 0
		}

		| answer if ldonrunner asks what's going on
		/doevents answerForPolling
		/doevents answerForVersionCheck
	}
/RETURN


| Answer for polling
#EVENT answerForPolling "<#1#> lrwazzup"
SUB EVENT_answerForPolling(line, ChatSender)
    /if (${InCombat}) {
    	/bct ${lrunDummyName} I supposed to start a combat.
    	/varset lrunLastState 1
    }
    /if (!${InCombat}) {
    	/bct ${lrunDummyName} I supposed to end a combat.
    	/varset lrunLastState 0
    }
/RETURN

| Answer for version check
#EVENT answerForVersionCheck "<#1#> lrcheckbackend"
SUB EVENT_answerForVersionCheck(line, ChatSender)
    | tell ldonrunner macro version.
    /bct ${lrunDummyName} My backend is E4.
/RETURN
