| Ldon Runner assistant.
| yell /bc something when combatstate change is detected.


sub LdonRunnerAssistant_Setup
	| last state
	| 0 - not in combat
	| 1 - started combat.
	/declare lrunLastState int outer 0
	
	| ldonrunner EQBCS client name
	/declare lrunDummyName string outer ldonrunner

    | tell ldonrunner macro version.
    /bct ${lrunDummyName} My backend is E3.

	/echo LR setup loaded.
/RETURN

sub LdonRunnerAssistant_Background_Events
    /if (${Me.CombatState.Equal[COMBAT]} && ${lrunLastState} == 0) {
    	/bct ${lrunDummyName} I supposed to start a combat.
    	/varset lrunLastState 1
    }
    
    /if (${Me.CombatState.NotEqual[COMBAT]} && ${lrunLastState} == 1) {
    	/bct ${lrunDummyName} I supposed to end a combat.
    	/varset lrunLastState 0
    }

	| answer if ldonrunner asks what's going on
	/doevents answerForPolling

	| answer if ldonrunner asks macro version
	/doevents answerForVersionCheck
/RETURN


| Answer for polling
#event answerForPolling "<#1#> lrwazzup"
SUB event_answerForPolling(line, ChatSender)
    /if (${Me.CombatState.Equal[COMBAT]}) {
    	/bct ${lrunDummyName} I supposed to start a combat.
    	/varset lrunLastState 1
    }
    /if (${Me.CombatState.NotEqual[COMBAT]}) {
    	/bct ${lrunDummyName} I supposed to end a combat.
    	/varset lrunLastState 0
    }
/RETURN

| Answer for version check
#event answerForVersionCheck "<#1#> lrcheckbackend"
SUB event_answerForVersionCheck(line, ChatSender)
    | tell ldonrunner macro version.
    /bct ${lrunDummyName} My backend is E3.
/RETURN
