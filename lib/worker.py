# Pythonライブラリ
import threading
import time
import queue
import socket
import tkinter.ttk as ttk
import re
from ttkthemes import ThemedTk
import sys

# ユーザー定義ライブラリ
import lib.gui_mainwindow
import lib.character
import lib.game_controller
import lib.game_settings as gs

PRESENTATION_QUEUE_CHECK_INTERVAL = 20          # ミリ秒
RECEIVING_SOCKET_TIMEOUT = 0.05                 # 秒
SENDING_QUEUE_CHECK_INTERVAL = 0.05             # 秒
STATUS_UPDATING_QUEUE_CHECK_INTERVAL = 0.05     # 秒
GAME_CONTROLLER_QUEUE_CHECK_INTERVAL = 0.05     # 秒

EQBCS_LOGIN_NAME = b'ldonrunner'            # バイト列で指定する。


class ThreadController:
    """
    このプログラムの本体。すべてのスレッドをここから起動し、共用する情報もここで管理する。
    """
    def __init__(self):
        """
        GUIおよび各ワーカースレッドを起動する。実質的に、逐次実行されるのはこのinitの内部のみ。
        """
        # このスレッド管理オブジェクトの状態を管理する。master側から0にセットされると終了する。
        self.running = 1

        # キューの定義
        self.presentation_queue = queue.Queue()         # GUIの表示を更新するための情報を格納する。
        self.status_update_queue = queue.Queue()        # 受信したデータを格納するためのキュー。
        self.send_queue = queue.Queue()                 # 送信するためのデータを格納するためのキュー。
        self.game_controller_queue = queue.Queue()      # ゲームコントローラーに対する指示を行う。

        # ソケットを生成して接続
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.socket.connect(('127.0.0.1', 2112))
        except ConnectionRefusedError:
            print("[WinError 10061] 対象のコンピューターによって拒否されたため、接続できませんでした。")
            sys.exit(-1)

        self.socket.sendall(b'LOGIN=' + EQBCS_LOGIN_NAME + b';')    # 仮想ユーザーでEQBCSに登録。名前がないと情報が送られてこない。

        # GUIスレッドを生成する。GUIの更新指示は、プレゼンテーションキューを介してのみ行う。
        self.master = ThemedTk(theme="equilux")
        self.gui = lib.gui_mainwindow.MainWindow(self.master,
                                                 self.presentation_queue,
                                                 self.game_controller_queue,
                                                 self.end_application)

        # 非同期I/Oを実行するスレッドを生成する。各スレッド内からこのクラスのメンバ変数を参照できるが、
        # ミューテックスを保証するために、スレッド間の通信はキューのみを使って行う必要がある。
        # スレッドをロックするためのオブジェクトを生成。原則として、各種ステータス値の変更・参照はロックを獲得してから行う必要がある。
        self.thread_lock = threading.Lock()

        # グループの生成。プレゼンテーションキューへの参照を渡しておく。
        self.group = lib.character.Group(EQBCS_LOGIN_NAME,
                                         self.presentation_queue,
                                         self.status_update_queue,
                                         self.thread_lock)

        # ゲームコントローラーの生成。
        self.game_controller = lib.game_controller.GameController(self.group,
                                                                  self.presentation_queue,
                                                                  self.send_queue,
                                                                  self.thread_lock)

        # 受信スレッドの起動。
        self.thread1 = threading.Thread(target=self.receiving_thread)
        self.thread1.setDaemon(True)    # 親プロセスが落ちると子も落ちるようにする
        self.thread1.start()

        # 送信スレッドの起動。
        self.thread2 = threading.Thread(target=self.sending_thread)
        self.thread2.setDaemon(True)    # 親プロセスが落ちると子も落ちるようにする
        self.thread2.start()

        # ゲーム管理スレッドの起動。
        self.thread3 = threading.Thread(target=self.game_controlling_thread)
        self.thread3.setDaemon(True)    # 親プロセスが落ちると子も落ちるようにする
        self.thread3.start()

        # ステータス管理スレッドの起動。
        self.thread4 = threading.Thread(target=self.status_updating_thread)
        self.thread4.setDaemon(True)    # 親プロセスが落ちると子も落ちるようにする
        self.thread4.start()

        # GUIにキューを処理させるための定期処理を登録する。
        self.periodic_call()
        self.master.mainloop()

    def periodic_call(self):
        """
        QUEUE_CHECK_INTERVAL(ms) ごとに、GUIの更新処理を要求する。また、このプログラムを停止すべきかどうかを判断する。
        """
        # GUIの更新処理を要求する。具体的にどう更新するかは、プレゼンテーションキューで指示する。
        self.gui.process_incoming()
        self.group.check_timed_events()

        if not self.running:
            print("stopping...")
            # GUI側のmainloopを停止する。
            self.master.destroy()

            # プログラム全体を停止する。
            import sys
            sys.exit()

        # Tkのメインループに、次回のこの関数の呼び出しを登録する。
        self.master.after(PRESENTATION_QUEUE_CHECK_INTERVAL, self.periodic_call)

    def end_application(self):
        """
        GUI側にコールバック関数として登録し、ここを呼び出すことで、次回のperiodic_call時にプログラムを終了させるようにする。
        ここでいきなり終了させないようにするのは、受信キューをクリアしてから終了させたいため。
        :return:
        """
        self.running = 0

    def receiving_thread(self):
        """
        受信スレッド。無限ループとして実行する。このスレッドはデーモンとして実行し、呼び出し元の終了時に自動的に終了するようにする。
        """
        while True:
            try:
                self.socket.settimeout(RECEIVING_SOCKET_TIMEOUT)        # 設定は秒
                _data = self.socket.recv(4096)
                if not _data:
                    break

                # GUIへデータを送信するキューに、受信したデータを詰める。
                self.status_update_queue.put(_data)

            except socket.timeout:
                pass
            except ConnectionResetError:
                # EQBCSが終了すると、ConnectionResetError: [WinError 10054] 既存の接続はリモート ホストに強制的に切断されました。が発出される。
                # なんか対応しよう。
                pass

    def sending_thread(self):
        """
        送信スレッド。無限ループとして実行する。このスレッドもデーモンとして実行し、呼び出し元の終了時に自動的に終了するようにする。
        self.socket.send(b'\tTELL\nyuudachi //hail\n')         # 送信の例。
        self.socket.send(b'\tNAMES\n')                         # 送信の例。
        """
        while True:
            while self.send_queue.qsize():
                try:
                    _msg = self.send_queue.get(0)
                    #print("seinding msg", _msg)
                    try:
                        self.socket.send(bytes(_msg, encoding='UTF-8'))
                    except socket.timeout:
                        pass

                except queue.Empty:
                    # just on general principles, although we don't
                    # expect this branch to be taken in this case
                    pass

            time.sleep(SENDING_QUEUE_CHECK_INTERVAL)

    def game_controlling_thread(self):
        """
        ここでキャラクターをコントロールする。
        :return:
        """
        while True:
            while self.game_controller_queue.qsize():
                try:
                    _msg = self.game_controller_queue.get()
                    self.game_controller.process_queue(_msg)
                    # ここで何かをする。ここからは、グループ->キャラクター、GUI、ともども見える。
                except queue.Empty:
                    pass

            # キューを処理しようがしまいが、実行すべきことはここで実行する。Planの駆動とか。
            self.game_controller.update()
            time.sleep(GAME_CONTROLLER_QUEUE_CHECK_INTERVAL)

    def status_updating_thread(self):
        """
        ここで受け取ったデータを解釈し、ステータスをアップデートする。
        いまのところ受信スレッドと挙動が一対一に対応するが、受信スレッドとは責務を分割する。
        :return:
        """
        # ldonrunnerにbctで送られてくるものは、キャラクター名が[]で囲まれている。
        # クライアントが/bcでブロードキャストするものは、キャラクター名が<>で囲まれている。
        _re_nbpkt = re.compile(r'NBPKT:([a-zA-Z]+):\[NB\](.+)\[NB\]')
        _re_nbjoin = re.compile(r'NBJOIN=([a-zA-Z]+)')
        _re_nbclientlist = re.compile(r'NBCLIENTLIST=([a-zA-Z ]+)')
        _re_nbquit = re.compile(r'NBQUIT=([a-zA-Z ]+)')
        _re_tell = re.compile(b'<([a-zA-Z ]+)> Tell from ([a-zA-Z ]+): (.+)')
        _re_combat_start = re.compile(b'\[([a-zA-Z ]+)\] I supposed to start a combat\.')
        _re_combat_end = re.compile(b'\[([a-zA-Z ]+)\] I supposed to end a combat\.')
        _re_e3_backend = re.compile(b'\[([a-zA-Z ]+)\] My backend is E3\.')
        _re_e4_backend = re.compile(b'\[([a-zA-Z ]+)\] My backend is E4\.')
        _re_generic_bc = re.compile(b'<([a-zA-Z ]+)> (.+)')

        while True:
            while self.status_update_queue.qsize():
                try:
                    # ここでステータスを更新する。ステータスの更新は、ミューテックスを要する。
                    _byte_msg = self.status_update_queue.get(0).strip()
                    _byte_packets = _byte_msg.split(b'\n')
                    _packets = ((_x.decode('utf-8'), _x) for _x in _byte_packets)

                    # NBJOINとかNBPKTとかで始まるデータ単位を、一つずつ処理
                    for _packet in _packets:
                        _text_packet = _packet[0].strip()
                        _byte_packet = _packet[1].strip()

                        if _match := _re_nbpkt.match(_text_packet):
                            """
                            このケースは、キャラクターのHPやMana、バフ、位置情報などのステータスを含むパケット。
                            """
                            _temp = _match.groups()

                            # 無視対象キャラクター出なかった場合だけ、処理する。
                            if not _temp[0].upper() in gs.ignore_character:
                                self.group.update_member(_temp[0], _temp[1])

                        elif _match := _re_nbquit.match(_text_packet):
                            """
                            キャラクターがログアウトしたことを知らせるパケット。リンクデッドの場合は送られてこない。
                            """
                            _temp = _match.groups()
                            # 無視対象キャラクター出なかった場合だけ、処理する。
                            if not _temp[0].upper() in gs.ignore_character:
                                print('[QUIT DETECTED] %s' % _temp[0])
                                self.group.delete_member(_temp[0])
                                self.presentation_queue.put(('CHAR_QUIT', _temp[0]))

                        elif _match := _re_tell.match(_byte_packet):
                            _temp = _match.groups()
                            print("[TELLS FROM] %s [TO] %s [BODY] %s" % (_temp[1], _temp[0], _temp[2]))
                            self.presentation_queue.put(('TELL_RECEIVED', (_temp[1], _temp[0], _temp[2])))

                        elif _match := _re_combat_start.match(_byte_packet):
                            """戦闘開始が報告された。"""
                            _temp = _match.groups()
                            # 無視対象キャラクターでなかった場合だけ、処理する。
                            if not _temp[0].upper() in gs.ignore_character:
                                print("[%s] reported start of a combat." % (_temp[0]))
                                self.game_controller_queue.put(("COMBAT STARTED", _temp[0].decode('utf-8')))
                                self.presentation_queue.put(("COMBAT STARTED", _temp[0].decode('utf-8')))

                        elif _match := _re_combat_end.match(_byte_packet):
                            """戦闘終了が報告された。"""
                            _temp = _match.groups()
                            # 無視対象キャラクターでなかった場合だけ、処理する。
                            if not _temp[0].upper() in gs.ignore_character:
                                print("[%s] reported end of a combat." % (_temp[0]))
                                self.game_controller_queue.put(("COMBAT ENDED", _temp[0].decode('utf-8')))
                                self.presentation_queue.put(("COMBAT ENDED", _temp[0].decode('utf-8')))

                        elif _match := _re_e3_backend.match(_byte_packet):
                            """E3バックエンドの使用が報告された。"""
                            _temp = _match.groups()
                            # 無視対象キャラクターでなかった場合だけ、処理する。
                            if not _temp[0].upper() in gs.ignore_character:
                                self.game_controller_queue.put(("SET E3 BACKEND", _temp[0].decode('utf-8')))

                        elif _match := _re_e4_backend.match(_byte_packet):
                            """E4バックエンドの使用が報告された。"""
                            _temp = _match.groups()
                            # 無視対象キャラクターでなかった場合だけ、処理する。
                            if not _temp[0].upper() in gs.ignore_character:
                                self.game_controller_queue.put(("SET E4 BACKEND", _temp[0].decode('utf-8')))

                        elif _match := _re_generic_bc.match(_byte_packet):
                            """EQクライアント側からユーザーコマンドとして送られてくるものは種類が多いため、個別の正規表現は作らない。"""
                            _temp = _match.groups()
                            # 無視対象キャラクターでなかった場合だけ、処理する。
                            if not _temp[0].upper() in gs.ignore_character:
                                print("[GENERIC BC FROM] %s [BODY] %s" % (_temp[0], _temp[1]))

                                if len(_temp) == 2 and _temp[1][:11] == b'i am mapper':
                                    # GUIに、マッパーの設定を行う。
                                    self.presentation_queue.put(('SET_MAPPER', _temp[0]))

                                elif len(_temp) == 2 and _temp[1][:10] == b'maploc add':
                                    # /bc maploc add ${Me.Loc}の結果を受ける
                                    self.presentation_queue.put(('RECORD_LOCATION', _temp))

                                elif len(_temp) == 2 and _temp[1][:25] == b'maploc find closest point':
                                    # キャラクターに一番近いmainパスの点にカーソルを移動する
                                    self.presentation_queue.put(('FIND_CLOSEST_MAIN_PATH_LOCATION', _temp))

                                elif len(_temp) == 2 and _temp[1][:14] == b'i am main tank':
                                    self.game_controller_queue.put(("SET TANK", _temp[0].decode('utf-8')))
                                    self.presentation_queue.put(("SET TANK", _temp[0].decode('utf-8')))

                                elif len(_temp) == 2 and _temp[1][:16] == b'i am main buffer':
                                    self.game_controller_queue.put(("SET BUFFER", _temp[0].decode('utf-8')))
                                    self.presentation_queue.put(("SET BUFFER", _temp[0].decode('utf-8')))

                                elif len(_temp) == 2 and _temp[1][:10] == b'lrhidec on':
                                    self.game_controller_queue.put(("SET HIDEC ON", _temp[0].decode('utf-8')))

                                elif len(_temp) == 2 and _temp[1][:11] == b'lrhidec off':
                                    self.game_controller_queue.put(("SET HIDEC OFF", _temp[0].decode('utf-8')))

                                elif len(_temp) == 2 and _temp[1][:16] == b'lrresetbufftimer':
                                    self.game_controller_queue.put(("RESET BUFF TIMER", _temp[0].decode('utf-8')))

                                else:
                                    print("UNCAUGHT(GENERIC):", _temp)

                        else:
                            # 拾えなかったタイプのメッセージ。バイト形式のまま、stdoutに出力する。
                            print("UNCAUGHT:", _byte_packet, _text_packet)
                            self.presentation_queue.put(('WRITE_TO_CONSOLE', _byte_packet))

                except queue.Empty:
                    pass

            time.sleep(STATUS_UPDATING_QUEUE_CHECK_INTERVAL)
