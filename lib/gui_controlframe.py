import tkinter as tk
import tkinter.ttk as ttk
import datetime

# ユーザー定義ライブラリ
from lib.gui_settings import *


class ControlFrame(ttk.Frame):
    """
    ボタンの詰め合わせ。
    """
    def __init__(self, master, game_controller_queue):
        super(ControlFrame, self).__init__()

        self.root = master
        self.configure(relief="ridge",
                       width=CHAR_FRAME_WIDTH)

        self.mode = None

        # ゲームコントローラーキューを保持。
        self.game_controller_queue = game_controller_queue

        # ボタン類を追加。
        # プレイモードを開始。
        self.btn_play_mode = ttk.Button(self,
                                        text='Play Mode',
                                        command=self.start_play_mode)
        self.btn_play_mode.grid(row=0, column=0, columnspan=3, sticky=tk.W + tk.E)

        # エディットモードを開始。
        self.btn_edit_mode = ttk.Button(self,
                                        text='Edit Mode',
                                        command=self.start_edit_mode)
        self.btn_edit_mode.grid(row=0, column=3, columnspan=3, sticky=tk.W + tk.E)

        # separator
        self.sepparator = ttk.Separator(self, orient='horizontal')
        self.sepparator.grid(row=1, column=0, columnspan=6, sticky=tk.W + tk.E)

        ### タイマー関係 ###
        # Clockという表示。
        self.lb_clock = ttk.Label(self,
                                  text="Clock (now)",
                                  font=(SMALL_FONT, 8),
                                  foreground=WINDOW_TEXT_FG,
                                  anchor=tk.W)
        self.lb_clock.grid(row=2, column=0, columnspan=1, sticky=tk.W + tk.E)

        # 現在時刻の表示。
        self.en_clock = ttk.Entry(self, width=6)
        self.en_clock.insert(0, '00:00:00')
        self.en_clock.configure(font=(SMALL_FONT, 8),
                                background=TEXT_BOX_BG,
                                foreground=CLOCK_COLOR,
                                state='readonly')
        self.en_clock.grid(row=2, column=1, columnspan=5, sticky=tk.W + tk.E)

        # Early Exit label。
        self.lb_early_exit_minutes = ttk.Label(self,
                                               text="Safe gate if remains <",
                                               font=(SMALL_FONT, 8),
                                               foreground=WINDOW_TEXT_FG,
                                               anchor=tk.W)
        self.lb_early_exit_minutes.grid(row=3, column=0, columnspan=2, sticky=tk.W + tk.E)

        # 早期脱出分数の表示。
        self.en_early_exit_minutes = ttk.Entry(self, width=6)
        self.en_early_exit_minutes.insert(0, '0')
        self.en_early_exit_minutes.configure(font=(SMALL_FONT, 8),
                                             background=TEXT_BOX_BG,
                                             foreground=TEXT_BOX_FG,
                                             state='normal',
                                             justify='right')
        self.en_early_exit_minutes.grid(row=3, column=2, columnspan=3, sticky=tk.W + tk.E)

        # 早期脱出分数の設定ボタン。
        self.btn_early_exit_minutes = ttk.Button(self,
                                                 text='set',
                                                 width=3,
                                                 command=self.set_early_exit_minutes)
        self.btn_early_exit_minutes.grid(row=3, column=5, columnspan=1, sticky=tk.W + tk.E)

        # Timer remains....
        self.lb_remain_time = ttk.Label(self,
                              text="Remain time",
                              font=(SMALL_FONT, 8),
                              foreground=WINDOW_TEXT_FG,
                              anchor=tk.W)
        self.lb_remain_time.grid(row=4, column=0, columnspan=1, sticky=tk.W + tk.E)

        # タイマー残り時間の表示。
        self.en_remain_time = ttk.Entry(self, width=6)
        self.en_remain_time.insert(0, '00:00')
        self.en_remain_time.configure(font=(SMALL_FONT, 8),
                                      background=TEXT_BOX_BG,
                                      foreground=TEXT_BOX_FG,
                                      state='normal',
                                      justify='right')
        self.en_remain_time.grid(row=4, column=1, columnspan=4, sticky=tk.W + tk.E)

        # タイマー残り時間の設定。
        self.btn_remain_time = ttk.Button(self,
                                          text='set',
                                          width=3,
                                          command=self.set_remain_time)
        self.btn_remain_time.grid(row=4, column=5, columnspan=1, sticky=tk.W + tk.E)

        # タイマーハードリミット（ETA）のラベル.
        self.lb_hard_timer = ttk.Label(self,
                                       text="ETA",
                                       font=(SMALL_FONT, 8),
                                       foreground=WINDOW_TEXT_FG,
                                       anchor=tk.W)
        self.lb_hard_timer.grid(row=5, column=0, columnspan=1, sticky=tk.W + tk.E)

        # タイマーハードリミットの表示および設定。
        self.en_hard_timer = ttk.Entry(self, width=6)
        self.en_hard_timer.insert(0, '24:00')
        self.en_hard_timer.configure(font=(SMALL_FONT, 8),
                                     background=TEXT_BOX_BG,
                                     foreground=TEXT_BOX_FG,
                                     state='normal',
                                     justify='right')
        self.en_hard_timer.grid(row=5, column=1, columnspan=4, sticky=tk.W + tk.E)

        # タイマーハードリミットの設定ボタン。
        self.btn_set_hard_timer = ttk.Button(self,
                                             text='set',
                                             width=3,
                                             command=self.set_hard_timer)
        self.btn_set_hard_timer.grid(row=5, column=5, columnspan=1, sticky=tk.W + tk.E)

        # タイマーのコントロール
        # start
        self.btn_timer_start = ttk.Button(self,
                                          text='Timer Start',
                                          command=self.start_timer)
        self.btn_timer_start.grid(row=6, column=0, columnspan=3, sticky=tk.W + tk.E)

        # stop
        self.btn_timer_stop = ttk.Button(self,
                                         text='Timer Stop',
                                         command=self.stop_timer)
        self.btn_timer_stop.grid(row=6, column=3, columnspan=3, sticky=tk.W + tk.E)

        # タイマーがオンかオフかの変数。gamecontroller側にもあるが、キューを介してgamecontrollerにオフを送信しても、
        # こちらのスレッドからみてすぐには更新されない。即時に表示状態を変えたい場合はここにフラグを持ち、gamecontroller側からの
        # 更新要求を却下する必要がある。
        self.is_timer_on = False

    def start_play_mode(self):
        self.mode = 'play'
        # エディットウィンドウが開いていたら、隠す。
        self.root.editor_frame.grid_forget()
        # プレイウィンドウを開く。
        self.root.play_frame.grid(row=2, column=1, columnspan=3, rowspan=5, sticky=tk.N + tk.W + tk.E)

    def start_edit_mode(self):
        self.mode = 'edit'
        # プレイウィンドウが開いていたら、隠す。
        self.root.play_frame.grid_forget()
        # エディットウィンドウを開く。
        self.root.editor_frame.grid(row=2, column=1, columnspan=3, rowspan=5, sticky=tk.N + tk.W + tk.E)

    def set_early_exit_minutes(self):
        """早期脱出をする分数を設定する。"""
        _early_exit_minutes = self.en_early_exit_minutes.get()

        if not _early_exit_minutes.isdigit():
            print("早期脱出の分数が、整数で指定されていません。")
            self.en_early_exit_minutes.delete(0, tk.END)
            self.en_early_exit_minutes.insert(0, '0')
            return

        # ゲームコントローラーに、早期脱出タイマーの新しい値を送信する。
        self.game_controller_queue.put(('SET EARLY EXIT MINUTES', int(_early_exit_minutes)))

    def set_remain_time(self):
        """残り時間を設定し、ハードリミットを洗い替える。"""
        _verify, _hour, _min = self.convert_time_format(self.en_remain_time.get())

        if not _verify:
            self.en_remain_time.delete(0, tk.END)
            self.en_remain_time.insert(0, '00:00')
            return

        # 現在時刻の取得
        _now = datetime.datetime.now()
        _hour_now = _now.hour
        _minute_now = _now.minute

        _new_hard_timer_min = _minute_now + _min
        _new_hard_timer_hour = _hour_now + _hour
        _new_hard_timer_day = 0                     # n日後

        # 分の桁上がり処理
        if _new_hard_timer_min > 59:
            _new_hard_timer_hour += _new_hard_timer_min // 60
            _new_hard_timer_min = _new_hard_timer_min % 60

        # 時の桁上がり処理。
        if _new_hard_timer_hour > 23:
            _new_hard_timer_day += _new_hard_timer_hour // 24
            _new_hard_timer_hour = _new_hard_timer_hour % 24

        # ETAを再設定。
        self.en_hard_timer.delete(0, tk.END)
        self.en_hard_timer.insert(0, '%02d:%02d' % (_new_hard_timer_hour, _new_hard_timer_min))

        self.game_controller_queue.put(('SET HARD TIMER',
                                        _new_hard_timer_day,
                                        _new_hard_timer_hour,
                                        _new_hard_timer_min))

    def set_hard_timer(self):
        """ハードリミットを設定し、残り時間を洗い替える。"""
        _verify, _hard_hour, _hard_min = self.convert_time_format(self.en_hard_timer.get())

        if not _verify:
            self.en_hard_timer.delete(0, tk.END)
            self.en_hard_timer.insert(0, '24:00')
            return

        # 現在時刻よりも24時間表示で早い時刻が指定されていたら、明日の時刻になる。
        _now = datetime.datetime.now()
        _hour_now = _now.hour
        _minute_now = _now.minute

        if _hour_now * 60 + _minute_now > _hard_hour * 60 + _hard_min:
            # 終了は翌日になる。
            _hard_day = 1
        else:
            _hard_day = 0

        # ゲームコントローラーに、新しいハードリミットの値を送信する。
        self.game_controller_queue.put(('SET HARD TIMER', _hard_day, _hard_hour, _hard_min))

        # 残り時間の計算。
        _target_day = _now + datetime.timedelta(days=_hard_day)
        _hard_timer = _target_day.replace(hour=_hard_hour, minute=_hard_min)
        _remain_timedelta = _hard_timer - _now
        _remain_secs = _remain_timedelta.total_seconds()

        _remain_hours = int(_remain_secs / 3600)
        _remain_mins = int(_remain_secs / 60) % 60

        # 表示の更新。
        self.en_remain_time.configure(state='normal')
        self.en_remain_time.delete(0, tk.END)
        self.en_remain_time.insert(0, "%02d:%02d" % (_remain_hours, _remain_mins))
        self.en_remain_time.configure(state='readonly')

    def start_timer(self):
        """タイマーの開始。"""
        self.is_timer_on = True

        # GUIの編集を不許可にする。
        self.en_early_exit_minutes.configure(state='readonly')
        self.en_remain_time.configure(state='readonly')
        self.en_hard_timer.configure(state='readonly')

        # ゲームコントローラーに、タイマーの開始を通達する。
        self.game_controller_queue.put(('START HARD TIMER', None))

    def stop_timer(self):
        """タイマーの停止。"""
        self.is_timer_on = False

        # ゲームコントローラーに、タイマーの停止を通達する。
        self.game_controller_queue.put(('STOP HARD TIMER', None))

        # GUIの編集を許可する。
        self.en_early_exit_minutes.configure(state='normal')
        self.en_remain_time.configure(state='normal')
        self.en_hard_timer.configure(state='normal')

        # 残り時間の色を元に戻す。
        self.en_remain_time.configure(foreground=TEXT_BOX_FG)
        _text = self.en_remain_time.get()
        self.en_remain_time.delete(0, tk.END)
        self.en_remain_time.insert(0, _text[0:-3])

    def convert_time_format(self, time_text):
        """時間／時刻がhh:mm形式で設定されているかをベリファイし、OKならhh:mmを返す。"""
        if not time_text:
            print("未設定の時間／時刻テキストに対してパースを実施しようとしました。")
            return False, 0, 0

        _splitted_time_text = time_text.split(":")

        if len(_splitted_time_text) != 2:
            print("時間／時刻テキストが、hh:mm形式にコロンで区切られていません。")
            return False, 0, 0

        if not _splitted_time_text[0].isdigit():
            print("時間が正の整数になっていません。")
            return False, 0, 0

        if not _splitted_time_text[1].isdigit():
            print("分が正の整数になっていません。")
            return False, 0, 0

        _hour = int(_splitted_time_text[0])
        _min = int(_splitted_time_text[1])

        return True, _hour, _min

    def clock_update(self, datetime_text):
        """時計のアップデート。"""
        self.en_clock.configure(state='normal')
        self.en_clock.delete(0, tk.END)
        self.en_clock.insert(0, datetime_text)
        self.en_clock.configure(state='readonly')

    def remain_time_update(self, remain_time_text):
        """残り時間のアップデート。"""
        if self.is_timer_on:
            self.en_remain_time.configure(state='normal')
            self.en_remain_time.configure(foreground=REMAIN_TIME_COUNTING_COLOR)
            self.en_remain_time.delete(0, tk.END)
            self.en_remain_time.insert(0, remain_time_text)
            self.en_remain_time.configure(state='readonly')
