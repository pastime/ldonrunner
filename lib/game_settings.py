# トップディレクトリのsettings.txtを読み込んで、各種グローバル設定を定義する。
# あらかじめデフォルトの設定値を辞書として登録しておき、そこからローカル変数を

# デフォルトの設定値。
setting = {
    'ARRIVAL_WAIT_SEC': 5.0,
    'ARRIVAL_DISTANCE_THRESHOLD': 20.0,
    'STARTBATTLE_REFRESH_SEC': 4.0,
    'MAX_SEEK_HEAD_CANDIDATES': 3,
    'MAX_ROLLBACK_WAYPOINTS_FOR_EZ_RECOVERY': 8,
    'MAP_PATH': "",
    'PERIODIC_HIDE_CORPSE_INTERVAL': 30,
    'MINIMUM_BUFF_INTERVAL': 30,
    'DIAGONAL_WALK_MIN_DEGREE': 15,
    'DIAGONAL_WALK_MAX_DEGREE': 60,
    'DIAGONAL_WALK_SETBACK_START': 15,
    'DIAGONAL_WALK_SETBACK_INCREMENT': 5,
    'DIAGONAL_WALK_SETBACK_MAX': 60,
    'CHECK_BATTLE_STATUS_AFTER_THIS_SECONDS': 10,
    'E3_COMBAT_COMMAND': '//bc startbattle',
    'E4_COMBAT_COMMAND': '//bc autobattle',
    'DEFAULT_BACKEND': 'E3',
    'BACKEND_POLLING_INTERVAL': 30,
    'KEYMAP_LOOKUP': 'Page_Up',
    'KEYMAP_LOOKDOWN': 'Page_Down',
    'LOOKUP_DOWN_ON_RECOVERY': 1,
}

buff_color = {}
ignore_character = set()
dest_loc_salt = {}


# 設定ファイルの内容を読み込む
with open('settings.txt', 'r', encoding='utf-8') as _fd:
    for _line in _fd:
        _line = _line.strip()

        # 空行もしくは#で開始する場合は、スキップする。
        if len(_line) == 0 or _line[0] == "#":
            continue

        _key_and_value = _line.split("=")
        if len(_key_and_value) != 2:
            print("設定行%sは、key=valueの形式になっていません。" % _line)
            continue

        _key, _value = _key_and_value
        _key = _key.strip()
        _value = _value.strip()

        # 値が十進数だったら、intにする。
        if _value.isdecimal():
            _value = int(_value)
        else:
            # 浮動小数点数に変換できたら、flaotにする。
            try:
                _value = float(_value)
            except ValueError:
                # できなければ、文字列のままとする。
                pass

        # 辞書settingに同名のキーを持つエントリが存在したら、valueで更新する。
        if _key in setting.keys():
            setting[_key] = _value
            continue

        # 設定がBUFF_COLOR=だったら、バフの色の設定とみなす。
        if _key == 'BUFF_COLOR':
            # _valueは、バフ名/色名。
            _buffname_colorname = _value.split('/')

            if len(_buffname_colorname) != 2:
                print("バフ名と色の対応の設定値が、buffname/colorname形式になっていません。", _value)

            _buff_name = _buffname_colorname[0].strip()
            _color_name = _buffname_colorname[1].strip()
            _buff_name = _buff_name.replace(' ', '_')       # tkinterのカラータグ名に空白を含められないため_で置き換える。

            # 設定する。
            buff_color[_buff_name] = _color_name

        # 設定がIGNORE_CHARACTER=だったら、無視するキャラクター名とみなす。
        if _key == 'IGNORE_CHARACTER':
            # 大文字で登録。
            ignore_character.add(_value.upper())

        # 設定がDEST_LOC_SALT=だったら、ゾーンごとに移動先に加味するランダム値の設定とみなす。
        if _key == 'DEST_LOC_SALT':
            # _valueは、ショーとゾーンネーム/値。
            _zonename_saltvalue = _value.split('/')

            if len(_zonename_saltvalue) != 2:
                print("ゾーンショートネームとランダム値の設定が、zoneshortname/randomvalue形式になっていません。", _value)

            _zone_short_name = _zonename_saltvalue[0].strip()
            _salt_value = _zonename_saltvalue[1].strip()

            # 設定する。
            try:
                _salt_value = float(_salt_value)
                dest_loc_salt[_zone_short_name] = _salt_value
            except ValueError:
                print("ゾーンごとに加味すべきランダム値が、浮動小数点数として解釈できません。" % _value)
