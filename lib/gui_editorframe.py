import tkinter as tk
import tkinter.ttk as ttk
import tkinter.messagebox as tkmsg
import tkinter.filedialog
#import pathlib
import os
import sys
import re

# ユーザー定義ライブラリ
from lib.tk_tooltip import CreateToolTip
from lib.gui_settings import *
import database.encyclopedia as db
import lib.plan


class EditorFrame(ttk.Frame):
    """
    エディタ機能を実現するためのフレーム。
    """
    def __init__(self, master):
        super(EditorFrame, self).__init__()

        self.root = master
        self.configure(relief="ridge",
                       width=CHAR_FRAME_WIDTH * 3)

        self.zone_plan = lib.plan.ZonePlan(zone_plan_name='new_plan.json',
                                           zone_id=202)                     # 仮にPoK

        # 一般的な変数。
        self.mapper_character_name = 'not yet set'  # 誰がマッパーか。/bc i am mapperで変更。
        self.path_name_selected = 'main'            # 編集中のパス名。
        self.point_cursor = 0                       # パス内の何番目の点を選択しているか。ゼロなのは、点が全く登録されていない場合のみ。

        ################### GUIの定義。まずは左の列から ########################
        # 左の列にボタン類を並べ、右の列にポイントのリストを置きたい。
        # 右の列がgridの何カラム目から開始するかを簡単に管理するため、一時的な変数を置く。
        _right_column_starting = 6

        # ttkのスタイルの変更方法はややこしい。新スタイル名は文字列なので自由につけられそうに思えるが、
        # my.TButtonはTButtonを継承したmyというスタイルという意味で、TButton部分を変えるとエラーになる。
        _style = ttk.Style()
        _style.configure('my.TButton', font=('Helvetica', 8))

        # 現在選択されているキャラクターを表示。
        # Entryにしておいて、ゲーム内から/bc i am mapper等で更新するほうがラク。
        self.en_mapper_name = ttk.Entry(self, width=6)
        self.en_mapper_name.insert(0,
                                   'Mapper: %s' % self.mapper_character_name)
        self.en_mapper_name.configure(font=(SMALL_FONT, 8),
                                      background=TEXT_BOX_BG,
                                      foreground=TEXT_BOX_FG,
                                      state='readonly')
        self.en_mapper_name.grid(row=0, column=0, columnspan=6, sticky=tk.W + tk.E)

        # New, Load, Saveボタンを表示
        self.btn_new = ttk.Button(self,
                                  text='New',
                                  style='my.TButton',
                                  width=2.5,
                                  command=self.make_new_zone_plan)
        self.btn_new.grid(row=1, column=0, columnspan=2, sticky=tk.W + tk.E)

        self.btn_load = ttk.Button(self,
                                   text='Load',
                                   style='my.TButton',
                                   width=2.5,
                                   command=self.load_zone_plan)
        self.btn_load.grid(row=1, column=2, columnspan=2, sticky=tk.W + tk.E)

        self.btn_save = ttk.Button(self,
                                   text='Save',
                                   style='my.TButton',
                                   width=2.5,
                                   command=self.save_zone_plan)
        self.btn_save.grid(row=1, column=4, columnspan=2, sticky=tk.W + tk.E)

        # 新規にリカバリーパスを追加する。
        self.btn_add_new_recovery_path = ttk.Button(self, text='New path', style='my.TButton', width=3)
        self.btn_add_new_recovery_path.grid(row=2, column=0, columnspan=3, sticky=tk.W + tk.E)

        # リカバリーパスを削除する。
        self.btn_delete_recovery_path = ttk.Button(self, text='Del path', style='my.TButton', width=3)
        self.btn_delete_recovery_path.grid(row=2, column=3, columnspan=3, sticky=tk.W + tk.E)

        # Waypointという表示。
        self.lb_waypoint = ttk.Label(self,
                                     text="Waypoint",
                                     font=(SMALL_FONT, 8),
                                     foreground=WINDOW_TEXT_FG,
                                     anchor=tk.W)
        self.lb_waypoint.grid(row=3, column=0, columnspan=6, sticky=tk.W + tk.E)

        # カーソル位置を10個戻す。
        self.btn_cursor_back_ten = ttk.Button(self,
                                              text='<<',
                                              width=1.5,
                                              command=self.tv_point_list_select_10rows_before)
        self.btn_cursor_back_ten.grid(row=4, column=0, sticky=tk.W + tk.E)

        # カーソル位置を1個戻す。
        self.btn_cursor_back_one = ttk.Button(self,
                                              text='<',
                                              width=1,
                                              command=self.tv_point_list_select_previous_row)
        self.btn_cursor_back_one.grid(row=4, column=1, sticky=tk.W + tk.E)

        # カーソル位置を1個進める。
        self.btn_cursor_proceed_one = ttk.Button(self,
                                                 text='>',
                                                 width=1,
                                                 command=self.tv_point_list_select_next_row)
        self.btn_cursor_proceed_one.grid(row=4, column=4, sticky=tk.W + tk.E)

        # カーソル位置を10個進める。
        self.btn_cursor_proceed_ten = ttk.Button(self,
                                                 text='>>',
                                                 width=1.5,
                                                 command=self.tv_point_list_select_10rows_after)
        self.btn_cursor_proceed_ten.grid(row=4, column=5, sticky=tk.W + tk.E)

        # 現在のカーソル位置を表示。
        self.en_cursor = ttk.Entry(self)
        self.en_cursor.insert(0, '0')
        self.en_cursor.configure(font=(SMALL_FONT, 8),
                                 background=TEXT_BOX_BG,
                                 foreground=TEXT_BOX_FG,
                                 width=3,
                                 state='readonly')
        self.en_cursor.grid(row=4, column=2, columnspan=2, sticky=tk.W + tk.E)

        # 現在のパスにポイントを追加/挿入する。
        self.btn_add_point = ttk.Button(self,
                                        text='ADD pt.',
                                        width=4,
                                        command=self.add_point_from_gui)
        self.btn_add_point.grid(row=5, column=0, columnspan=3, sticky=tk.W + tk.E)

        # 現在のポイントを削除する。
        self.btn_delete_point = ttk.Button(self,
                                           text='DEL pt.',
                                           width=4,
                                           command=self.delete_point)
        self.btn_delete_point.grid(row=5, column=3, columnspan=3, sticky=tk.W + tk.E)

        # アクションを付加するというラベル。
        self.lb_add_actions = ttk.Label(self,
                                        text="Add actions to this waypoint",
                                        font=(SMALL_FONT, 8),
                                        foreground=WINDOW_TEXT_FG,
                                        anchor=tk.W)
        self.lb_add_actions.grid(row=6, column=0, columnspan=6, sticky=tk.W + tk.E)

        # 追加するアクションをテキストで定義するための欄。
        self.en_action_by_text = ttk.Entry(self, width=6)
        self.en_action_by_text.insert(0, '')
        self.en_action_by_text.configure(font=(SMALL_FONT, 8),
                                         background=TEXT_BOX_BG,
                                         foreground=TEXT_BOX_FG,
                                         width=8)
        self.btn_update_action_by_text = ttk.Button(self,
                                                    text='UPDATE',
                                                    style='my.TButton',
                                                    width=1.5,
                                                    command=self.en_action_by_text_overwrite_actions)
        self.en_action_by_text.grid(row=7, column=0, columnspan=4, sticky=tk.W + tk.E)
        self.btn_update_action_by_text.grid(row=7, column=4, columnspan=2, sticky=tk.W + tk.E)

        # よくあるアクションを追加する簡便なボタン。
        self.btn_action_shortcut_0 = ttk.Button(self,
                                                text='W',
                                                style='my.TButton',
                                                width=2,
                                                command=self.shortcut_add_action_wait)      # wait3。3秒待て。
        self.btn_action_shortcut_1 = ttk.Button(self,
                                                text='B',
                                                style='my.TButton',
                                                width=2,
                                                command=self.shortcut_add_action_buff)      # buff30。ここでバフせよ。
        self.btn_action_shortcut_2 = ttk.Button(self,
                                                text='H',
                                                style='my.TButton',
                                                width=2,
                                                command=self.shortcut_add_action_hidec)     # hidec1。全員/hidec npcして1秒待て。
        self.btn_action_shortcut_3 = ttk.Button(self,
                                                text='A',
                                                style='my.TButton',
                                                width=2,
                                                command=self.shortcut_add_action_autosell)  # autosell30。オートセルして30秒待て。
        self.btn_action_shortcut_4 = ttk.Button(self,
                                                text='P',
                                                style='my.TButton',
                                                width=2,
                                                command=self.shortcut_add_action_p)         # p: 戦闘が開始しても、次のwaypointまで進め。
        self.btn_action_shortcut_5 = ttk.Button(self,
                                                text='R',
                                                style='my.TButton',
                                                width=2,
                                                command=self.shortcut_add_action_r)         # r: 戦闘が発生したら、手前のwaypointまで戻れ。

        self.btn_action_shortcut_0.grid(row=8, column=0, sticky=tk.W + tk.E)
        self.btn_action_shortcut_1.grid(row=8, column=1, sticky=tk.W + tk.E)
        self.btn_action_shortcut_2.grid(row=8, column=2, sticky=tk.W + tk.E)
        self.btn_action_shortcut_3.grid(row=8, column=3, sticky=tk.W + tk.E)
        self.btn_action_shortcut_4.grid(row=8, column=4, sticky=tk.W + tk.E)
        self.btn_action_shortcut_5.grid(row=8, column=5, sticky=tk.W + tk.E)

        # ツールチップ
        self.ttp_btn_action_shortcut_0 = CreateToolTip(self.btn_action_shortcut_0, 'wait3: wait for 3secs until next move.')
        self.ttp_btn_action_shortcut_1 = CreateToolTip(self.btn_action_shortcut_1, 'buff30: start group buffs and wait for 30secs.')
        self.ttp_btn_action_shortcut_2 = CreateToolTip(self.btn_action_shortcut_2, 'hidec1: do /hidec npc and wait 1sec.')
        self.ttp_btn_action_shortcut_3 = CreateToolTip(self.btn_action_shortcut_3, 'autosell30: start auto sell and wait for 30secs.')
        self.ttp_btn_action_shortcut_4 = CreateToolTip(self.btn_action_shortcut_4, 'p: dont fight here. proceed to next waypoint if combat starts')
        self.ttp_btn_action_shortcut_5 = CreateToolTip(self.btn_action_shortcut_5, 'r: (not implemented yet)')

        # マップを表示、または更新。
        self.btn_show_map = ttk.Button(self, text='Show Map', command=self.root.show_map)
        self.btn_show_map.grid(row=9, column=0, columnspan=3, sticky=tk.W + tk.E)

        ################### ここから右の列 ########################
        # ファイル名を表示。ファイル名の変更が必要なら、セーブ時に行わせる。
        self.en_filename = ttk.Entry(self)
        self.en_filename.insert(0, 'filename: no name')
        self.en_filename.configure(font=(SMALL_FONT, 8),
                                   background=TEXT_BOX_BG,
                                   foreground=TEXT_BOX_FG,
                                   state='readonly')
        self.en_filename.grid(row=0, column=0 + _right_column_starting, columnspan=2, sticky=tk.W + tk.E)

        # 関連するゾーン名を表示。shortnameとlongnameを合わせたものを表示。
        self.en_zone_name = ttk.Entry(self)
        self.en_zone_name.insert(0, '(zone name)')
        self.en_zone_name.configure(font=(SMALL_FONT, 8),
                                    background=TEXT_BOX_BG,
                                    foreground=TEXT_BOX_FG,
                                    state = 'readonly')
        self.en_zone_name.grid(row=1, column=0 + _right_column_starting, columnspan=2, sticky=tk.W + tk.E)

        # 選択されているパス名を表示。
        self.en_current_path_name = ttk.Entry(self)
        self.en_current_path_name.insert(0, 'main')
        self.en_current_path_name.configure(font=(SMALL_FONT, 8),
                                            background=TEXT_BOX_BG,
                                            foreground=TEXT_BOX_FG,
                                            width=8,
                                            state='readonly')
        self.en_current_path_name.grid(row=2, column=0 + _right_column_starting, columnspan=2, sticky=tk.W + tk.E)

        # 進行度を表示するためのツリービュー。
        self.tv_point_list = ttk.Treeview(self, height=7)  # browseは、同時に1つしか選択できない。
        self.tv_point_list = ttk.Treeview(self, height=7, selectmode='browse')  # browseは、同時に1つしか選択できない。
        self.tv_point_list["columns"] = (1, 2, 3, 4, 5)   # 列インデックスの作成
        self.tv_point_list["show"] = "headings"           # 表形式にする
        self.tv_point_list.column(1, width=25, stretch=tk.NO)     # 幅の設定
        self.tv_point_list.column(2, width=60)
        self.tv_point_list.column(3, width=60)
        self.tv_point_list.column(4, width=60)
        self.tv_point_list.column(5, width=174)
        self.tv_point_list.heading(1, text="#")           # ヘッダのテキスト
        self.tv_point_list.heading(2, text="x")
        self.tv_point_list.heading(3, text="y")
        self.tv_point_list.heading(4, text="h")
        self.tv_point_list.heading(5, text="actions")
        self.tv_point_list.grid(row=3, column=0 + _right_column_starting, rowspan=8, sticky=tk.W + tk.E)

        # ツリービューにスクロールバーをつける。
        self.sb_point_list = ttk.Scrollbar(self, orient=tk.VERTICAL, command=self.tv_point_list.yview)
        self.tv_point_list.configure(yscrollcommand=self.sb_point_list.set)
        self.sb_point_list.grid(row=3, column=0 + _right_column_starting + 1, rowspan=8, sticky='nsew')

        # <<TreeviewSelect>>は、yviewが動いたときなどに不意に発動するのでやめる。
        #self.tv_point_list.bind("<<TreeviewSelect>>", self.tv_point_list_click_event)
        self.tv_point_list.bind("<1>", self.tv_point_list_on_click)

        self.point_list_incremental_id = 0          # 現在存在している行の数（ウィジェット）。行オブジェクトは削除しないので、原則増加する。
        self.point_list_current_item_number = 0     # 現在存在している要素の数（値）。要素を削除した場合は減らす。
        self.point_list_detached_id = set()         # 現在非表示になっている行を保存。

        # 最初の更新。
        self.update_zone_plan_widgets()

        # /bc maploc add ${Me.Loc}, ${Me.Height} ADDITIONAL OPTIONS
        self.re_map_loc = re.compile(r"maploc add *(-?[0-9\.]+), *(-?[0-9\.]+), *(-?[0-9\.]+) *([A-Za-z0-9 ]*)")

    def changer_mapper(self, mapper_name):
        """
        マッパーの名前を変える。
        :param mapper_name:
        :return:
        """
        self.mapper_character_name = mapper_name
        self.en_mapper_name.configure(state='normal')
        self.en_mapper_name.delete(0, tk.END)
        self.en_mapper_name.insert(0, 'Mapper: %s' % self.mapper_character_name)
        self.en_mapper_name.configure(state='readonly')

    def tv_point_list_change_focus(self):
        """
        self.point_cursorの位置に、カーソルを移動する。
        単純な移動や点を追加した際は飛び先のROWIDは存在するはずだが、点を削除してカーソルを上に移動させた場合は、
        点が全くなくったときにpoint_cursorが0となるため、この場合はフォーカスを動かさない。
        """
        if self.point_cursor > 0:
            # カーソルを移動させ、その位置が見えるようにスクロールさせる。
            self.tv_point_list.selection_set("ROWID_%d" % self.point_cursor)
            self.tv_point_list.see("ROWID_%d" % self.point_cursor)

        else:
            # 選択すべき行がないので、行選択を解除する。
            self.tv_point_list.selection_clear()

        self.en_action_by_text_update()

    def en_action_by_text_update(self):
        """ツリービューの選択行が変わったときに、アクションの編集欄の情報をアップデートする。"""
        # アクションのテキストを削除する。
        self.en_action_by_text.delete(0, tk.END)

        if self.point_cursor == 0:
            # 設定すべき項目がまだない
            return

        _path = self.zone_plan.get_path(self.path_name_selected)
        _actions = _path.path_points[self.point_cursor - 1].actions
        #print(_path.path_points[self.point_cursor - 1].actions)
        if _actions:
            self.en_action_by_text.insert(0, _path.path_points[self.point_cursor - 1].actions)

    def en_action_by_text_overwrite_actions(self):
        """ゾーンプラン上のアクションを更新し、そののち画面も更新。"""
        if self.point_cursor == 0:
            # 設定すべき項目がまだない
            return

        _path = self.zone_plan.get_path(self.path_name_selected)
        # ユーザー入力文字列をそのまま保持してしまうのはどうなのか。のちのちバリデーション処理が必要。
        # とりあえずテキストであることは保証されているので、エラーにはならないが。
        _path.path_points[self.point_cursor - 1].actions = self.en_action_by_text.get()
        self.tv_point_list_update_item(self.point_cursor - 1)

    def shortcut_add_action(self, an_action):
        """ショートカットボタンからアクションを一つ追加するための、共通機能。"""
        if self.point_cursor == 0:
            # 設定すべき項目がまだない
            return

        _an_action = an_action.strip()  # スペースの数を調整するために、いったんトリムする
        _path = self.zone_plan.get_path(self.path_name_selected)
        _point = _path.path_points[self.point_cursor - 1]

        # 一つスペースを開けて末尾に追加
        if _point.actions:
            # 捨てに同じアクションが登録されていたら、追加しない。
            _actions_already_set = _point.actions.split(';')
            if an_action not in _actions_already_set:
                _point.actions = _point.actions + ";%s" % an_action
        else:
            _point.actions = "%s" % an_action

        self.en_action_by_text_update()
        self.tv_point_list_update_item(self.point_cursor - 1)

    def shortcut_add_action_wait(self):
        self.shortcut_add_action("wait3")

    def shortcut_add_action_buff(self):
        self.shortcut_add_action("buff30")

    def shortcut_add_action_hidec(self):
        self.shortcut_add_action("hidec1")

    def shortcut_add_action_autosell(self):
        self.shortcut_add_action("autosell30")

    def shortcut_add_action_p(self):
        self.shortcut_add_action("p")

    def shortcut_add_action_r(self):
        self.shortcut_add_action("r")

    def tv_point_list_change_focus_by_index(self, index):
        """指定された行番号へカーソルを移動させる。範囲外の場合、端点にてクリップ。"""
        _index = max(1, index)                                      # ゼロ以下の場合は1にする(ROWID_およびpoint_cursorは1開始）。
        _index = min(_index, self.point_list_current_item_number)   # インデックス最大値を超えていたら、最大値にする。
        self.point_cursor = _index
        self.tv_point_list_change_focus()

    def tv_point_list_select_next_row(self):
        self.tv_point_list_change_focus_by_index(self.point_cursor + 1)

    def tv_point_list_select_previous_row(self):
        self.tv_point_list_change_focus_by_index(self.point_cursor - 1)

    def tv_point_list_select_10rows_after(self):
        self.tv_point_list_change_focus_by_index(self.point_cursor + 10)

    def tv_point_list_select_10rows_before(self):
        self.tv_point_list_change_focus_by_index(self.point_cursor - 10)

    def tv_pointer_add_row_if_need(self):
        """
        点が追加された後に呼び出し、ツリービューに行が足りない場合は新規に作成する。
        :return:
        """
        if self.point_list_current_item_number > self.point_list_incremental_id:
            # 欄が足りない場合、アイテムを増やす必要がある。
            _row_id = "ROWID_%d" % (self.point_list_incremental_id + 1)
            self.tv_point_list.insert("",
                                      index="end",
                                      iid=_row_id,    # itemの属性として入ってこない。代わりにtextを使う。これは実質いらない。
                                      text=_row_id,   # これは画面には表示されない。
                                      values=(self.point_list_incremental_id, 0, 0, 0, ""))
            self.point_list_incremental_id += 1

    def tv_point_list_on_click(self, event):
        _item = self.tv_point_list.identify('item', event.x, event.y)
        if _item:
            self.point_cursor = int(_item[6:])  # ROWID_### 形式を想定
            #print("point cursor:", self.point_cursor)
            self.en_action_by_text_update()
        '''
        print('********** tree mouse click event **********')
        print('clicked on', self.tv_point_list.item(_item)['text'])
        print('event.x: %d, event.y: %d' % (event.x, event.y))
        print('******************************************\n')
        '''

    def update_zone_plan_widgets(self, from_index=0):
        """ゾーンプラン関係のウィジェットの表示を更新。ツリービュー以外は、毎回更新しなくてもよいのだが。"""
        # 選択されているファイル名の表示
        self.en_filename.configure(state='normal')
        self.en_filename.delete(0, tk.END)
        self.en_filename.insert(0, '%s' % self.zone_plan.zone_plan_name)
        self.en_filename.configure(state='readonly')

        # 選択されているパス名の表示
        self.en_current_path_name.configure(state='normal')
        self.en_current_path_name.delete(0, tk.END)
        self.en_current_path_name.insert(0, "Path: %s" % self.path_name_selected)
        self.en_current_path_name.configure(state='readonly')

        # ゾーン名の表示
        self.en_zone_name.configure(state='normal')
        self.en_zone_name.delete(0, tk.END)
        self.en_zone_name.insert(0,
                                 "Zone: %s/%s" % (db.get_zone_shortname(self.zone_plan.zone_id),
                                                  db.get_zone_longname(self.zone_plan.zone_id)))
        self.en_zone_name.configure(state='readonly')

        self.tv_point_list_update_item(from_index=from_index)

    def tv_point_list_update_item(self, from_index=0):
        """TreeViewの値の更新。from_indexを指定すると、そこから後の値だけを更新する。"""
        _path = self.zone_plan.get_path(self.path_name_selected)

        if not _path:
            print("パス%sは、現在のゾーンプランには存在しません。" % _path)
            return

        for _ix in range(self.point_list_incremental_id):
            if _ix < from_index:
                continue

            _rowid = 'ROWID_%d' % (_ix + 1)

            if _ix >= self.point_list_current_item_number:
                self.tv_point_list.item(_rowid,
                                        values=("", "", "", "", ""))
                if _rowid not in self.point_list_detached_id:
                    self.tv_point_list.detach(_rowid)           # 隠した状態にする
                    self.point_list_detached_id.add(_rowid)
                continue

            _entry = _path.path_points[_ix]
            _value = (_ix,
                      "%.2f" % _entry.x,
                      "%.2f" % _entry.y,
                      "%.2f" % _entry.h,
                      "" if not _entry.actions else _entry.actions)

            if _rowid in self.point_list_detached_id:
                self.tv_point_list.reattach(_rowid, '', _ix + 1)         # 表示した状態にする
                self.point_list_detached_id.remove(_rowid)

            self.tv_point_list.item(_rowid,
                                    values=_value)

    def add_point(self, x, y, h, actions=None):
        """点の追加。"""
        _path = self.zone_plan.get_path(self.path_name_selected)
        _path.insert_point(self.point_cursor, x, y, h, actions)

        self.point_list_current_item_number += 1
        self.tv_pointer_add_row_if_need()
        self.update_zone_plan_widgets()

        self.point_cursor += 1
        self.tv_point_list_change_focus()

    def add_point_from_text(self, character_name, loc_string):
        """bcコマンドで送信されてきたLocを解釈し、点を追加/挿入する。"""
        if character_name != self.mapper_character_name:
            print("マッパー以外(%s)からのlocの登録は許可されていません。現在のマッパーは%sです。" % (character_name, self.mapper_character_name))
            return

        if self.zone_plan.zone_id != self.root.get_character_pane(self.mapper_character_name).zone_id:
            print("現在マッパーがいるゾーンと、編集しようとしているゾーンが異なります。")
            return

        _result = self.re_map_loc.match(loc_string)
        if not _result:
            print("マップへのloc登録のフォーマットが違います。/bc ${Me.loc}, ${Me.Height}と入力してください。 受信された内容は、%sです。" % loc_string)
        try:
            _result_groups = _result.groups()
            if len(_result_groups) == 3:
                _tmp_y, _tmp_x, _tmp_h = _result_groups   # ゲーム内のloc表示はy,x,hなので注意。
                _tmp_text = None
            if len(_result_groups) == 4:
                _tmp_y, _tmp_x, _tmp_h, _tmp_text = _result_groups   # ゲーム内のloc表示はy,x,hなので注意。
            _x = float(_tmp_x)
            _y = float(_tmp_y)
            _h = float(_tmp_h)
            #print("additional text is...", _tmp_text)

        except Exception as e:
            print("%s:マップにLocを追加しようとしましたが、フォーマットが正しくありませんでした。%s" % (e, loc_string))
            return

        self.add_point(_x, _y, _h, _tmp_text)

    def add_point_from_gui(self):
        """GUIが把握しているマッパーの座標を、点として記録する。/bcコマンドで追加するときよりも精度が悪くなる。"""
        if not self.mapper_character_name:
            print("マッパーが設定されていません。")
            return

        _character_pane = self.root.get_character_pane(self.mapper_character_name)
        if not _character_pane:
            print("マッパーがログインしていません。")
            return

        if self.zone_plan.zone_id != _character_pane.zone_id:
            print("マッパーがマッピング対象のゾーンにいません。")
            return

        print("adding a point...(from gui)")
        self.add_point(_character_pane.x,
                       _character_pane.y,
                       _character_pane.h)

    def delete_point(self):
        """カーソルで指定された点を削除する。"""
        # self.point_cursorは1開始のため、リスト上の点のインデックス+1となっていることに留意する。
        _path = self.zone_plan.get_path(self.path_name_selected)

        if not _path:
            # 既に削除されたリカバリーパスに対して削除を行おうとした。発生するか？
            print("パスの準備ができていません。")
            return

        if self.point_cursor == 0:
            # まだ一度も点が登録されておらず、行が選択されていない。
            print("点が未登録です。")
            return

        if self.point_list_current_item_number == 0:
            print("点がありません。")
            return

        _path.delete_point(self.point_cursor - 1)
        self.point_list_current_item_number -= 1
        if self.point_cursor > 1 or (self.point_cursor == 1 and self.point_list_current_item_number == 0):
            # カーソルが西一番上の行にある場合以外は、カーソルを一つ上に移動させる。
            # 一番上にあり、かつ最後の点を削除した場合は、カーソルの位置をゼロにする。
            # 一番上にあるが、まだ点が残っている場合は、カーソルを一番上に置いたままにする。
            self.point_cursor -= 1
        self.update_zone_plan_widgets()
        self.tv_point_list_change_focus()

    def make_new_zone_plan(self):
        """
        新規にゾーンプランを生成し、現在のものを破棄する。コンファームのダイアログが必要。
        :return:
        """
        if not tkmsg.askokcancel("Confirmation", "Replacing current zone plan with new one. Is it ok?"):
            return

        if self.mapper_character_name not in self.root.characters.values():
            print("マッパーが設定されていないか、ログアウトしています。ゾーンを取得できません。")
            return

        _zone_id = self.root.get_character_pane(self.mapper_character_name).zone_id

        # file_path,basenameは、保存時に変更するのでここでは適当でよい。
        self.zone_plan = lib.plan.ZonePlan(zone_plan_name='new_plan.json',
                                           zone_id=_zone_id)
        self.point_list_current_item_number = 0
        self.update_zone_plan_widgets()
        self.point_cursor = 0

        try:
            # 選択行のリセット。このtry-exceptはどうかと思うが、特定のiidの有無を調べるためにTreeviewの全itemを走査するのは面倒。
            # 起動後何も点を追加せずに新規パスを生成したときだけ、ROWID_1がないという状況があり得る。
            self.tv_point_list.selection_set("ROWID_1")
        except:
            pass

        # カンバスのサイズがゾーンごとに異なるため、MapWindowが存在するときは、いったん閉じたほうが楽。
        if self.root.map_window:
            self.root.destroy_map_window()

        print("ゾーンプランを新規に作成しました。")

    def load_zone_plan(self):
        """ゾーンプランを読み込む。"""
        _initial_dir = os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), 'path')
        _file_type = [("ldonrunner経路ファイル", "*.json")]
        _file_name = tkinter.filedialog.askopenfilename(filetypes=_file_type,
                                                        initialdir=_initial_dir)

        if not _file_name:
            print("キャンセルされました。")
            return

        # ロードする。
        self.zone_plan.load_zone_plan(_file_name)

        # ロード時にツリービューの行が足りない可能性があるため、必要に応じて増やす。
        _main_path_length = len(self.zone_plan.get_path('main').path_points)
        self.point_list_current_item_number = 0
        for _ix in range(_main_path_length):
            self.point_list_current_item_number += 1
            self.tv_pointer_add_row_if_need()

        # 各種パス関連ウィジェットの更新。
        self.update_zone_plan_widgets()

        # 読み込んだゾーンプランのmainパスの長さがゼロの場合がありうる。
        if _main_path_length == 0:
            self.point_cursor = 0
        else:
            self.point_cursor = 1
        self.tv_point_list_change_focus()

    def save_zone_plan(self):
        """ゾーンプランを保存する。"""
        _initial_dir = os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), 'path')
        _initial_filename = self.en_filename.get()
        _file_type = [("ldonrunner経路ファイル", "*.json")]
        _filename = tkinter.filedialog.asksaveasfilename(filetypes=_file_type,
                                                         initialfile=_initial_filename,
                                                         initialdir=_initial_dir)

        if _filename:
            # キャンセルされなかった場合は、ゾーンプランの名前を変更する。
            self.en_filename.configure(state='normal')
            self.en_filename.delete(0, tk.END)
            self.en_filename.insert(0, os.path.basename(_filename))
            self.en_filename.configure(state='readonly')
        else:
            # キャンセルされた場合は、ここで離脱。
            print("キャンセルされました。")
            return

        # 保存処理。
        self.zone_plan.zone_plan_name = os.path.basename(_filename)
        self.zone_plan.save_zone_plan(_filename)
