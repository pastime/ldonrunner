'''
WriteOut("\ar#\ax Commands Available");
    WriteOut("\ar#\ax \ay/bct ToonName //command\ax (send Command to ToonName)");
    WriteOut("\ar#\ax \ay/bcg //command\ax (send Command to your group, EXCLUDING yourself)");
    WriteOut("\ar#\ax \ay/bcga //command\ax (send Command to your group, INCLUDING yourself)");
    WriteOut("\ar#\ax \ay/bca //command\ax (send Command to all connected names EXCLUDING yourself)");
    WriteOut("\ar#\ax \ay/bcaa //command\ax (send Command to all connected names INCLUDING yourself)");
    WriteOut("\ar#\ax \ay/bccmd status\ax (show connected or not and settings)");
    WriteOut("\ar#\ax \ay/bccmd names\ax (show who is connected)");
    WriteOut("\ar#\ax \ay/bccmd channels <channel list>\ax (set list of channels to receive tells from)");
'''


def bct(target, text):
    """一人のキャラクターにbcコマンドを送信する。"""
    # 本家のEQBCSクライアントでも、他のbc系コマンドはこのコマンド＋ループへのエイリアス。
    return '\tTELL\n' + target + ' ' + text + '\n'


def bcg(text, all_toons=True):
    """
    すべてのグループメンバーにbctするだけ。all_toons=Falseの場合、自分を除外。
    :param text:
    :return:
    """
    # 未実装
    pass


def bcga(text):
    """
    bcgから自分を除外。
    :param text:
    :return:
    """
    # 未実装
    bcg(text, all_toons=False)


def bca(text, all_toons=True):
    """
    すべての接続済みキャラクターにbctするだけ。all_toons=Falseの場合、自分を除外。
    :param text:
    :return:
    """
    # 未実装
    pass


def bcaa(text):
    """
    bcgから自分を除外。
    :param text:
    :return:
    """
    # 未実装
    bca(text, all_toons=False)


def bccmd_names():
    """接続しているキャラクターの名前の一覧を要求する。"""
    return b'\tNAMES\n'
