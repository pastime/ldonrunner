import tkinter as tk
import tkinter.ttk as ttk

# ユーザー定義ライブラリ
from lib.gui_settings import *
import lib.game_settings as gs
import database.encyclopedia as db


class CharacterFrame(ttk.LabelFrame):
    """
    メインウィンドウ内に6個並べる、キャラクターごとのペインの定義。
    """
    def __init__(self, master, character_num, character_name):
        super(CharacterFrame, self).__init__()

        self.configure(text=character_num,
                       relief="ridge",
                       width=CHAR_FRAME_WIDTH,
                       height=CHAR_FRAME_HEIGHT)

        # キャラクターがいるゾーンのIDと座標。マップエディット機能から使用する。
        # このCharacterオブジェクトのデータはThreadControllerの管理下にあり、GUIからは直接参照できないようにしているため、
        # ステータス表示の更新時にキュー経由でここで受け取ったものを使う。
        self.zone_id = 0
        self.x = 0
        self.y = 0
        self.h = 0

        self.is_main_tank = False            # メインタンクか
        self.is_main_buffer = False     # メインバファーか

        # 戦闘状態。
        self.combat_state = False

        # レベル、クラス、ID、HP、Mana,Endurance、Petとかを表示。
        self.tb_status = tk.Text(self, width=STATUS_TEXT_WIDTH, height=STATUS_TEXT_LINES)
        self.tb_status.configure(font=(MID_FONT, 10),
                                 bg=TEXT_BOX_BG,
                                 fg=TEXT_BOX_FG,
                                 state='disabled')
        self.tb_status.tag_configure("NAME_COLOR", foreground=STATUS_COLOR_NAME)
        self.tb_status.tag_configure("COMBAT_COLOR", foreground=STATUS_COLOR_COMBAT)
        self.tb_status.tag_configure("HP_COLOR", foreground=STATUS_COLOR_HP)
        self.tb_status.tag_configure("MANA_COLOR", foreground=STATUS_COLOR_MANA)
        self.tb_status.tag_configure("ENDU_COLOR", foreground=STATUS_COLOR_ENDURANCE)
        self.tb_status.tag_configure("TARGET_COLOR", foreground=STATUS_COLOR_TARGET)
        self.tb_status.tag_configure("PET_COLOR", foreground=STATUS_COLOR_PET)
        self.tb_status.tag_configure("CASTING_COLOR", foreground=STATUS_COLOR_CASTING)
        self.tb_status.tag_configure("LOC_COLOR", foreground=STATUS_COLOR_LOC)
        self.tb_status.tag_configure("ZONE_COLOR", foreground=STATUS_COLOR_ZONE)
        self.tb_status.grid(row=0, columnspan=2, sticky=tk.E + tk.W)

        # バフ類を表示
        self.tb_buffs1 = tk.Text(self, width=BUFF_BOX_WIDTH, height=BUFF_BOX_LINES, wrap="none")
        self.tb_buffs2 = tk.Text(self, width=BUFF_BOX_WIDTH, height=BUFF_BOX_LINES, wrap="none")
        self.tb_buffs1.configure(font=(SMALL_FONT, 8),
                                 bg=TEXT_BOX_BG,
                                 fg=TEXT_BOX_FG,
                                 state='disabled')
        self.tb_buffs2.configure(font=(SMALL_FONT, 8),
                                 bg=TEXT_BOX_BG,
                                 fg=TEXT_BOX_FG,
                                 state='disabled')

        # 色の設定。
        for _buff_name, _color_name in gs.buff_color.items():
            try:
                self.tb_buffs1.tag_configure(_buff_name, foreground=_color_name)
                self.tb_buffs2.tag_configure(_buff_name, foreground=_color_name)
            except:
                print("%sは、tkinterの色名にありません。" % _color_name)

        # テスト用ダミー値
        self.tb_status.insert('1.0', '')
        self.tb_buffs1.insert('1.0', '')
        self.tb_buffs2.insert('1.0', '')
        self.tb_buffs1.grid(row=1, column=0, sticky=tk.W)
        self.tb_buffs2.grid(row=1, column=1, sticky=tk.E)

        # キャラクターステートの状態を表示する。
        self.en_character_state = ttk.Entry(self, width=BUFF_BOX_WIDTH)
        self.en_character_state.configure(font=(SMALL_FONT, 8),
                                          background=TEXT_BOX_BG,
                                          foreground=TEXT_BOX_FG,
                                          state='readonly')
        self.en_character_state.grid(row=2, columnspan=2, sticky=tk.W + tk.E)

        # 走行ステートの状態を表示する
        self.en_running_state = ttk.Entry(self, width=BUFF_BOX_WIDTH)
        self.en_running_state.configure(font=(SMALL_FONT, 8),
                                        background=TEXT_BOX_BG,
                                        foreground=TEXT_BOX_FG,
                                        state='readonly')
        self.en_running_state.grid(row=3, columnspan=2, sticky=tk.W + tk.E)

        # 進行度を表示する。
        self.tb_progression = tk.Text(self, width=STATUS_TEXT_WIDTH, height=PROGRESSION_TEXT_LINES)
        self.tb_progression.configure(font=(SMALL_FONT, 8),
                                      bg=TEXT_BOX_BG,
                                      fg=TEXT_BOX_FG,
                                      state='disabled')
        self.tb_progression.grid(row=4, columnspan=2, sticky=tk.E + tk.W)

    def update_status(self, status):
        """ステータス表示用テキストボックスの更新。"""
        self.tb_status.configure(state=tk.NORMAL)
        self.tb_status.delete("1.0", "end")

        if status == {}:
            return

        _name_text = status['name']
        _name_text += '(T)' if self.is_main_tank else ''
        _name_text += '(B)' if self.is_main_buffer else ''
        self.tb_status.insert(tk.END,
                              '%s' % _name_text,
                              'NAME_COLOR')

        self.tb_status.insert(tk.END,
                              '\n' if not self.combat_state else ' (in combat!)\n',
                              'COMBAT_COLOR')

        self.tb_status.insert(tk.END,
                              'HP   %5d/%5d\n' % (status['hp'], status['max_hp']),
                              'HP_COLOR')

        self.tb_status.insert(tk.END,
                              'MANA %5d/%5d\n' % (status['mana'], status['max_mana']),
                              'MANA_COLOR')

        self.tb_status.insert(tk.END,
                              'END  %5d/%5d\n' % (status['endurance'], status['max_endurance']),
                              'ENDU_COLOR')

        self.tb_status.insert(tk.END,
                              'TGT=%5d@%2d%%\n' % (status['target_id'], status['target_hp']),
                              'TARGET_COLOR')

        self.tb_status.insert(tk.END,
                              'PET=%5d@%2d%%\n' % (status['pet_id'], status['pet_hp']),
                              'PET_COLOR')

        # スペルの詠唱テキスト。スペルのIDを文字列に変換してやる必要がある。
        _s = status['casting_spell_id']
        _spell_casting_text = '\n' if _s == 0 else 'Casting...%s\n' % db.get_spell_name(_s)
        self.tb_status.insert(tk.END,
                              _spell_casting_text,
                              'CASTING_COLOR')

        self.tb_status.insert(tk.END, 'X=%.2f,Y=%.2f,z=%.2f\n' % (status['x'], status['y'], status['h']),
                              'LOC_COLOR')

        self.tb_status.insert(tk.END,
                              'Z=%s(%d)' % (status['zone_longname'], status['zone_id']),
                              'ZONE_COLOR')

        self.tb_status.configure(state=tk.DISABLED)

        # マップエディット機能から使う変数を更新。
        self.zone_id = int(status['zone_id'])
        self.x = float(status['x'])
        self.y = float(status['y'])
        self.h = float(status['h'])

    def update_buffs(self, target_textbox, buff_names):
        """target_textboxとして指定されたバフウィンドウの更新。"""
        target_textbox.configure(state=tk.NORMAL)
        target_textbox.delete("1.0", "end")
        for _buff in buff_names:
            # 頭の+-を除外し、空白をアンダースコアで置き換えたバフ名がsettings.txtで色の登録を行われていたら、色をつける。
            _tmp_buff = _buff[1:] if _buff[0] in ['+', '-', '*'] else _buff
            _tmp_buff = _tmp_buff.replace(' ', '_')


            if _tmp_buff in gs.buff_color.keys():
                target_textbox.insert(tk.END, '%s\n' % _buff, _tmp_buff)
            else:
                target_textbox.insert(tk.END, '%s\n' % _buff)

        target_textbox.configure(state=tk.DISABLED)

    def update_buffs1(self, buff_names):
        """左のバフウィンドウの更新。"""
        self.update_buffs(self.tb_buffs1, buff_names)

    def update_buffs2(self, buff_names):
        """右のバフウィンドウの更新。"""
        self.update_buffs(self.tb_buffs2, buff_names)

    def update_character_state_entry(self, text):
        """キャラクターステートを表示するEntryの更新。"""
        self.en_character_state.configure(state='normal')
        self.en_character_state.delete(0, tk.END)
        self.en_character_state.insert(0, 'CHR State: %s' % text)
        self.en_character_state.configure(state='readonly')

    def update_running_state_entry(self, text):
        """キャラクターごとの走行状態ステートを表示するEntryの更新。"""
        self.en_running_state.configure(state='normal')
        self.en_running_state.delete(0, tk.END)
        self.en_running_state.insert(0, 'RUN State: %s' % text)
        self.en_running_state.configure(state='readonly')

    def update_state_console_textbox(self, text):
        """キャラクターごとのステートコンソールに一行追加。"""

        self.tb_progression.configure(state=tk.NORMAL)

        # 末尾に書き込み
        self.tb_progression.insert(tk.END, "%s\n" % text)

        # 行数が一定数を超えていたら、頭のほうを削除する。
        _total_line_numbers = int(self.tb_progression.index('end-1c').split('.')[0])
        if _total_line_numbers > MAX_STATE_CONSOLE_BUFFER_LINES:
            self.tb_progression.delete("1.0", "%d.0" % (_total_line_numbers - MAX_STATE_CONSOLE_BUFFER_LINES + 1))

        # カーソルを末尾へもっていく。
        self.tb_progression.see("end")

        self.tb_progression.configure(state=tk.DISABLED)
