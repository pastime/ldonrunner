# MainWindow
MAIN_WINDOW_TITLE = 'Ldon Runner'
MAIN_WINDOW_WIDTH = 1212
MAIN_WINDOW_HEIGHT = 900
CHAR_FRAME_WIDTH = 194
CHAR_FRAME_HEIGHT = 600

TELL_TEXT_BOX_WIDTH = 40
TELL_TEXT_BOX_LINES = 8
WINDOW_BG = 'gray23'            # ウィンドウの背景色（何もないところの色）
WINDOW_TEXT_FG = 'gray90'       # ウィンドウに直に書かれるLabel等の文字色
TEXT_BOX_BG = 'gray12'          # テキストボックスの背景。濃い目のグレー。
TEXT_BOX_FG = 'gray80'          # テキストボックス内の文字の色。暗めの白。
TEXT_ITEM_FG = 'DarkOrchid1'    # tell中のアイテムリンクの色。リンク機能はTkinter出はむつかしいので提供しない。
TEXT_TIME_FG = 'DeepSkyBlue2'   # tellの時刻部分の色。
TEXT_NAME_FG = 'yellow2'        # tellの人名部分の色。
PROGRESSION_TEXT_LINES = 9      # 進行度を表示するツリービューの行数。
SMALL_FONT = "Yu Gothic UI"
MID_FONT = "Yu Gothic UI"
MONOSPACED_FONT = "Consolas"

# CharacterFrame - Status
STATUS_TEXT_WIDTH = 28
STATUS_TEXT_LINES = 10
STATUS_COLOR_NAME = 'lawn green'
STATUS_COLOR_COMBAT = 'deep pink'
STATUS_COLOR_HP = 'red2'
STATUS_COLOR_MANA = 'dodger blue'
STATUS_COLOR_ENDURANCE = 'dark green'
STATUS_COLOR_TARGET = 'gold'
STATUS_COLOR_PET = 'LightPink1'
STATUS_COLOR_CASTING = 'yellow'
STATUS_COLOR_LOC = 'thistle1'
STATUS_COLOR_ZONE = 'thistle2'
BUFF_BOX_WIDTH = 16
BUFF_BOX_LINES = 20

# CharacterFrame - Progression
TEXT_PROGRESSION_INDEX_FG = 'DeepSkyBlue2'  # プログレッションのインデックス部分の色。
TEXT_PROGRESSION_ZONE_FG = 'DarkOrchid1'    # プログレッションのインデックス部分の色。
TEXT_PROGRESSION_LOC_X_FG = 'yellow2'       # プログレッションのloc部分の色。
TEXT_PROGRESSION_LOC_Y_FG = 'green2'        # プログレッションのloc部分の色。
TEXT_PROGRESSION_LOC_H_FG = 'SeaGreen1'     # プログレッションのloc部分の色。
TEXT_PROGRESSION_ACTIONS_FG = 'dodger blue' # プログレッションのactions部分の色。
TEXT_CURRENT_LINE_BG = "PaleTurquoise1"     # プログレっションの選択行の色。
STATUS_TEXT = '%s\nHP   %5d/%5d\nMANA %5d/%5d\nENDU %5d/%5d\nTGTid=%5d@%2d%%\nPetID=%5d@%2d%%\nX=%.2f,Y=%.2f,z=%.2f\n%s\nZ=%s(%d)'
MAX_STATE_CONSOLE_BUFFER_LINES = 15

# MapWindow
MAP_WINDOW_DEFAULT_SIZE = '600x600'
MAP_CANVAS_OUTER_MARGIN = 20
MAP_LINE_COLOR = 'gray30'
MAP_BG_COLOR = 'gray60'
MAP_PATH_COLOR = 'deeppink2'
MAP_POINT_RADIUS = 3
MAP_POINT_COLOR = 'deeppink2'
MAP_POINT_FOCUSED_COLOR = 'yellow'
MAP_POINT_BORDER_COLOR = 'gray10'
MAP_POINT_ACTION_TEXT_COLOR = 'royalblue1'
MAP_POINT_ACTION_TEXT_SHIFT_X = 10      # 点に対してどれくらいずらして表示するか。
MAP_POINT_ACTION_TEXT_SHIFT_Y = 0       # 点に対してどれくらいずらして表示するか。
MAP_POINT_INDEX_LABEL_INTERVAL = 5
MAP_POINT_INDEX_LABEL_COLOR = 'magenta2'
MAP_POINT_INDEX_LABEL_SHIFT_X = 0       # 点に対してどれくらいずらして表示するか。
MAP_POINT_INDEX_LABEL_SHIFT_Y = 20      # 点に対してどれくらいずらして表示するか。
MAP_WINDOW_SHRINK_WIDTH = 600       # マップウィンドウをワンクリックでシュリンクしたとき、どのような大きさになるか。
MAP_WINDOW_SHRINK_HEIGHT = 400

# PlayFrame
MASTER_PLAN_TEXT_BOX_WIDTH = 30
MASTER_PLAN_TEXT_BOX_LINES = 11
PROGRESSION_TEXT_BOX_WIDTH = 62
PROGRESSION_TEXT_BOX_LINES = 18

# ControlFrame
CLOCK_COLOR = 'DarkGoldenRod1'
REMAIN_TIME_COUNTING_COLOR = 'cyan'

# Sound Filenames
# ファイルが存在しない場合は、かわりにWindowsの通知音が鳴る。
SOUND_MESSAGE = 'sounds/chime.wav'
SOUND_TAKING_RECOVERY = 'sounds/button-click-3.wav'
SOUND_CANT_REACH_DESTINATION = 'sounds/click1.wav'
SOUND_FINISHED = 'sounds/buttonchime02up.wav'
FINISHING_NOTICE_SOUND_INTERVAL = 5.0   # やることがなくなったときの通知音を鳴らす間隔。SOUND_FINISHEDの長さより長めに。

