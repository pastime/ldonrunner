from transitions.extensions import GraphMachine as Machine
import graphviz
import datetime
import math
import winsound
import random

# ユーザー定義ライブラリ
import lib.bc_commands
from lib.gui_settings import *
import database.encyclopedia as db
import lib.game_settings as gs

# ステートマシン図を出力したいときだけこのスクリプト直接実行するが、通常とエントリポイントが違うため、
# lib.planのインポートのパスが変わってしまうので、エラー回避のためのその時だけ飛ばす。
if __name__ != '__main__':
    import lib.plan

# Graphvizを使用するので、公式からダウンロードし、dot.exeファイルのある場所にPathを通しておくこと。
# Windowsのインストーラ版は必要ならライブラリが一式入っていないようなので、32bitのzip版を入れた。
# 非Windows環境ではまた異なるが、とりあえずdotがコマンドとして実行できるようになっていればよい。
# https://graphviz.org/download/
# cx_freeze等を使うときに、実行ファイルに取り込むことができるか？
# transition 0.8.7の時点では、devブランチを落として来なくても、pip版でGraphmachineを使用できる。

# なぜかわからないが、MQ2の/keypress Uはキーボードからの入力ではうまく動かない。ホットキーに登録するといけた。/keypress I とか Vとかはいけるのに、謎。
# bctで動くかどうか確かめる必要あり。->bctだといけた。
# ほか、/bct name //keypress Homeで、視点を水平に戻せる。この機能も必要だろう。


class GameController:
    """
    自動運転機能の中核をなすクラス。worker.pyのThreadController内の変数でインスタンスが保持される。
    GUI等からの指示は、ThreadControllerが保持するgame_controller_queueに貯めておく。
    game_controlling_threadの定期処理の際に、
        game_controller_queueからメッセージを取り出し、このクラスのprocess_queueを呼び出して、適宜処理を行う。
        このクラスのupdate関数を呼び出し、GroupStateMachineのtick処理を呼び出す。
    という手順で駆動する。
    そのほか、マスタープランの管理（保持及びロード）なども、このクラスで行う。
    """
    def __init__(self,
                 group,
                 presentation_queue,
                 sending_queue,
                 threading_lock):

        # 各種参照
        self.group = group
        self.presentation_queue = presentation_queue
        self.sending_queue = sending_queue
        self.threading_lock = threading_lock

        self.group_state_machine = GroupStateMachine(self,
                                                     presentation_queue,
                                                     sending_queue,
                                                     threading_lock)
        # マスタープランは、zoneplanのリスト。順番に消化していく。
        self.master_plan = []

        # タイマー関係
        self.is_timer_on = False
        self.hard_timer_datetime = None
        self.early_exit_minutes = 0

        # マクロのバックエンド
        self.macro_backend = None
        self.macro_backend_last_polling_datetime = None

    def update(self):
        # スレッドの無限ループから、一定時間ごとに呼び出される。
        # 呼び出しもとでウェイト処理を入れているので、ここで入れる必要はない。
        self.group_state_machine.update()

        # タイマー表示の更新処理。あくまでも表示だけ。
        self.update_timer()

    def get_backend(self):
        """バックエンドを取得する。"""
        if not self.macro_backend:
            # バックエンドが未取得の場合はデフォルト値を返すが、その前にバックエンドの問い合わせを送信しておく。
            # ただし、連打するとEQクライアント側のEQBCSのバッファがあふれてしまうので、必ず一定間隔を置く。
            _now = datetime.datetime.now()
            if not self.macro_backend_last_polling_datetime or _now > self.macro_backend_last_polling_datetime + datetime.timedelta(seconds=gs.setting['BACKEND_POLLING_INTERVAL']):
                _main_tank_name = self.get_main_tank_name()
                if _main_tank_name:
                    print("バックエンドの確認を行います…")
                    _sending_packet = '//bc lrcheckbackend'
                    self.sending_queue.put(lib.bc_commands.bct(_main_tank_name, _sending_packet))
                self.macro_backend_last_polling_datetime = _now

            return gs.setting['DEFAULT_BACKEND']

        return self.macro_backend

    def update_timer(self):
        """時計の時刻の更新と、脱出タイマーのカウントダウン処理。"""
        # 時計をアップデートする。
        _now = datetime.datetime.now()
        _datetime_text = _now.strftime("%Y/%m/%d %H:%M:%S")
        self.presentation_queue.put(('UPDATE CLOCK', _datetime_text))

        if self.is_timer_on and self.hard_timer_datetime:
            # タイマーがオンのときだけ、残り時間のカウントダウンを行う。
            _remain_time = self.hard_timer_datetime - _now
            _seconds = _remain_time.total_seconds()
            _remain_hours = int(_seconds / 3600)
            _remain_mins = int(_seconds / 60) % 60
            _remain_secs = _seconds % 60
            #print("remains: %d, %d" % (_remain_hours, _remain_mins))
            self.presentation_queue.put(('UPDATE REMAIN TIME', "%3d:%02d:%02d" % (_remain_hours, _remain_mins, _remain_secs)))

    def load_master_plan(self, _master_plan_filename):
        # マスタープランをファイルから読み込む。
        _zone_file_list = []
        with open(_master_plan_filename, "r") as _fd:
            _zone_file_list = _fd.readlines()

        self.master_plan = []
        for _zone_file in _zone_file_list:
            _zp = lib.plan.ZonePlan()
            _zp.load_zone_plan(_zone_file)
            self.master_plan.append(_zp)

        # 全キャラクターが参照するゾーンプランを最初のものにリセットする。
        for _name in self.group.get_member_name_list():
            self.group_state_machine.character_zone_plan_index[_name] = 0

    def set_plan_progression(self, zone_plan_index=0, main_path_index=0):
        """指定された値まで、プランの消化度を進める。GUIから呼び出される前提。"""
        self.group_state_machine.character_zone_plan_index = zone_plan_index
        self.group_state_machine.head_index = main_path_index

    def get_main_tank_name(self, verbose=True):
        """メインタンク名を取得する。"""
        if self.group_state_machine.tank_name != '':
            return self.group_state_machine.tank_name
        _tank_name = ''
        _max_tank_priority = 0
        with self.threading_lock:
            for _character_name, _character in self.group.members.items():
                _priority = db.get_player_class(_character.player_class)[1]
                #print("%s(%d) has tank priority of %d..." % (_character_name, _character.player_class, _priority))
                if _priority > _max_tank_priority:
                    _tank_name = _character_name
                    _max_tank_priority = _priority
        if verbose:
            print("メインタンクはは%sと推定されました。" % _tank_name)
        return _tank_name

    def get_main_buffer_name(self, verbose=True):
        """メインBuffer名を取得する。"""
        if self.group_state_machine.buffer_name != '':
            return self.group_state_machine.buffer_name
        _buffer_name = ''
        _max_buffer_priority = 0
        with self.threading_lock:
            for _character_name, _character in self.group.members.items():
                _priority = db.get_player_class(_character.player_class)[2]
                #print("%s(%d) has buffer priority of %d..." % (_character_name, _character.player_class, _priority))
                if _priority > _max_buffer_priority:
                    _buffer_name = _character_name
                    _max_buffer_priority = _priority
        if verbose:
            print("メインBufferは%sと推定されました。" % _buffer_name)
        return _buffer_name

    def get_action_order_by_name(self):
        """
        グループメンバーの名前を行動順に並べたリストを得る。
        基本的にタンクオーダーの順だが、タンクが明示的に指定されている場合は、タンクが優先される。バッファーは関係なし。
        """
        _character_order = []
        with self.threading_lock:
            for _character_name, _character in self.group.members.items():
                _priority = db.get_player_class(_character.player_class)[1]
                _character_order.append([_character_name, _priority])

        # priorityに着目して降順にソート
        _sorted_char_names = sorted(_character_order, reverse=True, key=lambda x: x[1])

        # タンクが明示的に指定されていた場合
        if self.group_state_machine.tank_name != '':
            _sorted_char_names = [_x for _x in _sorted_char_names if _x[0] != self.group_state_machine.tank_name]
            _sorted_char_names = [[self.get_main_tank_name(), 100]] + _sorted_char_names

        #print("キャラクターの行動順は、", _sorted_char_names)
        return [_x[0] for _x in _sorted_char_names]

    def get_group_center(self):
        """グループメンバーの座標の中心を求める。"""
        _member_number = len(self.group.members)
        if _member_number == 0:
            print("ログインしているキャラクターがいません。")
            return

        _zone_id = None
        _mean_x = 0
        _mean_y = 0
        _mean_h = 0

        for _value in self.group.members.values():
            # すべてのキャラクターは同じゾーンにいなければならない。
            if not _zone_id:
                _zone_id = _value.zone_id
            elif _zone_id != _value.zone_id:
                print("キャラクターが複数のゾーンに散らばっています")
                return 0, 0, 0, None

            _mean_x += _value.x
            _mean_y += _value.y
            _mean_h += _value.h

        _mean_x /= _member_number
        _mean_y /= _member_number
        _mean_h /= _member_number

        return _mean_x, _mean_y, _mean_h, _zone_id

    def check_character_zone(self, character_name):
        """指定されたキャラクターがいまいるゾーンが、使用しているマスタープランのゾーンと一致するか。"""
        _zone_id = self.group.get_member(character_name).zone_id
        # TODO: とりあえず、マスタープランの最初のものを参照している。マルチゾーン対応になった場合は、ここも要修正。
        return _zone_id == self.master_plan[0].zone_id

    def process_queue(self, message):
        """
        キューの処理をここで行う。
        :param message:
        :return:
        """
        if message[0] == 'LOAD_MASTER_PLAN':
            # プランをロードする。
            print('loading new master plan...')
            self.load_master_plan(message[1])

        elif message[0] == 'SET_PLAN_PROGRESSION':
            # プランの進捗度をGUIから設定する。
            self.set_plan_progression(zone_plan_index=int(message[1]),
                                      main_path_index=int(message[2]))

        elif message[0] == 'SET_AUTOPLAY_ON':
            # オートプレイをオンにする。
            print("オートプレイをオンにします。")
            self.group_state_machine.refresh_character_list()
            self.group_state_machine.flag_auto_run = True
            self.group_state_machine.flag_set_ready = True
            self.group_state_machine.set_ready()
            if self.group_state_machine.head_index > 1:
                # 進行中の場合は、ヘッドをひとつ巻きもどす。
                # 誰かが引っ掛かった状態から再開したときに、他のメンバーが引っかかりを無視して進行するのを防ぐ。
                self.group_state_machine.head_index -= 1

        elif message[0] == 'SET_AUTOPLAY_OFF':
            # オートプレイをオフにする。
            print("オートプレイをオフにします。")
            self.group_state_machine.flag_auto_run = False

        elif message[0] == 'PLAY_ONE_STEP':
            # オートプレイをオフにし、ワンステップだけ実行するフラグを立てる。
            print("オートプレイをオフにし、次のワンステップだけ実行します。")
            self.group_state_machine.refresh_character_list()
            self.group_state_machine.flag_auto_run = False
            self.group_state_machine.flag_play_one_step = True
            self.group_state_machine.flag_set_ready = True

        elif message[0] == 'SET_CLOSEST_MAINPATH_WAYPOINT_AS_HEAD':
            # 現在地に最も近いメインパス上のウェイポイントをHEADとしてセットする。
            # グループの現在地は、全キャラクターの座標の重心とする。
            _mean_x, _mean_y, _mean_h, _zone_id = self.get_group_center()

            if not _zone_id:
                # キャラクターが複数ゾーンに散らばっていた。
                return

            # マスタープランから、該当するゾーンを調べる。
            _current_zone_plan = None
            for _zone_plan in self.master_plan:
                if _zone_plan.zone_id == _zone_id:
                    _current_zone_plan = _zone_plan

            if not _current_zone_plan:
                print("現在のゾーンは、マスタープラン上のゾーンプランに含まれていません。")
                return

            _sorted_index_distance_pair = _current_zone_plan.main_path.get_close_points(_mean_x,
                                                                                        _mean_y,
                                                                                        _mean_h)

            if self.group_state_machine.head_index > 0:
                # すでにゾーンプランの途中まで進んでいる場合、往路復路の取違いを防止するため、現在消費済みの経路の中のポイントだけが候補となる。
                _candidate_pairs = [_x for _x in _sorted_index_distance_pair if _x[0] <= self.group_state_machine.head_index]

                # その上位のポイントの中から、インデックスが最大のものを選ぶ。
                _answer_index = 0
                for _pair in _candidate_pairs[:gs.setting['MAX_SEEK_HEAD_CANDIDATES']]:
                    if _pair[0] > _answer_index:
                        _answer_index = _pair[0]

            else:
                # ゾーンプランのインデックスがゼロ（ロード直後など）の場合は、全ポイントの中から、単純に最も近いウェイポイントを選ぶ。
                _answer_index, _distance = _current_zone_plan.main_path.get_closest_point(_mean_x,
                                                                                          _mean_y,
                                                                                          _mean_h)

            print("最も近い点は、インデックス%dです。" % _answer_index)
            print("現在の平均位置は、%f %f %f" % (_mean_x, _mean_y, _mean_h))
            self.group_state_machine.head_index = _answer_index

        elif message[0] == 'RESET_HEAD_TO_ZERO':
            """ヘッドをリセットする。"""
            self.group_state_machine.head_index = 0

        elif message[0] == 'RESET_MASTERPLAN':
            """マスタープランをリセットする。"""
            self.master_plan = []

        elif message[0] == 'ADD_ZONEPLAN_TO_MASTERPLAN':
            """マスタープランにゾーンプランを追加する。"""
            self.master_plan.append(message[1])

        elif message[0] == 'COMBAT STARTED':
            """戦闘開始が報告された。"""
            self.group_state_machine.character_is_in_combat[message[1]] = True

        elif message[0] == 'COMBAT ENDED':
            """戦闘終了が報告された。"""
            self.group_state_machine.character_is_in_combat[message[1]] = False

        elif message[0] == 'SET TANK':
            """タンク名が設定された。"""
            self.group_state_machine.tank_name = message[1]

        elif message[0] == 'SET BUFFER':
            """Buffer名が報告された。"""
            self.group_state_machine.buffer_name = message[1]

        elif message[0] == 'SET HIDEC ON':
            """自動hidecモードがオンになった。"""
            print("Auto-hidc npc mode on.")
            self.group_state_machine.periodic_hide_corpse_mode = True

        elif message[0] == 'SET HIDEC OFF':
            """自動hidecモードがオフになった。"""
            print("Auto-hidc npc mode off.")
            self.group_state_machine.periodic_hide_corpse_mode = False

        elif message[0] == 'RESET BUFF TIMER':
            """バフタイマーをリセットする。"""
            print("Resetting previous buff timestamp.")
            self.group_state_machine.previous_buff_datetime = None

        elif message[0] == 'SET EARLY EXIT MINUTES':
            """早期脱出タイマーを設定する。"""
            print("Setting early exit timer as %d." % message[1])
            self.early_exit_minutes = message[1]

        elif message[0] == 'SET HARD TIMER':
            """タイマーのハードリミットを設定する。"""
            print("Setting hard timer limit as +%dday(s), %d:%d." % (message[1],
                                                                     message[2],
                                                                     message[3]))

            # タイマーの期限のdatetimeオブジェクトを生成する。
            _now = datetime.datetime.now()
            _new_day = _now + datetime.timedelta(days=message[1])

            self.hard_timer_datetime = _new_day.replace(hour=message[2], minute=message[3])

        elif message[0] == 'START HARD TIMER':
            """脱出タイマーを開始する。"""
            print("Starting exit timer countdown.")
            self.is_timer_on = True

        elif message[0] == 'STOP HARD TIMER':
            """脱出タイマーを停止する。"""
            print("Stopping exit timer countdown.")
            self.is_timer_on = False

        elif message[0] == 'SET E3 BACKEND':
            """バックエンドをE3にする。"""
            print("E3 backend available.")
            self.macro_backend = 'E3'

        elif message[0] == 'SET E4 BACKEND':
            """バックエンドをE4にする。"""
            print("E4 backend available.")
            self.macro_backend = 'E4'

        else:
            print('GameController: Unrecognizable queue entry:', message)


class GroupStateMachine:
    """
    グループの状態を管理するステートマシン。マスタープランを参照するために、GameContollerへの参照をもつ。
    また、下位のステートとして、CharacterLongMoveStateMachineのインスタンスを、キャラクターの数だけ辞書として持つ。
    この階層では、グループがどのような状態にあり、次に何をするべきかを管理し、戦闘や待機状態を管理したり、
    CharacterLongMoveStateMachineに指示を出す。
    """
    def __init__(self,
                 game_controller,
                 presentation_queue,
                 sending_queue,
                 threading_lock):

        # 諸所参照
        self.game_controller = game_controller      # あんまり親への参照を持つのはどうかと思うが。
        self.presentation_queue = presentation_queue
        self.sending_queue = sending_queue
        self.threading_lock = threading_lock        # キャラクターのデータを参照するので、ロックする機会がある。

        # 諸所変数
        self.character_long_move_state_machines = {}    # 名前: CharacterLongMoveStatusの辞書を持つ。
        self.character_is_in_combat = {}                # キャラクターの戦闘状態を保持する。キーは名前、値はBool。
        self.tank_name = ''                             # タンクの名前。startbattle先となる。
        self.buffer_name = ''                           # バファーの名前。/bct tank_name //tell buffer_name buff meなイメージ。

        self.head_index = 0                             # グループが到達したメインパスのもっとも先の点を保持する。
        self.character_zone_plan_index = {}             # キャラクターごとに、マスタープランの何番目のファイルを参照しているか、名前:intの辞書で保持する。

        # 各種フラグ。
        self.flag_play_one_step = False                 # 次のステップひとつだけを実行するというフラグ。これをオンにするときは、auto_runは自動的にオフにする。
        self.flag_auto_run = False                      # Ready状態になったら、自動的に次の行動を開始するか。GUIからトグルする。
        self.is_in_combat = False                       # ステートマシンの戦闘状態ではなく、ゲーム内の戦闘状態（推定）を保持する。
        self.was_panicked = False                       # パニック中に戦闘になった場合、戦闘終了後にパニックを継続するためのフラグ。パニック中の戦闘を通常戦闘と別ステートにするべきなのかも？

        # hidec関係
        self.periodic_hide_corpse_mode = False          # 定期的にhidecするモードがオンかどうか。
        self.periodic_hide_corpse_timer = None          # 前回のhidecを行った時間を記録する。

        # Wait関係
        self.already_waited_here = False                # このウェイポイント上で、すでに休息したか。
        self.wait_timer_duration = 0                    # Waitする秒数

        # そのステートにチェンジした時刻を保持するための変数。
        self.state_started_datetime = None

        # 前回のバフ時刻。
        self.previous_buff_datetime = None

        # 前回いずれかのキャラクターのヒットポイントが変動した時刻。
        self.previous_hp_change_datetime = None
        self.previous_hp_dict = {}

        # メインタンクがそれまでいたと思っているゾーン。これが変わったときに、guiに時刻を表示させる。
        self.zone_id_where_main_tank_was_in = -1

        # 状態の定義
        self.states = ['ready', 'proceed', 'combat', 'wait', 'finished', 'combat_during_waiting']

        self.transitions = [
            {'trigger': 'check',    # 不要か。すべてのキャラクターのゾーンプランが終了した。
             'source': 'ready',
             'dest': 'finished',
             'conditions': 'condition_all_character_move_finished',
             'after': 'reset_timer'},

            {'trigger': 'check',    # レディ状態から戦闘を開始する。
             'source': 'ready',
             'dest': 'combat',
             'conditions': 'condition_can_combat',
             'after': 'reset_timer'},

            {'trigger': 'check',    # 休息を開始する。
             'source': 'ready',
             'dest': 'wait',
             'conditions': 'condition_wait_for_a_while',
             'after': 'reset_timer'},

            {'trigger': 'check',    # レディ状態から前進を開始する。
             'source': 'ready',
             'dest': 'proceed',
             'conditions': 'condition_move_ok',
             'after': 'reset_timer'},

            {'trigger': 'check',    # 不要か。戦闘中にかからわず、前進を開始する。
             'source': 'ready',
             'dest': 'proceed',
             'conditions': 'condition_in_combat_and_gotta_proceed',
             'after': 'reset_timer'},

            {'trigger': 'check',    # 前進状態が終了した。
             'source': 'proceed',
             'dest': 'ready',
             'conditions': 'condition_finish_proceeding',
             'after': 'reset_timer'},

            {'trigger': 'check',    # 戦闘が終了した。
             'source': 'combat',
             'dest': 'ready',
             'conditions': 'condition_finish_combat',
             'after': 'reset_timer'},

            {'trigger': 'check',    # 休息中に戦闘が発生した。
             'source': 'wait',
             'dest': 'combat_during_waiting',
             'conditions': 'condition_start_battle_during_waiting',
             'after': 'reset_timer'},

            {'trigger': 'check',    # 休息を終了する。
             'source': 'wait',
             'dest': 'ready',
             'conditions': 'condition_finish_waiting',
             'after': 'reset_timer'},

            {'trigger': 'check',   # 休息中の戦闘を終了する。
             'source': 'combat_during_waiting',
             'dest': 'wait',
             'conditions': 'condition_finish_combat_during_waiting',
             'after': 'reset_timer'},

        ]

        self.machine = Machine(model=self,
                               states=self.states,
                               transitions=self.transitions,
                               initial='ready',
                               auto_transitions=False,
                               ignore_invalid_triggers=True,
                               ordered_transitions=False,
                               title="",                        # GraphMachine用
                               show_auto_transitions=False,     # GraphMachine用
                               show_conditions=False)           # GraphMachine用

    def update(self):
        """このルーチンは、タイマーで一定時間ごとに呼び出される。"""
        for _trigger in self.machine.get_triggers(self.state):
            self.trigger(_trigger)
            # 自己遷移する場合は、beforeとかafterとかで処理する必要がある。

        # 一定時間ごとに全員が/hidec npcするモードかどうかチェックし、そうなら実行する。
        self.periodic_hidec_check()

        # メインタンクのいるゾーンが変わっていたら、報告させる。
        self.check_zone_change()

        # キャラクターのヒットポイントの更新チェック。
        self.check_character_hp_change()

        # キャラクターのステータスを更新する。必ずタンクから行動開始させたい。
        for _character_name in self.game_controller.get_action_order_by_name():
            if _character_name in self.character_long_move_state_machines:
                # ステートマシンが生成されるより前に呼び出される可能性があるため、生成確認が必要。
                self.character_long_move_state_machines[_character_name].update()

    def reset_timer(self):
        """
        アクションの開始時刻を記録するためのコールバック。
        :return:
        """
        self.state_started_datetime = datetime.datetime.now()

    def refresh_character_list(self):
        """グループに存在しなくなったキャラクターなどを同期する。"""
        # とりあえず、現時点では過去のステートマシンをすべて廃棄してリフレッシュする。
        # LD対策等で、取り分けて保存しておくような機能が必要なような気がする。
        self.character_long_move_state_machines = {}
        for _name in self.game_controller.group.get_member_name_list():
            if len(self.character_long_move_state_machines) > 6:
                # キャラクターは6人まで。
                break
            self.character_long_move_state_machines[_name] = CharacterLongMoveStateMachine(self.game_controller,
                                                                                           _name,
                                                                                           self,
                                                                                           self.presentation_queue,
                                                                                           self.sending_queue,
                                                                                           self.threading_lock)
            self.character_is_in_combat[_name] = False  # 初期状態は非戦闘状態とする。

    def check_combat_status(self):
        """グループが戦闘状態にあるかのチェックを行う。"""
        _combat_state = False

        for _value in self.character_is_in_combat.values():
            _combat_state = _combat_state or _value

        return _combat_state

    def check_character_hp_change(self):
        """各キャラクターのヒットポイントが変動していないかチェックし、していたら変動時刻を更新する。"""
        with self.threading_lock:
            for _character_name in self.game_controller.group.get_member_name_list():
                _character_hp = self.game_controller.group.get_member(_character_name).hp

                if _character_name not in self.previous_hp_dict:
                    # 未登録のキャラクターなら登録する。これは変動にカウントしない。
                    self.previous_hp_dict[_character_name] = _character_hp
                    continue

                if _character_hp != self.previous_hp_dict[_character_name]:
                    # HPが変動していたら、前回のヒットポイント値と変動時刻を更新する。
                    #print("%sのヒットポイントが変動した…" % _character_name)
                    self.previous_hp_dict[_character_name] = _character_hp
                    self.previous_hp_change_datetime = datetime.datetime.now()

    def is_masterplan_available(self):
        """マスタープランを利用可能かチェックする。"""
        # TODO:とりあえず、マスタープランにゾーンプランがひとるの状態だけを想定。これは変更する必要がある。
        if len(self.game_controller.master_plan) == 0:
            return False
        if self.head_index > len(self.game_controller.master_plan[0].main_path.path_points) - 1:
            return False
        return True

    def get_combat_command(self):
        """バックエンドに応じて、適切な戦闘開始コマンドを返す。"""
        _backend = self.game_controller.get_backend()
        #print(_backend)
        if _backend == 'E3':
            return gs.setting['E3_COMBAT_COMMAND']
        elif _backend == 'E4':
            return gs.setting['E4_COMBAT_COMMAND']
        else:
            print("%sは、適切なバックエンドではありません。" % _backend)
        return None

    def periodic_hidec_check(self):
        """定期的に/hidec npcを実施するためのルーチン。"""
        # モードがオフなら実行しない。
        if not self.periodic_hide_corpse_mode:
            return

        _now = datetime.datetime.now()
        # 初期設定がまだなら初期設定を行う。
        if self.periodic_hide_corpse_timer is None:
            self.periodic_hide_corpse_timer = _now
            return

        # 一定時間経過していたら、即時に/hidec npcを実行し、タイマーをリセットする。
        if _now > self.periodic_hide_corpse_timer + datetime.timedelta(seconds=gs.setting['PERIODIC_HIDE_CORPSE_INTERVAL']):
            self.hidec_npc()
            self.periodic_hide_corpse_timer = _now

    def hidec_npc(self):
        """グループのキャラクター全員が、/hidec npcを行う。"""
        for _character_name in self.game_controller.group.get_member_name_list():
            _sending_packet = '//hidec NPC'
            self.sending_queue.put(lib.bc_commands.bct(_character_name, _sending_packet))

    def check_zone_change(self):
        """メインタンクのいるゾーンが変更されたときに、報告する。"""
        _main_tank_name = self.game_controller.get_main_tank_name(verbose=False)
        _main_tank = self.game_controller.group.get_member(_main_tank_name)
        if _main_tank:
            _current_zone_id = _main_tank.zone_id
            if _current_zone_id != self.zone_id_where_main_tank_was_in:
                print("メインタンクのゾーンチェンジを検出しました。")
                self.zone_id_where_main_tank_was_in = _current_zone_id
                _text = "I entered %s." % db.get_zone_longname(_current_zone_id)
                self.presentation_queue.put(('MISC_INFO', (_main_tank_name, _text)))

    def condition_all_character_move_finished(self):
        """すべてのキャラクターが、ゾーンの最終目的地まで到達した。"""
        '''
        # この遷移をどうするか、あとまわし。最後のゾーンプランの最後の点まで到達したときにTrueとなるべき。
        _everyone_finised_zoneplan = True

        for _character_name, _character_state in self.character_state_machines.items():
            _everyone_finised_zoneplan = _everyone_finised_zoneplan and _character_state.state == 'finished'
            #print("%s は %s" % (_character_name, _character_state.state))

        self.presentation_queue.put(('UPDATE_GROUP_STATE_ENTRY', 'finished'))
        return _everyone_finised_zoneplan
        '''
        return False

    def condition_move_ok(self):
        """
        ready -> proceed
        ready状態かつ戦闘状態でなければ、移動を開始する。
        """
        if not (self.flag_auto_run or self.flag_play_one_step):
            # オートランもしくは一回だけ進行するというフラグが立っていなければ、移動開始しない。
            return False

        if not self.is_masterplan_available():
            return False

        _current_point = self.game_controller.master_plan[0].main_path.get_point(self.head_index)

        if self.check_combat_status():
            if _current_point and _current_point.find_action('p'):
                # ポイントにp属性があったら、戦闘開始フラグが立っていても無視して移動する。
                pass
            else:
                # 戦闘状態だったら、移動開始しない。もしかすると、戦闘終了から一定時間は移動開始しないほうがいいのかも。
                return False

        # ポイントのアクションにgoto行番号というものが含まれていたら、ヘッドを移動させる。
        if _current_point:
            _goto = _current_point.find_action('goto')
            if _goto:
                self.head_index = int(_goto)

            # ここにいろいろ追加してもよい。

        # 次のポイントに移動させる。
        # とりあえずmaster_plan[0]としているが、要複数対応。
        if self.head_index > len(self.game_controller.master_plan[0].main_path.path_points) - 1:
            print("ゾーンプランがありません。TODO: このあとの処理を要追加。")
            if datetime.datetime.now() > self.state_started_datetime + datetime.timedelta(
                    seconds=FINISHING_NOTICE_SOUND_INTERVAL):
                # 終了を通知する音を鳴らす。
                winsound.PlaySound(SOUND_FINISHED,
                                   winsound.SND_FILENAME | winsound.SND_ASYNC)
                self.state_started_datetime = datetime.datetime.now()
            return False

        print("Heading -> %d..." % self.head_index)

        # Characterのステートマシンに点列を与え、到達点のインデックスをゼロにする。
        for _character_name, _character_state in self.character_long_move_state_machines.items():
            _character_state.path_name_running = 'main'  # 現在走行中のパスの名前
            # この点列に沿って移動する。
            _character_state.current_dest = self.game_controller.master_plan[0].main_path.path_points[self.head_index]
            _character_state.points = [_character_state.current_dest]
            _character_state.zone_plan_index_fed = '%d' % self.head_index

        # 移動開始するため、現在のウェイポイントで休息したフラグをオフにする。AutosellやBuffでも同様にすべき。
        self.already_waited_here = False

        self.presentation_queue.put(('UPDATE_GROUP_STATE_ENTRY', 'proceed'))
        return True

    def condition_in_combat_and_gotta_proceed(self):
        return False

    def should_combat_start(self, state_text='combat'):
        """戦闘開始を判断するヘルパ関数。ready状態とwait状態から使用する。"""
        if not self.is_masterplan_available():
            return False

        # ウェイポイントがp属性を持つ場合、戦闘に入らない。これのmaster_plan[0]も、複数ゾーン対応時に要変更。
        _current_point = self.game_controller.master_plan[0].main_path.get_point(self.head_index)
        _action = _current_point.find_action('p')
        if _action:
            return False

        # 戦闘の開始
        if self.check_combat_status() and (self.flag_auto_run or self.flag_play_one_step):
            self.is_in_combat = True
            _sending_packet = self.get_combat_command()
            self.sending_queue.put(lib.bc_commands.bct(self.game_controller.get_main_tank_name(),
                                                       _sending_packet))
            self.presentation_queue.put(('UPDATE_GROUP_STATE_ENTRY', state_text))
            return True
        return False

    def condition_can_combat(self):
        """ready状態で、かつcombat_statusがTrueなら、戦闘開始してcombatに遷移。"""
        return self.should_combat_start(state_text='combat')

    def condition_wait_for_a_while(self):
        """待機する。ready->waiting"""
        # これのmaster_plan[0]も、複数ゾーン対応時に要変更。
        if not self.is_masterplan_available():
            return False

        _current_point = self.game_controller.master_plan[0].main_path.get_point(self.head_index)

        # wait##の場合
        _action = _current_point.find_action('wait')
        if _action and not self.already_waited_here:
            self.wait_timer_duration = int(_action)
            self.already_waited_here = True
            self.presentation_queue.put(('UPDATE_GROUP_STATE_ENTRY', 'waiting %dsec' % int(_action)))
            return True

        #buff##の場合
        _action = _current_point.find_action('buff')
        if _action and not self.already_waited_here:
            _now = datetime.datetime.now()
            if self.previous_buff_datetime is None or _now > self.previous_buff_datetime + datetime.timedelta(minutes=gs.setting['MINIMUM_BUFF_INTERVAL']):
                self.wait_timer_duration = int(_action)
                self.already_waited_here = True
                _sending_packet = '//tell %s buff me' % self.game_controller.get_main_buffer_name()
                self.sending_queue.put(lib.bc_commands.bct(self.game_controller.get_main_tank_name(), _sending_packet))
                self.presentation_queue.put(('UPDATE_GROUP_STATE_ENTRY', 'buff and waiting %dsec' % int(_action)))
                self.previous_buff_datetime = _now
                return True
            else:
                _text = "ウェイポイントでのバフが指定されていますが、前回のバフからの経過時間%.02f分が%d分よりも短いため、スキップします。"
                print(_text % ((_now - self.previous_buff_datetime).seconds / 60.0,
                               gs.setting['MINIMUM_BUFF_INTERVAL']))

        #hidecの場合
        _action = _current_point.find_action('hidec')
        if _action and not self.already_waited_here:
            self.wait_timer_duration = int(_action)
            self.already_waited_here = True
            self.hidec_npc()
            '''
            for _character_name in self.game_controller.group.get_member_name_list():
                _sending_packet = '//hidec NPC'
                self.sending_queue.put(lib.bc_commands.bct(_character_name, _sending_packet))
                self.presentation_queue.put(('UPDATE_GROUP_STATE_ENTRY', 'hidec and waiting %dsec' % int(_action)))
            '''
            return True

        # bc の場合。bc 待機秒数 コマンド、の形式とする。
        _action = _current_point.find_action('bc')
        if _action and not self.already_waited_here:
            _splitted = _action.strip().split(" ")
            if len(_splitted) != 2 or not _splitted[0].strip().isdigit():
                # 形式がおかしいので、何もしない。
                print(_splitted)
                print("ウェイポイントに設定されたbcコマンドが、bc 待機秒数 コマンドのようになっていません。")
                return False
            self.wait_timer_duration = int(_splitted[0])
            self.already_waited_here = True
            _sending_packet = '//bc %s' % _splitted[1]
            self.sending_queue.put(lib.bc_commands.bct(self.game_controller.get_main_tank_name(),
                                                       _sending_packet))
            self.presentation_queue.put(('UPDATE_CHARACTER_CONSOLE',
                                         self.game_controller.get_main_tank_name(),
                                         '%s' % _sending_packet))

            return True
        return False

    def condition_finish_proceeding(self):
        """
        proced -> ready
        すべてのキャラクターが移動を完了し、レディになった。
        """
        _everyone_finished_moving = True
        _everyone_reached_same_zone_plan_index = True
        _group_reached_zone_plan_index = ''

        for _character_name, _character_state in self.character_long_move_state_machines.items():
            # このforは回らない可能性がある。
            # ひとりでもReadyでなかったら、グループは移動完了していない。当然、同じゾーンプランインデックスにも到達していない。
            if _character_state.state != 'ready':
                _everyone_finished_moving = False
                _everyone_reached_same_zone_plan_index = False

        #print("DEBUG:", end="")

        for _character_name, _character_state in self.character_long_move_state_machines.items():
            # このforは回らない可能性がある。
            if _group_reached_zone_plan_index == '':
                # 一人目の到達地点をいったん代入。
                _group_reached_zone_plan_index = _character_state.zone_plan_index_reached

            #print("[%s]:%s " % (_character_name, _character_state.zone_plan_index_reached), end="")

            # 全員が移動を完了していても、全員の到達済みゾーンプランインデックスが一致していない場合は、同じゾーンプランインデックスまでは到達していない。
            # 原則的に、このifは通らないはず。
            if _group_reached_zone_plan_index != _character_state.zone_plan_index_reached and _everyone_finished_moving:
                _everyone_reached_same_zone_plan_index = False
                print("GS: すべてのキャラクターのlongのステートがreadyですが、到達位置が異なります！要原因究明。%s vs %s" % (_group_reached_zone_plan_index,
                                                                                       _character_state.zone_plan_index_reached))

        #print(" grp:%s" % _group_reached_zone_plan_index, end="")
        #print("")

        if _everyone_finished_moving and _everyone_reached_same_zone_plan_index and _group_reached_zone_plan_index != '':
            print("全員が移動を完了した…(%s)" % _group_reached_zone_plan_index)
            # 前進完了していたら、進行済みのパスのインデックスを1増加させ、またワンステップ実行状態をオフにする。
            self.head_index += 1
            self.flag_play_one_step = False

            # ここはいまhead_indexをハイライトしているが、ゾーンプランを複数扱うようにしたら、考慮する必要あり。
            self.presentation_queue.put(('UPDATE_PROGRESSION_LINE_HIGHLIGHT', self.head_index + 1))
            self.presentation_queue.put(('UPDATE_GROUP_STATE_ENTRY', 'ready'))
            return True

        return False

    def should_finish_combat(self, rollback_after_fight):
        """戦闘終了を判定するヘルパ関数。combat状態と、combat_during_waitingから使用する。"""
        if not self.check_combat_status():
            # 戦闘が終了していた。
            self.is_in_combat = False
            _sending_packet = '//bc stopbattle'
            self.sending_queue.put(lib.bc_commands.bct(self.game_controller.get_main_tank_name(),
                                                       _sending_packet))
            if self.head_index > 0:
                self.head_index -= rollback_after_fight    # 戦闘終了後に集合するWaypoint

            # ここはいまhead_indexをハイライトしているが、ゾーンプランを複数扱うようにしたら、考慮する必要あり。
            self.presentation_queue.put(('UPDATE_PROGRESSION_LINE_HIGHLIGHT', self.head_index + 1))
            self.presentation_queue.put(('UPDATE_GROUP_STATE_ENTRY', 'ready'))
            return True

        # まだ戦闘が終了していない。この時点でstopbattleしていたら、startbattleしなおしたいところ。
        if datetime.datetime.now() > self.state_started_datetime + datetime.timedelta(seconds=gs.setting['STARTBATTLE_REFRESH_SEC']):
            _sending_packet = self.get_combat_command()
            self.sending_queue.put(lib.bc_commands.bct(self.game_controller.get_main_tank_name(),
                                                       _sending_packet))
            self.state_started_datetime = datetime.datetime.now()
            return False

        # 戦闘が継続しているが、一定時間以上どのキャラもヒットポイントが変化していない場合、戦闘状態の更新が正常に同期できなかった可能性がある。
        # その場合、強制的に/bc lrwazzupさせる。
        if self.previous_hp_change_datetime and datetime.datetime.now() > self.previous_hp_change_datetime + datetime.timedelta(seconds=gs.setting['CHECK_BATTLE_STATUS_AFTER_THIS_SECONDS']):
            print("戦闘が継続していますが、どのキャラクターのヒットポイントも一定時間以上変動していません。同期状態を確認します。")
            _sending_packet = '//bc lrwazzup'
            self.sending_queue.put(lib.bc_commands.bct(self.game_controller.get_main_tank_name(),
                                                       _sending_packet))
            # HPは更新されていないが、タイマーはリセットする必要がある。
            self.previous_hp_change_datetime = datetime.datetime.now()

        return False

    def condition_finish_combat(self):
        """戦闘の終了。ステートがcombatで、かつ全員が非戦闘状態なら、戦闘を終了する。"""
        return self.should_finish_combat(rollback_after_fight=1)

    def condition_finish_waiting(self):
        """休息の終了"""
        if datetime.datetime.now() > self.state_started_datetime + datetime.timedelta(seconds=self.wait_timer_duration):
            self.presentation_queue.put(('UPDATE_GROUP_STATE_ENTRY', 'ready'))
            return True
        return False

    def condition_start_battle_during_waiting(self):
        """休息中に戦闘が開始された。"""
        return self.should_combat_start(state_text='combat(during waiting)')

    def condition_finish_combat_during_waiting(self):
        """休息中の戦闘が終了した。"""
        return self.should_finish_combat(rollback_after_fight=0)

    def condition_someone_died(self):
        # おそらく不要。
        return False

    def set_ready(self):
        print("グループステートを強制的にレディに変更")
        self.machine.set_state('ready')
        self.reset_timer()
        self.presentation_queue.put(('UPDATE_GROUP_STATE_ENTRY', 'ready'))


class CharacterLongMoveStateMachine:
    """
    キャラクターが次にどこまで移動すべきかを、点列として管理する。リカバリーパスを選択する場合は、
    直近の座標から適切なリカバリーパスを検索し、点列を洗い替えることで経路を切り替える。
    また、リカバリーパスの終了時の元の経路への復帰も、当然ここで行う。
    """
    def __init__(self,
                 game_controller,
                 character_name,
                 group_state_machine,
                 presentation_queue,
                 sending_queue,
                 threading_lock):

        self.game_controller = game_controller      # あんまり親への参照を持つのはどうかと思うが。
        self.character_name = character_name
        self.group_state_machine = group_state_machine
        self.presentation_queue = presentation_queue
        self.sending_queue = sending_queue
        self.threading_lock = threading_lock        # キャラクターのデータを参照するので、ロックする機会がある。

        # 隣接する点の間の移動を管理するステートマシン
        self.character_short_move_state_machine = CharacterShortMoveStateMachine(game_controller,
                                                                                 character_name,
                                                                                 self,
                                                                                 presentation_queue,
                                                                                 sending_queue,
                                                                                 threading_lock)

        self.state_started_datetime = None          # 経過時間を確認するためのもの。
        # 実験用に、今のところ複数ゾーンのことは考えない。
        self.path_name_running = ''                 # 現在走行中のパスの名前
        self.zone_plan_index_fed = ''               # 現在供給済みのポイントの番号(ゾーン名+番号)
        self.zone_plan_index_reached = ''           # 現在到達済みのポイントの番号(ゾーン名+番号)

        # 現在走行中のパスについて。
        self.point_index_reached = 0                # 現在到達済みのポイントの番号(ポイント列内の)
        self.points = []                            # この点列に沿って移動する。通常は長さ1だが、リカバリーパスおよびリカバリーパスからの復帰後は複数ポイントを保持する。
        self.current_dest = None                    # 現在設定されている目的地。

        # ひとつ前のWaypointへ戻るケースを管理する変数
        self.previous_points = []                   # ひとつ前の移動で使った点列。
        self.current_retry_depth = 0                # 目的地に到達できないときに、複数のWaypointを戻ってリトライをかける。現在トライしている深さに相当。
        self.ez_recovery = False                    # イージーリカバリーモードかどうかを判定する。

        # リカバリーパス関係
        self.recovery_path_consumed = set()         # 直近のリカバリーで使用したリカバリーパスの集合。一度リカバリーに成功するとリセットする。
        self.diagonal_walk = False                  # 最大深さまで履歴を遡っても復帰できなかった場合のための、円周方向への移動の管理。円周方向とは言っても、少し後ろに戻す。
        self.diagonal_flipper = False               # 円周方向への移動時に、自座標と目標座標を結ぶ線の、どちらの側にランダムに動かすかを管理する。
        self.diagonal_setback = 0                   # 円周方向への移動時に、同時に少し後退させる。その量を管理する。

        # 状態の定義
        self.states = ['ready', 'follow_main_path', 'recovery']

        # 原則として、すべての遷移のafterでreset_timerを呼び出す。
        self.transitions = [
            {'trigger': 'check',
             'source': 'ready',
             'dest': 'follow_main_path',
             'conditions': 'condition_move_ok',
             'after': 'reset_timer'},

            {'trigger': 'check',
             'source': 'follow_main_path',
             'dest': 'ready',
             'conditions': 'condition_reached_group_destination',
             'after': 'reset_timer'},

            {'trigger': 'check',
             'source': 'follow_main_path',
             'dest': 'follow_main_path',
             'conditions': 'condition_moving'},     # afterはない（タイマーリセットしない）
        ]

        # transitionsライブラリは、modelに指定されたクラスを修飾してステートマシンの機能を付加するように動作する。
        self.machine = Machine(model=self,
                               states=self.states,
                               transitions=self.transitions,
                               initial='ready',
                               auto_transitions=False,
                               ordered_transitions=False,
                               title="",                        # GraphMachine用
                               show_auto_transitions=False,     # GraphMachine用
                               show_conditions=False)            # GraphMachine用

        # グラフ出力の時はプレゼンテーションキューがNoneなので、エラーを回避。
        if self.presentation_queue:
            self.presentation_queue.put(('UPDATE_CHARACTER_STATE_ENTRY', self.character_name, 'ready'))
            self.presentation_queue.put(('UPDATE_CHARACTER_CONSOLE', self.character_name, 'CS: created.'))

    def update(self):
        """このルーチンは、タイマーで一定時間ごとに呼び出される。"""
        for _trigger in self.machine.get_triggers(self.state):
            self.trigger(_trigger)
            # 自己遷移する場合は、beforeとかafterとかで処理する必要がある?

        # 短距離移動のステータスを更新する。
        self.character_short_move_state_machine.update()

    def reset_timer(self):
        """
        アクションの開始時刻を記録するためのコールバック。
        :return:
        """
        self.state_started_datetime = datetime.datetime.now()

    def lookup_lookdown(self):
        """ルックアップ/ルックダウン動作を行う。"""
        if gs.setting['LOOKUP_DOWN_ON_RECOVERY'] != 1:
            # オフなら実施しない。
            return
        _mean_x, _mean_y, _mean_h, _zone_id = self.game_controller.get_group_center()
        _my_height = self.game_controller.group.get_member(self.character_name).h

        # 高さを比較して、送るべきコマンドを生成。
        if _my_height > _mean_h:
            _sending_packet = '//keypress %s hold' % gs.setting['KEYMAP_LOOKDOWN']
        elif _my_height < _mean_h:
            _sending_packet = '//keypress %s hold' % gs.setting['KEYMAP_LOOKUP']
        else:
            # 完全に同じ高さということはあまりないと思うが。
            return

        # 強制的に周囲の死体を消す。ただし、現状このルーチンはゾーンが適正でなくても回るので、ゾーン違いの時にパケットの送信防止処置が必要。
        # ゾーン違うを検出したら特別なステートに移行させるような処理になったら、このチェックは不要になる。
        if self.game_controller.check_character_zone(self.character_name):
            self.sending_queue.put(lib.bc_commands.bct(self.character_name, _sending_packet))

    def condition_move_ok(self):
        """
        ready->follow_main_path
        移動が可能である。
        """
        if len(self.points) == 0 and self.point_index_reached >= len(self.points) - 1:
            # 点列が読み込まれていないか、末尾の点まで到達していたら、遷移しない。
            # 状態がレディで、points = []の場合は、この状態で待機することになる。
            return False

        # 円周方向移動設定のリセット。
        self.diagonal_walk = False
        self.diagonal_setback = gs.setting['DIAGONAL_WALK_SETBACK_START'] - gs.setting['DIAGONAL_WALK_SETBACK_INCREMENT']

        self.presentation_queue.put(('UPDATE_CHARACTER_STATE_ENTRY', self.character_name, 'follow_main_path'))
        self.presentation_queue.put(('UPDATE_CHARACTER_CONSOLE', self.character_name, 'CS: following main path.'))
        return True

    def condition_moving(self):
        """
        follow_main_path -> follow_main_path
        移動中のチェック。
        基本的に、常に自己遷移が発生する（Trueを返しても、Falseを返しても、follow_main_pathに遷移）。
        """
        # 移動管理のステートマシンが終了していたら、次の点を与える。
        if self.character_short_move_state_machine.state == 'ready' and self.character_short_move_state_machine.dest is None:
            if self.diagonal_walk:
                # ダイアゴナルウォークモードだった場合、終了して通常の処理に戻す。
                self.diagonal_walk = False
                return True

            elif self.point_index_reached < len(self.points):
                if self.current_retry_depth > 0:
                    # イージーリカバリーモード中の移動なら、ルックアップ／ダウンを行う。
                    self.lookup_lookdown()

                #print("%s: 次のウェイポイントを供給します。%s" % (self.character_name, self.point_index_reached))
                self.character_short_move_state_machine.set_dest(self.points[self.point_index_reached])
                self.character_short_move_state_machine.start_move()
                self.presentation_queue.put(('UPDATE_CHARACTER_CONSOLE',
                                             self.character_name,
                                             'CS: feeding next point(%d).' % self.point_index_reached))
            self.point_index_reached += 1
            #print("現在の履歴の長さ:%s:%d" % (self.character_name, len(self.previous_points)))
            return True

        if self.character_short_move_state_machine.state == 'cant_reach_destination':
            # 移動管理のステートマシンが目的地に到達できないといっていた場合。

            if not (self.group_state_machine.flag_play_one_step or self.group_state_machine.flag_auto_run):
                # オートランもワンステップランも不可になっていたら、それ以上何もしない。
                # リカバリーパスの使用もしない（のがいいだろう）ので、このステートマシンの移動失敗ステート(panic)に遷移すべき。
                return True

            # まだイージーリカバリーモードではなかった。
            if not self.ez_recovery:
                #print(self.previous_points)
                if len(self.previous_points) > 0:
                    # 利用できる履歴が存在する。
                    print("履歴からウェイポイントを取り出します。キャラクター:%s 履歴の長さ:%d 現在の深さ:1" % (self.character_name, len(self.previous_points)))
                    self.points = self.previous_points + self.points
                    self.point_index_reached += len(self.previous_points)
                    self.current_retry_depth = 0
                    self.ez_recovery = True

                else:
                    # 利用できる履歴が足りなかった。これは履歴を消費しつくしたときではなく、履歴のない状態で過去の履歴を使用する判定になったとき。
                    print("%s: まだ履歴がありません。ダイアゴナル移動を試行します。" % self.character_name)
                    self.try_diagonal_move()
                    self.point_index_reached = 0
                    return True

            if self.ez_recovery:
                # イージーリカバリーモードである。
                self.current_retry_depth += 1       # リトライの深さを一つ上げる。

                # 前回リカバリーをトライした点の一つ前に戻す。前回リカバリーパスを一つでも進行で来ていた場合を考慮し、単純に-=1ではなく、current_retry_depthを使って再計算する。、
                self.point_index_reached = len(self.previous_points) + 1 - self.current_retry_depth

                if self.point_index_reached >= 0:
                    _pt = self.points[self.point_index_reached]
                    self.character_short_move_state_machine.set_dest(_pt)
                    print("履歴を遡ります。キャラクター:%s, 深さ:%d, (%f %f) pir:%d" % (self.character_name,
                                                                        self.current_retry_depth,
                                                                        _pt.x,
                                                                        _pt.y,
                                                                        self.point_index_reached))

                    self.lookup_lookdown()
                    self.character_short_move_state_machine.start_move()  # 短距離移動ステートマシンをレディに戻す。
                    self.presentation_queue.put(('UPDATE_CHARACTER_CONSOLE',
                                                 self.character_name,
                                                 'CS: retrying previous waypoint(easy-recovery).'))
                    return True

                else:
                    # 履歴が足りない。ダイアゴナル移動を試行し、イージーリカバリーモードを解除する。
                    print("%s: 過去の履歴が足りません。ダイアゴナル移動を試行します。" % self.character_name)

                    # 進行をリセットする。…
                    self.ez_recovery = False
                    self.points = [self.current_dest]   # 初期状態に戻す
                    self.point_index_reached = 0
                    self.current_retry_depth = 0
                    self.try_diagonal_move()

                    return True

        # 何も起こっていないので、自己遷移する。
        return True

    def try_diagonal_move(self):
        """
        円周方向への移動をトライ。ステートは変わらない。
        """
        # 音を鳴らす。
        winsound.PlaySound(SOUND_TAKING_RECOVERY,
                           winsound.SND_FILENAME | winsound.SND_ASYNC)

        # ルックアップ／ルックダウンを行う。
        self.lookup_lookdown()

        # 強制的に周囲の死体を消す。ただし、現状このルーチンはゾーンが適正でなくても回るので、ゾーン違いの時にパケットの送信防止処置が必要。
        # ゾーン違うを検出したら特別なステートに移行させるような処理になったら、このチェックは不要になる。
        if self.game_controller.check_character_zone(self.character_name):
            _sending_packet = '//hidec NPC'
            self.sending_queue.put(lib.bc_commands.bct(self.character_name, _sending_packet))

        # 強制的にリカバリー状態に遷移。
        self.diagonal_walk = True
        self.diagonal_flipper = not self.diagonal_flipper   # 逆にする。
        self.diagonal_setback += gs.setting['DIAGONAL_WALK_SETBACK_INCREMENT']
        self.diagonal_setback = min(self.diagonal_setback, gs.setting['DIAGONAL_WALK_SETBACK_MAX'])

        ## 短距離移動ステートマシンに、移動先を供給する。
        # グループの目標位置からキャラクターの位置へのベクトルaを求める。
        _group_dest = self.game_controller.master_plan[0].main_path.path_points[self.group_state_machine.head_index]
        _my_character = self.game_controller.group.get_member(self.character_name)
        _vector_x = _my_character.x - _group_dest.x
        _vector_y = _my_character.y - _group_dest.y

        # 回転量をランダム生成。
        _rotate_angle = random.uniform(gs.setting['DIAGONAL_WALK_MIN_DEGREE'], gs.setting['DIAGONAL_WALK_MAX_DEGREE'])
        _rotate_angle = - _rotate_angle if self.diagonal_flipper else _rotate_angle
        _rotate_radian = math.radians(_rotate_angle)

        # 上記の回転量で、ベクトルaを回転する。
        _rotated_vector_x = _vector_x * math.cos(_rotate_radian)
        _rotated_vector_y = _vector_y * math.sin(_rotate_radian)

        # 長さの変更。
        _rotated_vector_len = math.sqrt(_rotated_vector_x ** 2 + _rotated_vector_y ** 2)
        _stretched_vector_x = _rotated_vector_x * (1.0 + self.diagonal_setback / _rotated_vector_len)
        _stretched_vector_y = _rotated_vector_y * (1.0 + self.diagonal_setback / _rotated_vector_len)

        # 目的地の設定。
        _dest_x = _group_dest.x + _stretched_vector_x
        _dest_y = _group_dest.y + _stretched_vector_y

        print("円周方向へのランダム移動および後退を試みます… Flip: %s, 後退量:%f, ランダム回転角:%.02f" % (self.diagonal_flipper,
                                                                           self.diagonal_setback, _rotate_angle))

        self.character_short_move_state_machine.set_dest(lib.plan.Point(_dest_x, _dest_y, 0))
        self.character_short_move_state_machine.start_move()
        self.presentation_queue.put(('UPDATE_CHARACTER_CONSOLE',
                                     self.character_name,
                                     'CS: diagonal walk recovery!'))

        return False

    def condition_reached_group_destination(self):
        """
        follow_main_path -> ready
        設定されたpointsの列を消化したら、readyに戻す。
        """
        # その時点でのself.pointsのすべてを消化した。
        _cond = self.point_index_reached > len(self.points)

        if _cond:
            #print(self.point_index_reached, len(self.points))
            #print("%s: CHR DESTに到達した！" % self.character_name)
            self.presentation_queue.put(('UPDATE_CHARACTER_STATE_ENTRY', self.character_name, 'ready'))
            self.presentation_queue.put(('UPDATE_CHARACTER_CONSOLE', self.character_name, 'CS: reached group dest.'))

            # ポイント履歴の保存。今回使ったポイント列を追加して、必要な長さに切り詰めておく。
            if self.current_retry_depth == 0:
                # リトライが発生していなかった場合は、すでにあるpointsの末尾に、現在のpointsを保存しておく。
                self.previous_points += self.points
            else:
                # イージーリカバリー後は、pointsの前にprevious_pointsがくっついているので、除外する。
                self.previous_points += self.points[len(self.previous_points):]

            # 切り詰め。
            self.previous_points = self.previous_points[-gs.setting['MAX_ROLLBACK_WAYPOINTS_FOR_EZ_RECOVERY']:]

            self.point_index_reached = 0
            self.current_retry_depth = 0
            self.ez_recovery = False
            self.points = []
            self.zone_plan_index_reached = self.zone_plan_index_fed
            return True

        return False

    def set_ready(self):
        print("%s: キャラクターステートを強制的にレディに変更。" % self.character_name)
        self.reset_timer()
        self.presentation_queue.put(('UPDATE_CHARACTER_STATE_ENTRY', self.character_name, 'ready'))
        self.presentation_queue.put(('UPDATE_CHARACTER_CONSOLE', self.character_name, 'CS: forced ready.'))


class CharacterShortMoveStateMachine:
    """
    キャラクターの、隣接するWaypoint間の移動を管理するステートマシン。
    ある点への移動を試み、できたかできなかっただけを管理するので、自分が全体の経路のどこを走っているのかなどは、ここでは気にする必要がない。
    ready状態の時に、外部からdestがセットされたことを検出することで、移動を開始する。
    """
    def __init__(self,
                 game_controller,
                 character_name,
                 character_long_move_state_machine,
                 presentation_queue,
                 sending_queue,
                 threading_lock):

        self.game_controller = game_controller      # あんまり親への参照を持つのはどうかと思うが。
        self.character_name = character_name
        self.character_long_move_state_machine = character_long_move_state_machine
        self.presentation_queue = presentation_queue
        self.sending_queue = sending_queue
        self.threading_lock = threading_lock        # キャラクターのデータを参照するので、ロックする機会がある。

        self.state_started_datetime = None          # 経過時間を確認するためのもの。
        self.dest = None                            # 現在の目的地。Pointオブジェクトをもつ。

        # 状態の定義
        self.states = ['ready', 'running', 'cant_reach_destination']

        # 原則として、すべての遷移のafterでreset_timerを呼び出す。
        self.transitions = [
            {'trigger': 'check',
             'source': 'running',
             'dest': 'ready',
             'conditions': 'condition_reached_destination',
             'after': 'reset_timer'},

            {'trigger': 'check',
             'source': 'running',
             'dest': 'cant_reach_destination',
             'conditions': 'condition_cant_reach_destination',
             'after': 'reset_timer'},
        ]

        # transitionsライブラリは、modelに指定されたクラスを修飾してステートマシンの機能を付加するように動作する。
        self.machine = Machine(model=self,
                               states=self.states,
                               transitions=self.transitions,
                               initial='ready',
                               auto_transitions=False,
                               ordered_transitions=False,
                               title="",                        # GraphMachine用
                               show_auto_transitions=False,     # GraphMachine用
                               show_conditions=False)            # GraphMachine用

        # グラフ出力の時はプレゼンテーションキューがNoneなので、エラーを回避。
        if self.presentation_queue:
            self.presentation_queue.put(('UPDATE_RUNNING_STATE_ENTRY', self.character_name, 'ready'))
            self.presentation_queue.put(('UPDATE_CHARACTER_CONSOLE', self.character_name, 'RS: ready.'))

    def update(self):
        """このルーチンは、タイマーで一定時間ごとに呼び出される。"""
        for _trigger in self.machine.get_triggers(self.state):
            self.trigger(_trigger)
            # 自己遷移する場合は、beforeとかafterとかで処理する必要がある。

    def reset_timer(self):
        """
        アクションの開始時刻を記録するためのコールバック。
        :return:
        """
        self.state_started_datetime = datetime.datetime.now()

    def set_dest(self, dest, salting=True):
        """
        移動先を設定するための関数。saltingがFalseのとき以外は、現在のゾーンに応じたランダム値を加味する。
        :param dest:
        :param salting:
        :return:
        """
        _my_zone_id = self.game_controller.group.get_member(self.character_name).zone_id
        _my_zone_shortname = db.get_zone_shortname(_my_zone_id)

        # 現在のゾーンが期待されたゾーンと違う場合は、移動させない。しかし、すでに送信キューに送ってしまったそのまま送られてしまう。
        # 積極的に移動を抑止するため、当面は現在の座標を移動先として送信して、ステートとしては何も変更しないことにする。
        # 「ゾーン違いによって移動を抑止している」というステートを別に設けた場合は、この処理は必要なくなるだろう。
        #if _my_zone_id != self.game_controller.master_plan[0].zone_id:
        if not self.game_controller.check_character_zone(self.character_name):
            print("%s: キャラクターがいるのは、期待されたゾーンではありません。移動を中止します。" % self.character_name)
            self.dest = None    # Noneでいいのか？

            # 現在地への移動をMQ2に指示する。前のゾーンでの移動先を送信キューに入れていた場合に、事実上のキャンセル処理になる。
            _me = self.game_controller.group.get_member(self.character_name)
            _sending_packet = '//moveto loc %f %f ' % (_me.y, _me.x)
            self.sending_queue.put(lib.bc_commands.bct(self.character_name, _sending_packet))

            return

        if not salting:
            self.dest = dest
            return

        # 現在のゾーンショートネームが目的地のランダマイズを必要とする場合は、ランダム値を加味する。
        if _my_zone_shortname in gs.dest_loc_salt.keys():
            #print("%s: ランダマイズ対象ゾーンなので、移動先にランダム値を加味します。ランダム幅は、%f" % (self.character_name, gs.dest_loc_salt[_my_zone_shortname]))
            self.dest = dest.get_salted_point(gs.dest_loc_salt[_my_zone_shortname])
        else:
            self.dest = dest

    def calc_dest(self):
        """このキャラクターが、destまでどれだけの距離かを返す。"""
        _character = self.game_controller.group.members[self.character_name]
        _x, _y, _h = _character.x, _character.y, _character.h
        return math.sqrt((_x - self.dest.x) ** 2 + (_y - self.dest.y) ** 2)  # とりあえず高度を考慮しない。

    def start_move(self):
        """
        ready -> running
        走行する。
        """
        # 次の移動先が指示されていた場合、そこを目標に定め、runningに遷移する。
        # 次のウェイポイントが近すぎる場合は、movetoを送信しない。
        if self.dest:
            self.machine.set_state('running')
            self.reset_timer()

            _distance = self.calc_dest()
            if _distance < gs.setting['ARRIVAL_DISTANCE_THRESHOLD']:
                print("次のウェイポイントが近いため、移動パケットの送信をスキップします。")
                self.presentation_queue.put(('UPDATE_CHARACTER_CONSOLE',
                                             self.character_name,
                                             'RS: next waypoint is very close. skipping move.'))
                # 移動せずに移動成功した扱いになる。
                return False
            else:
                _sending_packet = '//moveto loc %f %f ' % (self.dest.y, self.dest.x)
                self.sending_queue.put(lib.bc_commands.bct(self.character_name, _sending_packet))
                self.presentation_queue.put(('UPDATE_RUNNING_STATE_ENTRY', self.character_name, 'running'))
                self.presentation_queue.put(('UPDATE_CHARACTER_CONSOLE', self.character_name, 'RS: %s.' % _sending_packet))
                return True
        else:
            print("%s: 移動指示が出ましたが、移動先が設定されていません。" % self.character_name)

    def condition_reached_destination(self):
        """
        running -> ready
        目的地に着いた。
        """
        with self.threading_lock:
            # 次の目標座標と、現在の座標の差を計算する。
            _distance = self.calc_dest()
            self.presentation_queue.put(('UPDATE_CHARACTER_CONSOLE', self.character_name, 'RS: -> %f' % _distance))

            # 到着したかどうか判定し、到着していたら目標地点をリセットし、readyに遷移する。
            if _distance < gs.setting['ARRIVAL_DISTANCE_THRESHOLD']:
                self.dest = None
                self.presentation_queue.put(('UPDATE_RUNNING_STATE_ENTRY', self.character_name, 'ready'))
                self.presentation_queue.put(('UPDATE_CHARACTER_CONSOLE', self.character_name, 'RS: ready.'))
                return True

        return False

    def condition_cant_reach_destination(self):
        """
        running -> cant_reach_destination
        指定時間以内に到達できなかった。
        """
        if datetime.datetime.now() > self.state_started_datetime + datetime.timedelta(seconds=gs.setting['ARRIVAL_WAIT_SEC']):
            print("STATE:%s: run -> cant reach destination" % self.character_name)
            self.presentation_queue.put(('UPDATE_RUNNING_STATE_ENTRY', self.character_name, 'cant_reach_destination'))
            self.presentation_queue.put(('UPDATE_CHARACTER_CONSOLE', self.character_name, 'RS: cant reach!'))

            # 到達できなかったことを通知する音を鳴らす。
            winsound.PlaySound(SOUND_CANT_REACH_DESTINATION,
                               winsound.SND_FILENAME | winsound.SND_ASYNC)
            return True

        return False


# このコード単体で実行した場合に、グラフ類を出力。
if __name__ == '__main__':
    _gs = GroupStateMachine(None, None, None, None)
    _gs_graph = _gs.machine.get_graph()
    graphviz.Source(_gs_graph, filename='group_state_machine_diagram', format='png').render(cleanup=True)

    _cs = CharacterLongMoveStateMachine(None, None, None, None, None, None)
    _cs_graph = _cs.machine.get_graph()
    graphviz.Source(_cs_graph, filename='character_state_machine_diagram', format='png').render(cleanup=True)

    _ms = CharacterShortMoveStateMachine(None, None, None, None, None)
    _ms_graph = _ms.machine.get_graph()
    graphviz.Source(_ms_graph, filename='move_state_machine_diagram', format='png').render(cleanup=True)
