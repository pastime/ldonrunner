import tkinter as tk
import tkinter.ttk as ttk
from tkinter import scrolledtext
import datetime
import queue
import winsound

# ユーザー定義ライブラリ
from lib.gui_settings import *
import database.encyclopedia as db
from lib.gui_editorframe import EditorFrame
from lib.gui_mapwindow import MapWindow
from lib.gui_characterframe import CharacterFrame
from lib.gui_controlframe import ControlFrame
from lib.gui_playframe import PlayFrame
import lib.game_settings as gs

"""
GUIはttkthemeで細かいウィジェットの見た目を調整する。この場合、rootとなるウィンドウをThemedTkオブジェクトで作る必要がある。
rootなウィンドウはworker.pyのThreadController内で実施しているので注意。
また、テーマが適用されるのはあくまでもttkのウィジェットで、これはtkのウィジェットと混在させられるが、
tk.Textに相当するttkウィジェットがないため、テキストボックスは別途色を調整する必要がある。うーん、いまいち。
色は以下を参考に。
http://www.science.smith.edu/dftwiki/index.php/Color_Charts_for_TKinter
"""


class MainWindow:
    """
    メインとなるウィンドウの定義。
    """
    def __init__(self,
                 master,
                 presentation_queue,
                 game_controller_queue,
                 end_application_callback):

        # 各種キューへの参照。
        self.presentation_queue = presentation_queue
        self.game_controller_queue = game_controller_queue

        # GUIを作ってゆく。
        self.root = master
        self.root.title(MAIN_WINDOW_TITLE)
        self.root.configure(background=WINDOW_BG)
        self.root.geometry("%dx%d" % (MAIN_WINDOW_WIDTH, MAIN_WINDOW_HEIGHT))

        # キャラクター設定関係
        self.characters = {0: 'None', 1: 'None', 2: 'None', 3: 'None', 4: 'None', 5: 'None'}
        self.CharacterPains = {}

        for _character_num, _character_name in self.characters.items():
            self.CharacterPains[_character_num] = CharacterFrame(self, _character_num, _character_name)
            self.CharacterPains[_character_num].grid(row=0, column=_character_num)

        # TELL TEXT BOX
        self.lb_tell_box = ttk.Label(self.root,
                                     text="Tells received/misc information",
                                     font=(SMALL_FONT, 8),
                                     foreground=WINDOW_TEXT_FG,
                                     anchor=tk.W)
        self.tb_tell_box = tk.scrolledtext.ScrolledText(self.root, width=TELL_TEXT_BOX_WIDTH, height=TELL_TEXT_BOX_LINES)
        self.tb_tell_box.configure(font=(MID_FONT, 8),
                                   bg=TEXT_BOX_BG,
                                   fg=TEXT_BOX_FG,
                                   state='disabled')

        self.tb_tell_box.tag_configure("ITEM_COLOR", foreground=TEXT_ITEM_FG)
        self.tb_tell_box.tag_configure("TIME_COLOR", foreground=TEXT_TIME_FG)
        self.tb_tell_box.tag_configure("NAME_COLOR", foreground=TEXT_NAME_FG)
        self.tb_tell_box.insert('1.0', '')
        self.lb_tell_box.grid(row=2, column=4, columnspan=2, sticky=tk.W + tk.E)
        self.tb_tell_box.grid(row=3, column=4, columnspan=2, sticky=tk.W + tk.E)

        # CONSOLE TEXT BOX
        self.lb_console_box = ttk.Label(self.root,
                                        text="ldonrunner console",
                                        font=(SMALL_FONT, 8),
                                        foreground=WINDOW_TEXT_FG,
                                        anchor=tk.W)
        self.tb_console_box = tk.scrolledtext.ScrolledText(self.root, width=TELL_TEXT_BOX_WIDTH, height=TELL_TEXT_BOX_LINES)
        self.tb_console_box.configure(font=(MID_FONT, 8),
                                      bg=TEXT_BOX_BG,
                                      fg=TEXT_BOX_FG,
                                      state='disabled')
        self.lb_console_box.grid(row=4, column=4, columnspan=2, sticky=tk.W + tk.E)
        self.tb_console_box.grid(row=5, column=4, columnspan=2, sticky=tk.W + tk.E + tk.S)

        # Control Frame
        self.control_frame = ControlFrame(self, game_controller_queue)
        self.control_frame.grid(row=2, column=0, rowspan=5, sticky=tk.N)

        # PlayFrame
        self.play_frame = PlayFrame(self)

        # EditorのFrame
        self.editor_frame = EditorFrame(self)

        # マップウィンドウ。子ウィンドウにするため、ウィンドウのほかに、アプリケーションとしてのオブジェクトを保持する必要がある。
        self.map_window_app = None
        self.map_window = None

        # self.rootのmainloop()は、このクラスの外で回す。

    def assign_new_character(self, character_name):
        """GUIにキャラクターをアサイン。"""
        # 無視すべきキャラクター名の場合は、追加しない。
        if character_name.upper() in gs.ignore_character:
            return False

        _character_ix = self.get_character_number('None')
        if _character_ix == -1:
            print('too many characters connected!(over 6)')
            return False
        else:
            self.characters[_character_ix] = character_name
            return True

    def remove_character(self, character_name):
        """GUIからキャラクターを削除。"""

        _character_ix = self.get_character_number(character_name)

        if _character_ix == -1:
            print('no such character name(%s)!' % character_name)
        else:
            # STATUSとBUFFS命令実行時に、GUIに登録されていないキャラクターを自動登録するようにしている。
            # presentationキューは別スレッドで管理されているため、この関数内で直ちにGUIからキャラクターを削除しても、
            # ステータスウィンドウとバフウィンドウを空にしようとした際に再登録されてしまうため、
            # また登録されてしまうため、キャラクターの削除もキューで処理する必要がある。
            self.presentation_queue.put(("STATUS", character_name, {}))
            self.presentation_queue.put(("BUFFS", character_name, ""))
            self.presentation_queue.put(("CHAR_DELETE", character_name))

    def get_character_number(self, character_name):
        """キャラクター名から表示位置を取得。"""
        for _ix in range(6):
            if self.characters[_ix] == character_name:
                return _ix
        return -1

    def get_character_pane(self, character_name):
        _char_num = self.get_character_number(character_name)
        if _char_num == -1:
            return None
        else:
            return self.CharacterPains[_char_num]

    def process_incoming(self):
        """プレゼンテーションキューに何かあれば、それに従ってGUI情報を更新する。"""
        while self.presentation_queue.qsize():
            try:
                _msg = self.presentation_queue.get()

                if len(_msg) == 3 and _msg[0] == 'STATUS':
                    # キャラクターステータス値の更新要求があった
                    if _msg[1] not in self.characters.values():
                        if not self.assign_new_character(_msg[1]):
                            return  # 6人以上登録されていたので、書き込む場所がない

                    self.CharacterPains[self.get_character_number(_msg[1])].update_status(_msg[2])

                elif len(_msg) == 2 and _msg[0] == 'CHAR_QUIT':
                    # キャラクターがログアウトした。
                    self.remove_character(_msg[1])

                elif len(_msg) == 3 and _msg[0] == 'BUFFS':
                    # バフの更新要求があった。
                    if _msg[1] not in self.characters.values():
                        if not self.assign_new_character(_msg[1]):
                            return  # 6人以上登録されていたので、書き込む場所がない

                    # 長い表示を分割。
                    if len(_msg[2]) > BUFF_BOX_LINES:
                        _buffs1 = _msg[2][:20]
                        _buffs2 = _msg[2][20:]
                    else:
                        _buffs1 = _msg[2]
                        _buffs2 = ''

                    # ステータス表示を更新
                    self.CharacterPains[self.get_character_number(_msg[1])].update_buffs1(_buffs1)
                    self.CharacterPains[self.get_character_number(_msg[1])].update_buffs2(_buffs2)

                elif len(_msg) == 2 and _msg[0] == 'CHAR_DELETE':
                    # GUI上の登録からキャラクターを削除する。
                    # 削除されたキャラクターの表示更新をすべて終えてから実行するように、キューへの登録を調整すること。
                    _character_ix = self.get_character_number(_msg[1])
                    if _character_ix == -1:
                        print('no such character name(%s)!' % _msg[1])
                    else:
                        self.characters[_character_ix] = 'None'

                elif len(_msg) == 2 and _msg[0] == 'TELL_RECEIVED':
                    # Tellを受信した。
                    _time_text = datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")      # ログファイル用
                    _short_time_text = datetime.datetime.now().strftime("%H:%M:%S")         # 表示用

                    self.tb_tell_box.configure(state=tk.NORMAL)
                    # ヘッダ部分をテキストボックスに追加する。
                    self.tb_tell_box.insert(tk.END, "[%s] " % _short_time_text, 'TIME_COLOR')

                    self.tb_tell_box.insert(tk.END, "%s->%s: " % (_msg[1][0].decode('utf-8'),
                                                                  _msg[1][1].decode('utf-8')),
                                            'NAME_COLOR')

                    # メッセージ部分をテキストボックスに追加する。
                    _split_message_body = _msg[1][2].split(b'\x12')         # アイテムリンクがある場合、\x12で挟まれている。

                    for _ix in range(len(_split_message_body)):
                        if _ix % 2 == 0:
                            # 偶数番目の要素は、そのまま文字列にする。
                            self.tb_tell_box.insert(tk.END, _split_message_body[_ix].decode('utf-8'))

                        elif len(_split_message_body[_ix]) > 56:
                            # 奇数番目の要素は、アイテムへのリンクタグを削除してから文字列にする。
                            self.tb_tell_box.insert(tk.END,
                                                    _split_message_body[_ix][56:].decode('utf-8'),
                                                    'ITEM_COLOR')

                        else:
                            # 奇数番目かつリンク部分の長さが56文字という想定以外の場合
                            self.tb_tell_box.insert(tk.END, _split_message_body[_ix].decode('utf-8'))

                    # 末尾に改行を付加し、表示を最下段まで進める。
                    self.tb_tell_box.insert(tk.END, "\n")
                    self.tb_tell_box.see("end")

                    self.tb_tell_box.configure(state=tk.DISABLED)

                    # ピーンと鳴らす。
                    winsound.PlaySound(SOUND_MESSAGE,
                                       winsound.SND_FILENAME | winsound.SND_ASYNC)

                elif len(_msg) == 2 and _msg[0] == 'MISC_INFO':
                    # tells領域に対する各種情報表示の処理。
                    _time_text = datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")      # ログファイル用
                    _short_time_text = datetime.datetime.now().strftime("%H:%M:%S")         # 表示用

                    self.tb_tell_box.configure(state=tk.NORMAL)
                    # ヘッダ部分をテキストボックスに追加する。
                    self.tb_tell_box.insert(tk.END, "[%s] " % _short_time_text, 'TIME_COLOR')

                    # _msg[1]は、('キャラクター名', 'テキスト')の形式。
                    self.tb_tell_box.insert(tk.END, "%s: " % (_msg[1][0]), 'NAME_COLOR')
                    self.tb_tell_box.insert(tk.END, _msg[1][1])

                    # 末尾に改行を付加し、表示を最下段まで進める。
                    self.tb_tell_box.insert(tk.END, "\n")
                    self.tb_tell_box.see("end")

                    self.tb_tell_box.configure(state=tk.DISABLED)

                elif len(_msg) == 2 and _msg[0] == 'WRITE_TO_CONSOLE':
                    # GUI上のコンソールへのテキスト追加命令を受け取った。
                    _time_text = datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")  # ログファイル用
                    _short_time_text = datetime.datetime.now().strftime("%H:%M:%S")  # 表示用

                    self.tb_console_box.configure(state=tk.NORMAL)
                    self.tb_console_box.insert(tk.END, "[%s] %s\n" % (_short_time_text, _msg[1]))
                    self.tb_console_box.see("end")
                    self.tb_console_box.configure(state=tk.DISABLED)

                elif len(_msg) == 2 and _msg[0] == 'SET_MAPPER':
                    """マッパーを変更。"""
                    self.editor_frame.changer_mapper(_msg[1].decode('utf-8'))

                elif len(_msg) == 2 and _msg[0] == 'RECORD_LOCATION':
                    """
                    マッパーが送信してきた座標を記録する。
                    """
                    _character = _msg[1][0].decode('utf-8')
                    _loc_etc = _msg[1][1].decode('utf-8')
                    self.editor_frame.add_point_from_text(_character, _loc_etc)

                elif len(_msg) == 2 and _msg[0] == 'UPDATE_GROUP_STATE_ENTRY':
                    """
                    Play Frameのグループステート表示欄を更新する。
                    """
                    self.play_frame.update_group_state_entry(_msg[1])

                elif len(_msg) == 3 and _msg[0] == 'UPDATE_CHARACTER_STATE_ENTRY':
                    """
                    Character Frameのキャラクターステート表示欄を更新する。
                    """
                    self.CharacterPains[self.get_character_number(_msg[1])].update_character_state_entry(_msg[2])

                elif len(_msg) == 3 and _msg[0] == 'UPDATE_RUNNING_STATE_ENTRY':
                    """
                    Character Frameのキャラクターごとの走行状態ステート表示欄を更新する。
                    """
                    self.CharacterPains[self.get_character_number(_msg[1])].update_running_state_entry(_msg[2])

                elif len(_msg) == 3 and _msg[0] == 'UPDATE_CHARACTER_CONSOLE':
                    """
                    Character FrameのキャラクターごとのProgressionウィンドウに一行追加する。。
                    """
                    self.CharacterPains[self.get_character_number(_msg[1])].update_state_console_textbox(_msg[2])

                elif len(_msg) == 2 and _msg[0] == 'UPDATE_PROGRESSION_LINE_HIGHLIGHT':
                    """
                    Progressionウィンドウの指定行をハイライトする。
                    """
                    self.play_frame.update_highlighted_line_of_progression(_msg[1])

                elif len(_msg) == 2 and _msg[0] == 'COMBAT STARTED':
                    """戦闘開始が報告された。"""
                    if _msg[1] not in self.characters.values():
                        if not self.assign_new_character(_msg[1]):
                            return  # 6人以上登録されていたので、書き込む場所がない

                    # ステータス表示を更新
                    self.CharacterPains[self.get_character_number(_msg[1])].combat_state = True

                elif len(_msg) == 2 and _msg[0] == 'COMBAT ENDED':
                    """戦闘終了が報告された。"""
                    if _msg[1] not in self.characters.values():
                        if not self.assign_new_character(_msg[1]):
                            return  # 6人以上登録されていたので、書き込む場所がない

                    # ステータス表示を更新
                    self.CharacterPains[self.get_character_number(_msg[1])].combat_state = False

                elif len(_msg) == 2 and _msg[0] == 'SET TANK':
                    """タンクを変更。"""
                    for _character_name in self.characters.values():
                        if _character_name == 'None':
                            continue
                        if _character_name == _msg[1]:
                            self.CharacterPains[self.get_character_number(_character_name)].is_main_tank = True
                        else:
                            self.CharacterPains[self.get_character_number(_character_name)].is_main_tank = False

                elif len(_msg) == 2 and _msg[0] == 'SET BUFFER':
                    """Bufferを変更。"""
                    for _character_name in self.characters.values():
                        if _character_name == 'None':
                            continue
                        if _character_name == _msg[1]:
                            self.CharacterPains[self.get_character_number(_character_name)].is_main_buffer = True
                        else:
                            self.CharacterPains[self.get_character_number(_character_name)].is_main_buffer = False

                elif len(_msg) == 2 and _msg[0] == 'UPDATE CLOCK':
                    """時計の時刻をアップデート。"""
                    self.control_frame.clock_update(_msg[1])

                elif len(_msg) == 2 and _msg[0] == 'UPDATE REMAIN TIME':
                    """タイマーの残り時間をアップデート。"""
                    self.control_frame.remain_time_update(_msg[1])

                '''
                elif len(_msg) == 2 and _msg[0] == 'FIND_CLOSEST_MAIN_PATH_LOCATION':
                    """
                    Mainパス上の、もっとも現在のマッパーの座標に近い点にエディタのカーソルを移動させる。
                    """
                    print("finding closest loc on the main path....")
                    # まだスタブ。この関数必要なようなそうでもないような？
                '''

            except queue.Empty:
                # 基本的にここには来ないはず。
                pass

    def show_map(self):
        """
        マップを表示する。マップウィンドウはモーダルではないので、既に表示されていても、Show Mapボタンが押されることがある。
        マップウィンドウをXボタン等で閉じても、いきなりオブジェクトが破壊されて、参照がNoneになるわけではないので、
        マップウィンドウを閉じたときに、明示的に参照を解放するコールバックを用意し、マップウィンドウを閉じる際に呼び出す。
        :return:
        """
        if not self.map_window:
            self.map_window = MapWindow(self.root, parent=self, on_closing=self.destroy_map_window)
            self.map_window.load_base_map(db.get_zone_shortname(self.editor_frame.zone_plan.zone_id))
            self.map_window.update_all_map()
        else:
            self.map_window.update_all_map()

    def destroy_map_window(self):
        """マップウィンドウを閉じる。"""
        self.map_window.destroy()
        self.map_window = None


#いったん移動機能に戻る。
#TODO: リカバリーパスの追加機能。
#TODO: リカバリーパスの削除機能。
#TODO: リカバリーパスの選択機能。

#TODO: TreeViewは、機能が多くなってきたので別クラスに分けたほうがよいか。->完成してからリファクタするときに考える。
