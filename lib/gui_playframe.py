import tkinter as tk
import tkinter.ttk as ttk
from tkinter import scrolledtext
import tkinter.filedialog
import os
import sys

# ユーザー定義ライブラリ
from lib.tk_tooltip import CreateToolTip
from lib.gui_settings import *
import lib.plan

#TODO: PlayFrameのためにMapWindowを別途使用するなら、MapWindowとEditorWindowの結合を分離する必要あり。

# 使い方の想定…Planファイルを開く。zone_planを列挙したもの。
# Playすると、zone_planを順番に処理していく。
# 原則的にゲームコントローラがプランの実行処理を行うので、ここはプレゼンテーション層となる。
# プレゼンテーションキューにPFとかプレフィックスがついていたら、ここの処理ルーチンに回してくるとかがいいかも。
# また、ボタン類による指示は、root.game_controller_queueを介してゲームコントローラに送信する。


class PlayFrame(ttk.Frame):
    """
    プランを実行する機能を実現するためのフレーム。
    """
    def __init__(self, master):
        super(PlayFrame, self).__init__()

        self.root = master
        self.configure(relief="ridge",
                       width=CHAR_FRAME_WIDTH * 3)

        # 一般的な変数。
        self.master_plan_filename = ''              # 読み込み済みのマスタープランの名前。
        self.selected_zoneplan_id = 0               # プログレッションのペインに表示するゾーンプランの番号（マスタープラン内での）

        # プログレッションのテキストボックスに表示するデータを管理する。部分的に文字色を変えたいので、文字列のリストではなく、文字列のリストのリストとする。
        # zonename, index, loc, actions
        self.progression_texts = []

        ################### GUIの定義。まずは左の列から ########################
        # 左の列にボタン類を並べ、右の列にポイントのリストを置きたい。
        # 右の列がgridの何カラム目から開始するかを簡単に管理するため、一時的な変数を置く。
        _right_column_starting = 12

        # ttkのスタイルの変更方法はややこしい。新スタイル名は文字列なので自由につけられそうに思えるが、
        # my.TButtonはTButtonを継承したmyというスタイルという意味で、TButton部分を変えるとエラーになる。
        _style = ttk.Style()
        _style.configure('my.TButton', font=('Helvetica', 8))

        # 現在選択されているマスタープラン名を表示。
        self.en_master_plan_name = ttk.Entry(self, width=8)
        self.en_master_plan_name.insert(0, 'no master plan loaded')
        self.en_master_plan_name.configure(font=(SMALL_FONT, 8),
                                           background=TEXT_BOX_BG,
                                           foreground=TEXT_BOX_FG,
                                           state='readonly')
        self.en_master_plan_name.grid(row=0, column=0, columnspan=12, sticky=tk.W + tk.E)

        # Load, Set Headボタンを表示
        self.btn_load = ttk.Button(self,
                                   text='Load',
                                   style='my.TButton',
                                   width=4,
                                   command=self.load_master_plan)
        self.btn_load.grid(row=1, column=0, columnspan=4, sticky=tk.W + tk.E)

        self.btn_set_head = ttk.Button(self,
                                       text='SeekH',
                                       style='my.TButton',
                                       width=4,
                                       command=self.seek_head)
        self.btn_set_head.grid(row=1, column=4, columnspan=4, sticky=tk.W + tk.E)

        self.btn_set_head = ttk.Button(self,
                                       text='ResetH',
                                       style='my.TButton',
                                       width=4,
                                       command=self.reset_head)
        self.btn_set_head.grid(row=1, column=8, columnspan=4, sticky=tk.W + tk.E)

        # Playボタン。
        self.btn_play = ttk.Button(self,
                                   text='Play',
                                   style='my.TButton',
                                   width=3,
                                   command=self.start_autoplay)
        self.btn_play.grid(row=2, column=0, columnspan=4, sticky=tk.W + tk.E)

        # Stopボタン。
        self.btn_stop = ttk.Button(self,
                                   text='Stop',
                                   style='my.TButton',
                                   width=3,
                                   command=self.stop_autoplay)
        self.btn_stop.grid(row=2, column=4, columnspan=4, sticky=tk.W + tk.E)

        # Play Oneボタン。
        self.btn_play_one = ttk.Button(self,
                                       text='>One',
                                       style='my.TButton',
                                       width=3,
                                       command=self.play_one_step)
        self.btn_play_one.grid(row=2, column=8, columnspan=4, sticky=tk.W + tk.E)

        # MasterPlanという表示。
        self.lb_master_plan = ttk.Label(self,
                                        text="Master Plan",
                                        font=(SMALL_FONT, 8),
                                        foreground=WINDOW_TEXT_FG,
                                        anchor=tk.W)
        self.lb_master_plan.grid(row=3, column=0, columnspan=12, sticky=tk.W + tk.E)

        # MasterPlan TEXT BOX
        self.tb_master_plan_box = tk.scrolledtext.ScrolledText(self,
                                                               width=MASTER_PLAN_TEXT_BOX_WIDTH,
                                                               height=MASTER_PLAN_TEXT_BOX_LINES)
        self.tb_master_plan_box.configure(font=(MID_FONT, 8),
                                          bg=TEXT_BOX_BG,
                                          fg=TEXT_BOX_FG)
        self.tb_master_plan_box.grid(row=4, column=0, columnspan=12, rowspan=7, sticky=tk.W + tk.E)

        # マップを表示、または更新。
        self.btn_show_map = ttk.Button(self, text='Show Map', command=self.root.show_map)
        self.btn_show_map.grid(row=11, column=0, columnspan=12, sticky=tk.W + tk.E)

        ################### ここから右の列 ########################

        # Progression TEXT BOX
        # 現在選択されているマスタープラン名を表示。
        self.en_group_state = ttk.Entry(self, width=8)
        self.en_group_state.insert(0, 'Groupstate: no state yet.')
        self.en_group_state.configure(font=(SMALL_FONT, 8),
                                      background=TEXT_BOX_BG,
                                      foreground=TEXT_BOX_FG,
                                      state='readonly')
        self.en_group_state.grid(row=0, column=_right_column_starting, sticky=tk.W + tk.E)

        # 現在ロードされているゾーンプランを連結して表示。
        self.tb_progression_box = tk.scrolledtext.ScrolledText(self,
                                                               width=PROGRESSION_TEXT_BOX_WIDTH,
                                                               height=PROGRESSION_TEXT_BOX_LINES)
        self.tb_progression_box.configure(font=(MONOSPACED_FONT, 8),
                                          bg=TEXT_BOX_BG,
                                          fg=TEXT_BOX_FG,
                                          state=tk.DISABLED)
        self.tb_progression_box.grid(row=1, column=_right_column_starting, rowspan=13, sticky=tk.W + tk.E + tk.N)
        self.tb_progression_box.tag_configure("INDEX_COLOR", foreground=TEXT_PROGRESSION_INDEX_FG)
        self.tb_progression_box.tag_configure("ZONE_COLOR", foreground=TEXT_PROGRESSION_ZONE_FG)
        self.tb_progression_box.tag_configure("LOC_X_COLOR", foreground=TEXT_PROGRESSION_LOC_X_FG)
        self.tb_progression_box.tag_configure("LOC_Y_COLOR", foreground=TEXT_PROGRESSION_LOC_Y_FG)
        self.tb_progression_box.tag_configure("LOC_Z_COLOR", foreground=TEXT_PROGRESSION_LOC_H_FG)
        self.tb_progression_box.tag_configure("ACTIONS_COLOR", foreground=TEXT_PROGRESSION_ACTIONS_FG)
        self.tb_progression_box.tag_configure("CURRENT_LINE", background=TEXT_CURRENT_LINE_BG)
        #self.tb_selected_progression_box_line_id = -1   # ハイライトする行。最初の値-1は無効な値とする。

    def load_master_plan(self):
        """マスタープランを読み込む。"""
        _initial_dir = os.path.join(os.path.abspath(os.path.dirname(sys.argv[0])), 'path')
        _file_type = [("マスタープランファイル", "*.txt")]
        _master_plan_file_name = tkinter.filedialog.askopenfilename(filetypes=_file_type,
                                                                    initialdir=_initial_dir)

        if not _master_plan_file_name:
            print("キャンセルされました。")
            return

        # ここでマスタープランをGUIに反映し、さらにゾーンプランを読み込む。
        with open(_master_plan_file_name, "r") as _fd:
            _zone_plan_filenames = _fd.readlines()

        # \nが邪魔
        _zone_plan_filenames = [_x.strip() for _x in _zone_plan_filenames]

        # 既存のデータをリセットする。
        self.root.game_controller_queue.put(('RESET_MASTERPLAN',))
        self.progression_texts = []

        # 表示上のリセット。
        self.tb_master_plan_box.configure(state=tk.NORMAL)
        self.en_master_plan_name.configure(state='normal')

        self.tb_master_plan_box.delete('1.0', 'end')
        self.en_master_plan_name.delete(0, tk.END)
        self.en_master_plan_name.insert(0, os.path.basename(_master_plan_file_name))
        _master_plan_wide_index = 0

        for _tmp_filename in _zone_plan_filenames:
            _tmp_zone_plan = lib.plan.ZonePlan()
            _tmp_zone_plan.load_zone_plan(os.path.join(_initial_dir, _tmp_filename))

            # gamecontrollerにゾーンプランを登録し、GUIを更新する。
            self.root.game_controller_queue.put(('ADD_ZONEPLAN_TO_MASTERPLAN', _tmp_zone_plan))
            self.tb_master_plan_box.insert('end', "%s\n" % _tmp_filename)
            print("%s をロードしました。" % _tmp_filename)

            _zone_plan_wide_index = 0
            for _point in _tmp_zone_plan.main_path.path_points:
                _tmp_progression_line = [_master_plan_wide_index,
                                         _tmp_zone_plan.zone_plan_name[:-5],
                                         _zone_plan_wide_index,
                                         (_point.x, _point.y, _point.h),
                                         _point.actions]
                self.progression_texts.append(_tmp_progression_line)
                _master_plan_wide_index += 1
                _zone_plan_wide_index += 1

        self.tb_master_plan_box.configure(state=tk.DISABLED)
        self.en_master_plan_name.configure(state='readonly')

        self.update_progression()

    def update_progression(self):
        """プログレッションを表示するテキストボックスの更新。"""
        self.tb_progression_box.configure(state=tk.NORMAL)
        self.tb_progression_box.delete('1.0', 'end')
        for _line in self.progression_texts:
            self.tb_progression_box.insert(tk.END, "%s" % _line[1], 'ZONE_COLOR')
            self.tb_progression_box.insert(tk.END, ":")
            self.tb_progression_box.insert(tk.END, f'{_line[2]:3d}', 'INDEX_COLOR')
            self.tb_progression_box.insert(tk.END, ":")
            self.tb_progression_box.insert(tk.END, f' x{_line[3][0]:8.2f}', 'LOC_X_COLOR')
            self.tb_progression_box.insert(tk.END, f' y{_line[3][1]:8.2f}', 'LOC_Y_COLOR')
            self.tb_progression_box.insert(tk.END, f' h{_line[3][2]:7.2f}', 'LOC_H_COLOR')
            if len(_line[4]) > 0:
                self.tb_progression_box.insert(tk.END, " %s" % _line[4], 'ACTIONS_COLOR')
            self.tb_progression_box.insert(tk.END, "\n")
        self.tb_progression_box.configure(state=tk.DISABLED)

    def update_highlighted_line_of_progression(self, index):
        """指定行だけをハイライト。"""
        self.tb_progression_box.configure(state=tk.NORMAL)
        self.tb_progression_box.tag_remove("CURRENT_LINE", 1.0, "end")  # 全体からハイライト業の指定を削除。もし遅かったら、行ワイズに書き換える。
        self.tb_progression_box.tag_add("CURRENT_LINE",
                                        "%d.0" % index,
                                        "%d.end+1c" % index)
        self.tb_progression_box.see("%d.0" % index)
        self.tb_progression_box.configure(state=tk.DISABLED)

    def update_group_state_entry(self, text):
        """グループステートを表示するEntryの更新。"""
        self.en_group_state.configure(state='normal')
        self.en_group_state.delete(0, tk.END)
        self.en_group_state.insert(0, 'Group State: %s' % text)
        self.en_group_state.configure(state='readonly')

    def seek_head(self):
        """最も近いメインパスの点に、ヘッドを移す。"""
        self.root.game_controller_queue.put(('SET_CLOSEST_MAINPATH_WAYPOINT_AS_HEAD',))

    def reset_head(self):
        """最も近いメインパスの点に、ヘッドを移す。"""
        self.root.game_controller_queue.put(('RESET_HEAD_TO_ZERO',))

    def play_one_step(self):
        """ワンステップ分だけグループプランを進める。"""
        self.root.game_controller_queue.put(('PLAY_ONE_STEP',))

    def start_autoplay(self):
        """プランの自動実行を開始する。"""
        self.root.game_controller_queue.put(('SET_AUTOPLAY_ON',))

    def stop_autoplay(self):
        """プランの自動実行を停止する。"""
        self.root.game_controller_queue.put(('SET_AUTOPLAY_OFF',))

