import tkinter as tk
import tkinter.ttk as ttk
import os
import glob
import re

# ユーザー定義ライブラリ
from lib.gui_settings import *
import lib.game_settings as gs


class MapWindow(tk.Toplevel):
    def __init__(self, master, parent, on_closing=None):
        super().__init__(master)
        self.attributes("-topmost", True)   # 常に最前面に表示

        self.master = master                # Tkオブジェクトになる
        self.parent = parent                # mainwindowを保持する
        self.on_closing = on_closing        # 自身を破壊する関数コールバック
        self.wm_protocol("WM_DELETE_WINDOW", on_closing)

        # 画像保持関係
        self.base_image = None              # PILイメージを保持するための変数。
        self.base_image_draw = None         # PILイメージに対するDrawオブジェクトを保持するための変数。
        self.base_map_lines = []
        self.base_map_points = []
        self.min_x = 0
        self.min_y = 0

        self.title("Map")
        #self.master.maxsize(1200, 800)

        # 1行目および、0列めのコンテンツは最大限拡大する。
        self.grid_rowconfigure(1, weight=1)
        self.grid_columnconfigure(0, weight=1)

        # フレームにボタン類を入れる。
        self.fr_button_frame = ttk.Frame(self)
        self.fr_button_frame.grid(row=0, column=0, sticky=tk.W)

        # 終わるボタン。このほかに、UpdateもしくはRedraw的なボタンがあってもいいか。
        self.btn_quit = ttk.Button(self.fr_button_frame)
        self.btn_quit.configure(text="Close Map Window",
                                command=self.quit_window)
        self.btn_quit.grid(row=0, column=0, sticky=tk.W)

        # ウィンドウサイズを小さくするボタン。
        self.btn_force_small_window = ttk.Button(self.fr_button_frame)
        self.btn_force_small_window.configure(text="Shrink Window",
                                              command=self.make_window_small)
        self.btn_force_small_window.grid(row=0, column=1, sticky=tk.W)

        # パスの描画ボタン。
        self.btn_draw_paths = ttk.Button(self.fr_button_frame)
        self.btn_draw_paths.configure(text="Update",
                                      command=self.update_all_map)
        self.btn_draw_paths.grid(row=0, column=2, sticky=tk.W)

        # マップカンバス
        self.cv_map_canvas = tk.Canvas(self)
        self.cv_map_canvas.grid(row=1, column=0, sticky=tk.E + tk.W + tk.N + tk.S)

        # 縦スクロールバー
        self.sb_vertical = tk.Scrollbar(self, orient=tk.VERTICAL)
        self.sb_vertical.grid(row=1, column=1, sticky=tk.N + tk.S)
        self.sb_vertical.config(command=self.cv_map_canvas.yview)

        # 横スクロールバー
        self.sb_horizontal = tk.Scrollbar(self, orient=tk.HORIZONTAL)
        self.sb_horizontal.grid(row=2, column=0, sticky=tk.E + tk.W)
        self.sb_horizontal.config(command=self.cv_map_canvas.xview)

        # Canvasのスクロール範囲を設定
        self.cv_map_canvas.config(xscrollcommand=self.sb_horizontal.set,
                                  yscrollcommand=self.sb_vertical.set)

        # Canvasの中に別のウィジェットを表示するために、create_windowして中にFrameを配置する。
        self.inner_frame = tk.Frame(self.cv_map_canvas, bd=2)
        self.inner_frame.grid_columnconfigure(0, weight=1)
        self.cv_map_canvas.create_window(0, 0, window=self.inner_frame, anchor='nw', tags='inner')

        self.update_layout()
        # If the widgets inside the canvas / the canvas itself change size,
        # the <Configure> event is fired which passes its new width and height to the corresponding callback
        self.cv_map_canvas.bind('<Configure>', self.on_configure)

        # マップファイルの内部を展開するための正規表現。sscalnfで読みこむような書き方なので、単純にカンマで区切るとPの最後の要素を壊しそう。
        self.re_map_line = re.compile(
            r"(L) *(-?[0-9\.]+), *(-?[0-9\.]+), *(-?[0-9\.]+), *(-?[0-9\.]+), *(-?[0-9\.]+), *(-?[0-9\.]+),([0-9 ]+),([0-9 ]+),([0-9 ]+)")
        self.re_map_point = re.compile(
            r"(P) *(-?[0-9\.]+), *(-?[0-9\.]+), *(-?[0-9\.]+), *(-?[0-9\.]+), *(-?[0-9\.]+), *(-?[0-9\.]+),([0-9 ]+),(.+)")

    def update_layout(self):
        """内部のウェジェット等が更新されたときの再調整。"""
        # すべてのイベントやコールバックを実行して同期する。
        self.inner_frame.update_idletasks()

        # Canvasのスクロール領域を再計算
        self.cv_map_canvas.configure(scrollregion=self.cv_map_canvas.bbox('all'))

        # スクロールをリセット
        self.cv_map_canvas.xview('moveto', '1.0')
        self.cv_map_canvas.yview('moveto', '1.0')

        # ウィンドウのサイズを内部のフレームに合わせる。
        self.size = self.inner_frame.grid_size()

    def on_configure(self, event):
        """カンバスサイズ等が変更された場合の再調整。"""
        print(event)
        _width, _height = event.width, event.height

        # インナーフレームの通常のサイズ。
        _natural_width = self.inner_frame.winfo_reqwidth()
        _natural_height = self.inner_frame.winfo_reqheight()

        # インナーフレームのサイズが変わったら、親のカンバスのサイズも変更する。
        self.cv_map_canvas.itemconfigure('inner', width=_width if _width > _natural_width else _natural_width)
        self.cv_map_canvas.itemconfigure('inner', width=_height if _height > _natural_height else _natural_height)

        # スクロール領域を更新。
        self.cv_map_canvas.configure(scrollregion=self.cv_map_canvas.bbox('all'))

    def quit_window(self):
        self.on_closing()

    def make_window_small(self):
        """マップ読み込み時にウィンドウが巨大になる場合があるので、ワンクリックで縮小化できるようにする。"""
        # self.configure()だと、ウィンドウを縮小する処理と拡大する処理がバッティングしてうまくいかない。
        self.geometry('%dx%d' % (MAP_WINDOW_SHRINK_WIDTH, MAP_WINDOW_SHRINK_HEIGHT))

    def load_base_map(self, map_short_name):
        """
        マップデータからマップを描画する。マップ上にアプリケーションで追加するオブジェクトはここでは扱わない。
        一度描画したものをキャッシュしておきたいが、速度的に難を感じるまでは手を付けないでおく。
        なお、tkのcanvasは、自身の画像情報をコピーする簡易な方法がなく扱いづらい。PIL等で描画し、Canvasに張り付けてはどうか。
        -> PIL側の描画が相当遅かったため中止。さらにPIL Image -> PIL PhotoImage -> tk.Canvas上のイメージへと
        いちいち変換しなければtkinter側で表示できない。tkinter側で毎回線を描くほうが速そう。
        """
        # とりあえず、ゾーンのshort_nameに該当するマップのファイルを全部読み込む。
        _map_file_candidates = glob.glob(os.path.join(gs.setting['MAP_PATH'],
                                                      "%s*.txt" % map_short_name))
        _map_file_candidates.sort()
        print(gs.setting['MAP_PATH'])

        if len(_map_file_candidates) == 0:
            print("cannot find any map file: %s ..." % map_short_name)
            return

        _map_file_data_list = []

        # アルファベット昇順で最初のファイルがメインのマップファイルかと思っていたが、そうでもないようだ。
        # おそらく無印->Base, 1,2,3とつくもの->各レイヤー。とりあえず全部取得する。
        # もっとも行数の大きいファイルをメインにするとか、そういう対応が必要かも？
        for _file_path in _map_file_candidates:
            with open(_file_path) as _df:
                _lines = _df.readlines()
                _map_file_data_list.append(_lines)

        # このフラット化は冗長で、上のループ内で実施できる。ただし、将来レイヤごとの管理にするかもしれないので、
        # レイヤごとのデータは別に保持しておける形にした。
        _flatten_map_data = []
        for _single_file_map_data in _map_file_data_list:
            _flatten_map_data.extend(_single_file_map_data)

        # 以下に分解。正規表現で拾えなかった場合は、エラーを吐いて戻る。
        # L x1,y1,z1,x2,y2,z2,r,g,b
        # P x1,y1,z1,r,g,b,layer?,label
        try:
            _lines = [self.re_map_line.match(_x).groups() for _x in _flatten_map_data if _x[0] == 'L']
            _points = [self.re_map_point.match(_x).groups() for _x in _flatten_map_data if _x[0] == 'P']

        except:
            print("some map data have unexpected format...aborting.")
            return

        # マップの描画領域を求めるために、すべてのノードの座標の最大最小のx,yを求める。
        _min_x = min(min([float(_v[1]) for _v in _lines]),
                     min([float(_v[4]) for _v in _lines]),
                     min([float(_v[1]) for _v in _points]))
        _min_y = min(min([float(_v[2]) for _v in _lines]),
                     min([float(_v[5]) for _v in _lines]),
                     min([float(_v[2]) for _v in _points]))
        _max_x = max(max([float(_v[1]) for _v in _lines]),
                     max([float(_v[4]) for _v in _lines]),
                     max([float(_v[1]) for _v in _points]))
        _max_y = max(max([float(_v[2]) for _v in _lines]),
                     max([float(_v[5]) for _v in _lines]),
                     max([float(_v[2]) for _v in _points]))

        _map_width = int(_max_x - _min_x + MAP_CANVAS_OUTER_MARGIN * 2)
        _map_height = int(_max_y - _min_y + MAP_CANVAS_OUTER_MARGIN * 2)

        self.cv_map_canvas.configure(width=_map_width,
                                     height=_map_height)

        self.min_x = _min_x
        self.min_y = _min_y
        self.base_map_lines = [{'x1': float(_x[1]),
                                'y1': float(_x[2]),
                                'x2': float(_x[4]),
                                'y2': float(_x[5])} for _x in _lines]
        self.base_map_points = [{'x': float(_x[1]),
                                 'y': float(_x[2]),
                                 'r': int(_x[4]),
                                 'g': int(_x[5]),
                                 'b': int(_x[6]),
                                 'text': _x[8]} for _x in _points]

    def draw_base_map(self):
        # ベースとなる地図を描画する。
        for _line in self.base_map_lines:
            self.cv_map_canvas.create_line(
                _line['x1'] - self.min_x + MAP_CANVAS_OUTER_MARGIN,
                _line['y1'] - self.min_y + MAP_CANVAS_OUTER_MARGIN,
                _line['x2'] - self.min_x + MAP_CANVAS_OUTER_MARGIN,
                _line['y2'] - self.min_y + MAP_CANVAS_OUTER_MARGIN,
                fill=MAP_LINE_COLOR)

        for _point in self.base_map_points:
            self.cv_map_canvas.create_text(
                _point['x'] - self.min_x + MAP_CANVAS_OUTER_MARGIN,
                _point['y'] - self.min_y + MAP_CANVAS_OUTER_MARGIN,
                text=_point['text'],
                fill=_from_rgb((_point['r'],
                                _point['g'],
                                _point['b'])))

    def draw_path(self, path, color=None, pathname=True, pathname_color=None, actions=True):
        """
        マップ上にユーザーが定義したパスを描画する。アクションもテキストで表示。
        この関数では、パス一本の描画を担う。
        :return:
        """
        _previous_point = None
        for _ix, _point in enumerate(path.path_points):
            # 線を描く。ただし、ひとつ前の点が定義されていないときは飛ばす。
            if _previous_point:
                # ゲーム内でのx,yの正負は画像のx,yと符号が逆転することに注意。
                self.cv_map_canvas.create_line(
                    -_previous_point.x - self.min_x + MAP_CANVAS_OUTER_MARGIN,
                    -_previous_point.y - self.min_y + MAP_CANVAS_OUTER_MARGIN,
                    -_point.x - self.min_x + MAP_CANVAS_OUTER_MARGIN,
                    -_point.y - self.min_y + MAP_CANVAS_OUTER_MARGIN,
                    fill=MAP_PATH_COLOR if not color else color,
                    width=2)

            # 円を描く
            _point_color = MAP_POINT_FOCUSED_COLOR if _ix == self.parent.editor_frame.point_cursor - 1 else MAP_POINT_COLOR
            self.cv_map_canvas.create_oval(-_point.x - self.min_x + MAP_CANVAS_OUTER_MARGIN - MAP_POINT_RADIUS,
                                           -_point.y - self.min_y + MAP_CANVAS_OUTER_MARGIN - MAP_POINT_RADIUS,
                                           -_point.x - self.min_x + MAP_CANVAS_OUTER_MARGIN + MAP_POINT_RADIUS,
                                           -_point.y - self.min_y + MAP_CANVAS_OUTER_MARGIN + MAP_POINT_RADIUS,
                                           outline=MAP_POINT_BORDER_COLOR,
                                           fill=_point_color,
                                           width=1)

            # TODO:テキストの描画はここで。プレースホルダ。
            self.cv_map_canvas.create_text(
                -_point.x - self.min_x + MAP_CANVAS_OUTER_MARGIN - MAP_POINT_RADIUS + MAP_POINT_ACTION_TEXT_SHIFT_X,
                -_point.y - self.min_y + MAP_CANVAS_OUTER_MARGIN - MAP_POINT_RADIUS + MAP_POINT_ACTION_TEXT_SHIFT_Y,
                text="" if not _point.actions else _point.actions,
                fill=MAP_POINT_ACTION_TEXT_COLOR)

            if _ix % MAP_POINT_INDEX_LABEL_INTERVAL == 0:
                self.cv_map_canvas.create_text(-_point.x - self.min_x + MAP_CANVAS_OUTER_MARGIN - MAP_POINT_RADIUS + MAP_POINT_INDEX_LABEL_SHIFT_X,
                                               -_point.y - self.min_y + MAP_CANVAS_OUTER_MARGIN - MAP_POINT_RADIUS + MAP_POINT_INDEX_LABEL_SHIFT_Y,
                                               text="%d" % _ix,
                                               fill=MAP_POINT_INDEX_LABEL_COLOR)

            _previous_point = _point

    def draw_all_path(self):
        """メインパスおよびリカバリーパスすべてを描画する。"""
        # 取り合えず簡単なものを置いておく。
        self.draw_path(self.parent.editor_frame.zone_plan.get_path('main'))
        # TODO:リカバリーパスもここで描く。

    def update_all_map(self):
        """カンバスの情報を全部消してから再度描画する。"""
        self.cv_map_canvas.delete('all')
        self.draw_base_map()
        self.draw_all_path()


def _from_rgb(rgb):
    """translates an rgb tuple of int to a tkinter friendly color code
    """
    return "#%02x%02x%02x" % rgb
