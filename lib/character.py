import re
import datetime

# ユーザー定義ライブラリ
import database.encyclopedia as db
import lib.game_settings as gs


# 書き込み時、読み込み時にミューテックスを保証する必要がある。
# グループの状態にどんなものがあるか？
# 移動可能、同期OK、色々…

VERBOSE_MODE = False

# ステータスアップデート時のデータの解釈は、すべてここで定義した正規表現で行い、一括して管理する。
# 簡単なものは正規表現を使わずスライス等で処理したほうが速いが、個別の対応は*しない*。
# GUI側から使いたくなるかもしれないので、ひとまずトップレベルに置く。
RE_LOC = re.compile(r'@=(-?[0-9\.]+):(-?[0-9\.]+):(-?[0-9\.]+)')
RE_LEADER_NAME = re.compile(r'N=([a-zA-Z]+)')
RE_HP = re.compile(r'H=(-?[0-9]+)/(-?[0-9]+)')
RE_MANA = re.compile(r'M=(-?[0-9]+)/(-?[0-9]+)')
RE_ENDURANCE = re.compile(r'E=(-?[0-9]+)/(-?[0-9]+)')
RE_BUFFS = re.compile(r'[0-9]+')
RE_SHORT_BUFFS = re.compile(r'[0-9]+')
RE_PET_BUFFS = re.compile(r'[0-9]+')
RE_LEVEL_CLASS = re.compile(r'L=(-?[0-9]+):(-?[0-9]+)')     # 負数はないとは思うが。
RE_TARGET = re.compile(r'T=(-?[0-9]+):(-?[0-9]+)')          # 負数はないとは思うが。
RE_PET = re.compile(r'P=(-?[0-9]+):(-?[0-9]+)')             # 負数はないとは思うが。
RE_DIRECTION = re.compile(r'\$=(-?[0-9\.]+)')
RE_ZONE = re.compile(r'Z=(-?[0-9]+):(-?[0-9]+)>(-?[0-9]+)')
RE_CAST = re.compile(r'C=(-?[0-9]+)')

SPELL_CAST_RESETTING_TIME = 2.5     # 秒

#STATUS_TEXT = '%s\nHP   %5d/%5d\nMANA %5d/%5d\nENDU %5d/%5d\nTGT= %d@%2d\nX=%.2f,Y=%.2f,z=%.2f\nZ=%s(%d)'


class Group:
    """
    グループの情報を管理する。
    """
    def __init__(self,
                 runner_user_name,
                 presentation_queue,
                 status_update_queue,
                 threading_lock):

        self.presentation_queue = presentation_queue
        self.status_update_queue = status_update_queue

        self.runner_user_name = runner_user_name    # EQBCSに接続するダミーユーザーの名前。自分以外の名前を取得するときに使う。
        self.threading_lock = threading_lock        # スレッドをロックするためのオブジェクト。原則的に更新・読み出し時にはロックする。
        self.members = {}                           # Characterオブジェクトの辞書。

    def add_member(self, name):
        """
        グループにメンバーを追加する。
        :param name:
        :return:
        """
        if name in self.members:
            print("The name %s is already used!" % name)
            return

        if name.upper() in gs.ignore_character:
            print("The name %s was just ignored..." % name)
            return

        # オブジェクトの生成時、__init__が終了してからローカル環境のdirに追加されるならこのロックは必要ないと思うが、どうなっているか？
        # しょっちゅう追加するものではないのでロックしておく。
        with self.threading_lock:
            self.members[name] = Character(name,
                                           self.presentation_queue,
                                           self.status_update_queue,
                                           self.threading_lock)

    def delete_member(self, name):
        """
        グループからメンバーを削除する。
        :param name:
        :return:
        """
        # キャラクターの削除を行う必要があるのか？->今のところ、あると想定。ログアウトしたキャラクターの情報があると、
        # 予期せぬバグに遭遇しそう。ただし、Linkdeadからの復帰のことを考え、Planの達成状況は破棄しない。
        # ロックした状態で、メンバー表上での存否確認、取り出し、削除までを実施する必要がある。
        with self.threading_lock:
            if name not in self.members:
                print("Cannot find the name %s!" % name)
                return

            # メンバー表から削除
            _member_to_delete = self.members.pop(name)

            # オブジェクトの破壊。外部でキャラクターへの参照を保持したままの可能性があるため、
            # メンバーから除外したらオブジェクトの寿命を尽きさせたい。
            # もしくは、キャラクターにis_validみたいなフラグを持たせる方法もあるが、とりあえずいまはこうする。
            del _member_to_delete

    def get_member(self, name):
        """
        参照を返すだけなので、ロックは必要ないはず。
        :param name:
        :return:
        """
        return self.members.get(name)

    def update_member(self, name, text):
        """
        メンバーのステータスを更新する。
        :param name:
        :param text:
        :return:
        """
        # アップデートすべきメンバーがグループにいなかったら、追加する。
        if name not in self.members:
            self.add_member(name)

        # 変更する値がCharacterのメンバ変数なので、このレベルではロックせず、Character側でロックする。
        self.members[name].update_statuses(text)

    def get_member_name_list(self, exclude_name=None):
        """
        メンバー名の一覧を返す。
        :param text: メンバー表から除外する名前
        :return:
        """
        _temp_roster = self.members.keys()
        if exclude_name in _temp_roster:
            _temp_roster.remove(exclude_name)

        return _temp_roster

    def check_timed_events(self):
        """
        メンバーのtimed_eventのチェックを行う。
        :return:
        """
        for _member in self.members.values():
            _member.check_timed_events()


class Character:
    """
    キャラクター情報を管理する。データの更新時にGUIの更新を要求するため、プレゼンテーションキューへの参照を持たせる。
    GUI側からは、このクラスの中身を直接見ることはないようにする。
    """
    def __init__(self,
                 name,
                 presentation_queue,
                 status_update_queue,
                 threading_lock):

        self.presentation_queue = presentation_queue    # プレゼンテーションキュー
        self.status_update_queue = status_update_queue  # ステータス更新キュー

        self.threading_lock = threading_lock            # ロックオブジェクト

        # セルフタイマーで起動するイベントを管理する。
        self.timed_events = []      # 時刻、キューに送るべきイベントの順に登録。

        self.is_ready = False
        self.is_linkdead = False

        # 一般的なステータス
        self.name = name
        self.leader_name = ''
        self.max_hp = 0
        self.max_mana = 0
        self.max_endurance = 0
        self.hp = 0
        self.mana = 0
        self.endurance = 0
        self.x = 0                  # @=
        self.y = 0                  # @=
        self.h = 0                  # @=
        self.direction = 0          # @=
        self.buffs = []             # B=
        self.short_buffs = []       # S=
        self.level = 0              # L=
        self.player_class = 0       # L=:
        self.character_id = 0       # >
        self.target_id = 0          # T=
        self.target_hp = 0          # T=:
        self.pet_id = 0             # P=
        self.pet_hp = 0             # P=:
        self.pet_buffs = []         # W=
        self.zone_id = 0            # Z
        self.zone_instance_id = 0   # Z:?
        self.direction = 0          # $=
        self.casting_spell_id = 0   # C=

    def check_timed_events(self):
        _now = datetime.datetime.now()
        _event_index_to_delete = []

        with self.threading_lock:
            for _ix, _event in enumerate(self.timed_events):
                if _event[0] < _now:    # イベント発生
                    # 削除すべきイベントを記録しておく。
                    # self.timed_eventsに対してループを回しているので、ここでの削除は不可。
                    _event_index_to_delete.append(_ix)

                    if _event[1] == 'SPELL_RESET' and _event[2] == self.casting_spell_id:
                        self.status_update_queue.put(b'NBPKT:%s:[NB]|C=0|[NB]' % bytes(self.name, encoding='utf-8'))

            # 削除すべきイベントを削除
            self.timed_events = [_iy for _ix, _iy in enumerate(self.timed_events) if _ix not in _event_index_to_delete]

    def update_statuses(self, text):
        """
        ステータスをバルクでセットする。
        :param text:
        :return:
        """
        _statuses = text.split("|")

        # EQBCSからの1通信に含まれるすべてのステータスのアップデートを行う間、スレッドのロックを行う。
        with self.threading_lock:

            _character_status_updated = False
            _buffs_updated = False

            for _status in _statuses:
                # 値がセットされていない場合は、更新処理は不要。最初と最後の|の前後に生じる''もこれでスキップ。
                # ただし、W=/みたいなものはすり抜けるので、不正な形式になっていたら呼び出し先で個別に対応。
                if len(_status) < 3:
                    continue

                # 値の更新。
                _tmp_character_status_updated, _tmp_buffs_updated = self.update_status(_status)

                # ステータスもしくはバフの情報が更新されたか。
                _character_status_updated = _character_status_updated | _tmp_character_status_updated
                _buffs_updated = _buffs_updated | _tmp_buffs_updated

            if _character_status_updated:
                _gui_change = ('STATUS',
                               self.name,
                               {'name': self.name,
                                'hp': self.hp,
                                'max_hp': self.max_hp,
                                'mana': self.mana,
                                'max_mana': self.max_mana,
                                'endurance': self.endurance,
                                'max_endurance': self.max_endurance,
                                'target_id': self.target_id,
                                'target_hp': self.target_hp,
                                'pet_id': self.pet_id,
                                'pet_hp': self.pet_hp,
                                'leader_name': self.leader_name,
                                'x': self.x,
                                'y': self.y,
                                'h': self.h,
                                'zone_longname': db.get_zone_longname(self.zone_id),
                                'zone_id': self.zone_id,
                                'casting_spell_id': self.casting_spell_id})
                self.presentation_queue.put(_gui_change)

            if _buffs_updated:
                # ソートは遅いが可読性が上がるので、実施する。
                _buff_texts = sorted([db.get_spell_name(_x) for _x in self.buffs])
                _short_buff_texts = sorted(['*' + db.get_spell_name(_x) for _x in self.short_buffs])
                _pet_buff_texts = sorted(['+' + db.get_spell_name(_x) for _x in self.pet_buffs])

                _buffs = _buff_texts + _short_buff_texts + _pet_buff_texts
                _buffs_change = ('BUFFS',
                                 self.name,
                                 _buffs)      # とりあえず左のペインに全部入れてみる
                self.presentation_queue.put(_buffs_change)

    def update_status(self, text):
        """
        単一のステータスをセットする。
        :param text:
        :return:
        """
        _character_status_updated = False
        _buffs_updated = False
        _type = text[:1]
        _body = text[2:]

        if _type == '@':        # Location
            _y, _x, _h = RE_LOC.search(text).groups()
            self.x = float(_x)
            self.y = float(_y)
            self.h = float(_h)
            _character_status_updated = True

            if VERBOSE_MODE:
                _line = "[%s] loc is now @ %f,%f,%f." % (self.name, self.x, self.y, self.h)
                self.presentation_queue.put(('WRITE_TO_CONSOLE', _line))

        elif _type == '$':        # Direction
            _direction = RE_DIRECTION.search(text)
            if not _direction:
                return False, False

            _direction = _direction.groups()
            self.direction = float(_direction[0])
            _character_status_updated = True

            if VERBOSE_MODE:
                _line = "[%s] direction is now @ %f." % (self.name, self.direction)
                self.presentation_queue.put(('WRITE_TO_CONSOLE', _line))

        elif _type == 'N':      # Group Leader
            self.leader_name = RE_LEADER_NAME.search(text).groups()[0]

            if VERBOSE_MODE:
                _line = "[%s] my leader is now %s." % (self.name, self.leader_name)
                self.presentation_queue.put(('WRITE_TO_CONSOLE', _line))

        elif _type == 'H':      # Hit Point
            _hp_data = RE_HP.search(text)
            if not _hp_data:
                return False, False

            _hp, _max_hp = _hp_data.groups()
            self.hp = int(_hp)
            self.max_hp = int(_max_hp)
            _character_status_updated = True

            if VERBOSE_MODE:
                _line = "[%s] HP is now @ %d/%d." % (self.name, self.hp, self.max_hp)
                self.presentation_queue.put(('WRITE_TO_CONSOLE', _line))

        elif _type == 'M':      # Mana
            _mana_data = RE_MANA.search(text)
            if not _mana_data:
                return False, False

            _mana, _max_mana = _mana_data.groups()
            self.mana = int(_mana)
            self.max_mana = int(_max_mana)
            _character_status_updated = True

            if VERBOSE_MODE:
                _line = "[%s] MANA is now @ %d/%d." % (self.name, self.mana, self.max_mana)
                self.presentation_queue.put(('WRITE_TO_CONSOLE', _line))

        elif _type == 'E':      # Endurance
            _endurance_data = RE_ENDURANCE.search(text)
            if not _endurance_data:
                return False, False
            _endurance, _max_endurance = _endurance_data.groups()
            self.endurance = int(_endurance)
            self.max_endurance = int(_max_endurance)
            _character_status_updated = True

            if VERBOSE_MODE:
                _line = "[%s] ENDURANCE is now @ %d/%d." % (self.name, self.endurance, self.max_endurance)
                self.presentation_queue.put(('WRITE_TO_CONSOLE', _line))

        elif _type == 'B':      # Buffs
            _buffs = RE_BUFFS.findall(text)
            if not _buffs:
                self.buffs = []         # バフデータがない時はNoneではなく空リストとし、型を揃える。
            else:
                self.buffs = [int(_x) for _x in _buffs]
            _buffs_updated = True

            if VERBOSE_MODE:
                _line = "[%s] Buffs are %s." % (self.name, ",".join(_buffs))
                self.presentation_queue.put(('WRITE_TO_CONSOLE', _line))

        elif _type == 'L':      # Level and Class
            _level_class_data = RE_LEVEL_CLASS.search(text)
            if not _level_class_data:
                return False, False

            _level, _class = _level_class_data.groups()
            self.level = int(_level)
            self.player_class = int(_class)             # クラスは、以前ENCが-242とかになっていたことがある。
            _character_status_updated = True

            if VERBOSE_MODE:
                _line = "[%s] LEVEL/CLASS is %d/%d(%s)." % (self.name,
                                                            self.level,
                                                            self.player_class,
                                                            db.get_player_class(self.player_class)[0])
                self.presentation_queue.put(('WRITE_TO_CONSOLE', _line))

        elif _type == 'S':      # Short time buffs
            _short_buffs = RE_SHORT_BUFFS.findall(text)
            if not _short_buffs:
                self.short_buffs = []         # バフデータがない時はNoneではなく空リストとし、型を揃える。
            else:
                self.short_buffs = [int(_x) for _x in _short_buffs]
            _buffs_updated = True

            if VERBOSE_MODE:
                _line = "[%s] Short buffs are %s." % (self.name, ",".join(_short_buffs))
                self.presentation_queue.put(('WRITE_TO_CONSOLE', _line))

        elif _type == 'Z':      # Zone, Instance ID, My Character ID
            _zone_infos = RE_ZONE.search(text)
            if not _zone_infos:
                return False, False

            _zone_id, _instance_id, _my_character_id = _zone_infos.groups()
            self.zone_id = int(_zone_id)
            self.zone_instance_id = int(_instance_id)
            self.character_id = int(_my_character_id)
            _character_status_updated = True

            if VERBOSE_MODE:
                _line = "[%s] ZONE_id %d / INSTANCE_id %d /My ID %d." % (self.name,
                                                                         self.zone_id,
                                                                         self.zone_instance_id,
                                                                         self.character_id)
                self.presentation_queue.put(('WRITE_TO_CONSOLE', _line))

        elif _type == 'T':      # Target ID and HP
            _target_data = RE_TARGET.search(text)
            if not _target_data:
                return False, False

            _target_id, _target_hp = _target_data.groups()
            self.target_id = int(_target_id)
            self.target_hp = int(_target_hp)
            _character_status_updated = True

            if VERBOSE_MODE:
                _line = "[%s] TARGET ID/HP is %d/%d." % (self.name, self.target_id, self.target_hp)
                self.presentation_queue.put(('WRITE_TO_CONSOLE', _line))

        elif _type == 'P':      # Pet ID and HP
            _pet_data = RE_PET.search(text)
            if not _pet_data:
                return False, False

            _pet_id, _pet_hp = _pet_data.groups()
            self.pet_id = int(_pet_id)
            self.pet_hp = int(_pet_hp)
            _character_status_updated = True

            if VERBOSE_MODE:
                _line = "[%s] PET ID/HP is %d/%d." % (self.name, self.pet_id, self.pet_hp)
                self.presentation_queue.put(('WRITE_TO_CONSOLE', _line))

        elif _type == 'W':      # Pet Buffs
            _pet_buffs = RE_PET_BUFFS.findall(text)
            if not _pet_buffs:
                self.pet_buffs = []         # バフデータがない時はNoneではなく空リストとし、型を揃える。
            else:
                self.pet_buffs = [int(_x) for _x in _pet_buffs]
            _buffs_updated = True

            if VERBOSE_MODE:
                _line = "[%s] Pet buffs are %s." % (self.name, ",".join(_pet_buffs))
                self.presentation_queue.put(('WRITE_TO_CONSOLE', _line))

        elif _type == 'C':      # Start Cast/Stop Cast
            """
            C=0以外の数値ならキャストスタート、0ならストップとする。
            """
            _cast_spell = RE_CAST.findall(text)
            if not _cast_spell:
                return False, False
            else:
                self.casting_spell_id = int(_cast_spell[0])
            _character_status_updated = True

            # タイマーイベントに、スペルキャストのリセットイベントを登録。キャストが速いと別なスペルに代わっている場合があるため、
            # その場合、リセットをキャンセルするためにキャスト開始したスペルのIDを持っておく。
            if self.casting_spell_id != 0:
                _event = [datetime.datetime.now() + datetime.timedelta(seconds=SPELL_CAST_RESETTING_TIME),
                          'SPELL_RESET',
                          self.casting_spell_id]
                self.timed_events.append(_event)

            if VERBOSE_MODE:
                if self.casting_spell_id != 0:
                    _line = "[%s] Start Casting... %s." % (self.name, self.casting_spell_id)
                else:
                    _line = "[%s] Removing Casting Information." % self.name
                self.presentation_queue.put(('WRITE_TO_CONSOLE', _line))

        else:
            # 処理できなかった部分をダンプする。
            _line = "?:%s: %s" % (self.name, text)
            self.presentation_queue.put(('WRITE_TO_CONSOLE', _line))

        return _character_status_updated, _buffs_updated
