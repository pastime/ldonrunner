import json
import math
import random

# ユーザー定義ライブラリ
import database.encyclopedia

# MasterPlanは、複数のZonePlanファイルを束ねたバッチファイルのようなもの。
# ZonePlanは、ファイル単位、ゾーン単位で設定し、メインのパスとリカバリーバス、各種イベントを設定したもの。
# エディタで編集するのは、ZonePlan。

'''
ZonePlanのセーブファイルはJSON形式にする。
とりあえずMasterPlanはあとで作ることにする。
'''


class ZonePlan:
    def __init__(self, zone_plan_name='default', zone_id=0):
        # 各種属性
        self.zone_plan_name = zone_plan_name        # ゾーンプラン名。ファイル名は、これに.jsonとつけたものにする。
        #self.file_path = file_path                 # ファイルのパス
        self.zone_id = zone_id                      # ゾーンのID

        # 保持するデータ
        self.main_path = Path('main', self.zone_id)
        self.recovery_paths = {}

    def add_recovery_path(self):
        """リカバリーパスを追加する。デフォルトでは、r+番号。"""
        _already_used_keys = self.recovery_paths.keys()
        _newly_adding_num = 0

        # 使われていない番号を探す。
        while 'r%d' % _newly_adding_num in _already_used_keys:
            _newly_adding_num += 1

        # 白地のパスを追加。
        _path_name = 'r%d' % _newly_adding_num
        self.recovery_paths[_path_name] = Path(_path_name, self.zone_id)

    def rename_recovery_path(self, old_name, new_name):
        if old_name not in self.recovery_paths.keys():
            print("リカバリーパスの名前を変更しようとしましたが、%sという名前のパスは存在しません。" % old_name)
            return
        if new_name in self.recovery_paths.keys():
            print("リカバリーパス%sという名前は、既に使われているため、変更できません。" % new_name)
            return

        self.recovery_paths[new_name] = self.recovery_paths[old_name]
        self.recovery_paths[new_name].path_name = new_name

    def save_zone_plan(self, file_path):
        """
        インスタンス内に含まれる情報をすべて辞書に詰め、JSON形式で保存する。
        pathやpath内のpoint等の情報は、それぞれのクラス内で辞書形式に変換してここに持ってくる。
        """
        _save_to_json = {'zone_plan_name': self.zone_plan_name,
                         'zone_id': self.zone_id,
                         'main_path': self.main_path.get_dict(),
                         'recovery_paths': [x.get_dict() for x in self.recovery_paths.values()]}

        with open(file_path, 'w') as _df:
            json.dump(_save_to_json, _df, ensure_ascii=False, indent=4)

    def load_zone_plan(self, file_path):
        """
        JSONから、ゾーンプランを読み込む。
        :param file_path:
        :return:
        """
        try:
            with open(file_path, 'r') as _df:
                _json = json.load(_df)
        except Exception as e:
            print("JSONファイル%sは、読み込めませんでした。" % file_path, e)
            return

        self.zone_plan_name = _json['zone_plan_name']
        self.zone_id = int(_json['zone_id'])

        # メインパスの再構成
        for _point in _json['main_path']['points']:
            self.main_path.add_point(float(_point['x']),
                                     float(_point['y']),
                                     float(_point['h']),
                                     _point['actions'])

        # TODO: リカバリーパスの再構成

    def find_closest_recovery_path(self, x, y, h, height_weight, upper_limit=0):
        """
        所属するリカバリーパスを、(x, y, h)に近い順にリスト化したものを返す。
        :param x:
        :param y:
        :param h:
        :param height_weight: 高さの差を重視する場合は、高定差への倍率で指定する。
        :param upper_limit: 最も近い点でも距離がこれを超える場合は、Noneを返す。0の場合は、指定なし。
        :return: [self.recovery_pathsのキー、リカバリーパス内でのPointオブジェクトのインデックス,距離]
        """
        # リカバリーパスの辞書から、(key, (closest_point_index, closest_distance))なリストを生成。
        _recovery_paths_data = [(_key, _value.get_closest_point(x, y, h, height_weight, upper_limit)) for _key, _value in self.recovery_paths.items()]

        # 上記のリストの要素が入れ子になっているのをフラット化し、さらにclosest_point_index=-1のものを除外。
        _recovery_paths_data = [(_x[0], _x[1][0], _x[1][1]) for _x in _recovery_paths_data if _x[1][0] != -1]

        # この時点で該当するパスがなかった場合は、sortedを通さずに空リストのまま返す。
        if len(_recovery_paths_data) == 0:
            return []

        # 3番目の要素で昇順ソートしたものを返す。
        return sorted(_recovery_paths_data, key=lambda _x: _x[2])

    def get_path(self, path_name):
        """パス名からメインパスもしくはリカバリーパスを返す。"""
        if path_name == 'main':
            return self.main_path
        else:
            return self.recovery_paths.get(path_name)

    def add_point_to_path(self, path_name, x, y, h, actions=None, index=None):
        """パス名と座標から、パスに点を追加もしくは挿入する。"""
        _target_path = self.get_path(path_name)

        if _target_path is None:
            print("パス%sは現在のゾーンプランには存在しません。")
            return

        if not index:
            _target_path.add_point(x, y, h, actions)
        else:
            _target_path.insert_point(index, x, y, h, actions)


class Path:
    def __init__(self, path_name, zone_id):
        # このあたりの属性はたぶん使わないと思うが…
        self.path_name = path_name                  # このパスの名前（ファイルパスではなく、通り道のパス）
        self.zone_short_name = zone_id              # このパスが属するゾーンのショートネーム。

        self.path_points = []   # Pointの配列。順番は重要。速度的に問題があればのちのち連結リストにする。

    def get_point(self, index):
        """index番目の点を取得。"""
        return None if index >= len(self.path_points) else self.path_points[index]

    def add_point(self, x, y, h, actions=None):
        """パスの末尾に点を足す。"""
        self.path_points.append(Point(x, y, h, actions))

    def insert_point(self, index, x, y, h, actions=None):
        """index番目の要素として、点を挿入。"""
        self.path_points = self.path_points[:index] + [Point(x, y, h, actions)] + self.path_points[index:]

    def delete_point(self, index):
        """index番目の要素を削除。"""
        self.path_points = self.path_points[:index] + self.path_points[index+1:]

    def get_closest_point(self, x, y, h, height_weight=1.0, upper_limit=0, head=None):
        """
        所属する点から、もっとも(x, y, h)に近いものを返す。
        :param x:
        :param y:
        :param h:
        :param height_weight: 高さの差を重視する場合は、高定差への倍率で指定する。
        :param upper_limit: 最も近い点でも距離がこれを超える場合は、Noneを返す。0の場合は、指定なし。
        :param head: 値が設定されていたら、その値のインデックスよりも前のポイントから探す。
        :return: Pointオブジェクトのインデックス, 距離
        """
        if len(self.path_points) == 0:
            print("Error - パスに点がありません。")
            return -1, 0

        _closest_point_index = 0
        _closest_distance = 100000      # 適当に大きい数を
        for _ix, _p in enumerate(self.path_points):
            if head and _ix > head:
                # headに整数が指定されていたら、その値を超えるインデックスのポイントは無視する。
                break
            _distance = _p.distance(x, y, h, height_weight)
            if _distance < _closest_distance:
                _closest_point_index = _ix
                _closest_distance = _distance

        if _closest_distance < upper_limit and upper_limit > 0:
            return -1, 0

        return _closest_point_index, _closest_distance

    def get_close_points(self, x, y, h, height_weight=1.0):
        """
        所属する点から、近い順に点のインデックスと距離を返す。
        :param x:
        :param y:
        :param h:
        :param height_weight: 高さの差を重視する場合は、高定差への倍率で指定する。
        :return: Pointオブジェクトのインデックスと距離のリスト。
        """
        if len(self.path_points) == 0:
            print("Error - パスに点がありません。")
            return -1, 0

        _index_distance_pair = [0] * len(self.path_points)
        for _ix, _p in enumerate(self.path_points):
            _distance = _p.distance(x, y, h, height_weight)
            _index_distance_pair[_ix] = (_ix, _distance)

        # 2列めをキーにソート
        _sorted_index_distance_pair = sorted(_index_distance_pair, key=lambda _x: _x[1])
        return _sorted_index_distance_pair

    def get_dict(self):
        """保持しているデータを辞書形式にしたものを返す。"""
        _serialized_dict = {
            'path_name': self.path_name,
            'points': [_point.get_dict() for _point in self.path_points]
        }
        return _serialized_dict


class Point:
    """
    点を管理するクラス。
    """
    def __init__(self, x, y, h, actions=None):
        self.x = x
        self.y = y
        self.h = h

        # ここでしばらくMediしろとか、とりあえず何秒待てとか、autosellしろとか、バフしろとか、音を鳴らせとか、指令を登録する。
        # ほか、誰にsay、rtz、ここで戦闘が発生しても次まで進め、ここで戦闘が発生したら戻れ、みたいな。
        # 順番の保持のためにリストとし、シリアライズの都合により、保持するものは文字列だけというルールとする。
        self.actions = actions

    def distance(self, x, y, h, height_weight=1.0):
        """
        指定された点と、この点との距離を求める。
        :param x:
        :param y:
        :param h:
        :param height_weight: 高さの差を重視する場合は、高定差への倍率で指定する。
        :return:
        """
        return math.sqrt((self.x - x)**2 + (self.y - y)**2 + ((self.h - h) + height_weight) ** 2)

    def get_dict(self):
        """保持しているデータを、辞書形式で返す。"""
        _serialized_dict = {
            'x': self.x,
            'y': self.y,
            'h': self.h,
            'actions': self.actions
        }
        return _serialized_dict

    def get_actions(self):
        """セミコロンで分割されたアクションを、リストにして返す。"""
        if not self.actions:
            return []
        return self.actions.split(';')

    def find_action(self, header):
        """
        headerで指定された文言で始まるアクションを持っていたら、最初のものを返す。
        :paran header: str
        :return: [header, value]形式。
        """
        _actions = self.get_actions()
        for _action in _actions:
            if _action[:len(header)] == header:
                # 頭からheaderの長さ分が一致したら、actionあり。

                if len(_action) == len(header):
                    # 後ろに秒数などのパラメータがなければ、このままint化して解釈してもエラーにならないように、0という文字列を返す。
                    return '0'
                else:
                    # 後ろにまだ何かあれば、それをパラメータとして文字列のまま返す。
                    return _action[len(header):]
        return None

    def get_salted_point(self, randomize_value):
        """
        x,y座標に対してrandomize_valueを加味した自身のコピーを作って返す。
        :param randomize_value:
        :return:
        """
        _salted_point = Point(self.x + random.uniform(-randomize_value, randomize_value),
                              self.y + random.uniform(-randomize_value, randomize_value),
                              self.h,
                              actions=self.actions)
        return _salted_point

    def __str__(self):
        print("(Point x=%f, y=%f, h=%f, act:%s)" % (self.x,
                                                    self.y,
                                                    self.h,
                                                    self.actions))